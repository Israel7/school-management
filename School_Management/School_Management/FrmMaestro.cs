﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibSM;
using System.IO;

namespace School_Management
{
    public partial class FrmMaestro : Form
    {
        Maestro mostrarMaestro = new Maestro();
        List<Maestro> mostrarInfoMaestro = new List<Maestro>();
        String archivo;
        Maestro maestro = new Maestro();

        public FrmMaestro()
        {
            InitializeComponent();
        }

        private void igualarCamposMaestro()
        {
            maestro.NumEmpleado = int.Parse(lblContMaestro.Text);
            maestro.ApellidoPaterno = txtApPatMaestro.Text;
            maestro.ApellidoMaterno = txtApMatMaestro.Text;
            maestro.Nombre = txtNomMaestro.Text;
            maestro.FechaNac = Convert.ToDateTime(dtFechaNacMaestro.Value.ToString("yyyy-MM-dd"));
            maestro.Materia = cbSelecMateria.Text;
            maestro.Dirección = txtDireccionMaestro.Text;
            maestro.Telefono = txtTelMaestro.Text;
            maestro.Email = txtEmailMaestro.Text;
            maestro.Cedula = txtCedulaMaestro.Text;
            maestro.Rfc = txtRfcMaestro.Text;
            maestro.Foto = archivo.Replace(@"\", @"\\");
            maestro.UsuarioMaestro = txtUsuarioMaestro.Text;
            maestro.ContrasenaMaestro = txtContrasenaMaestro.Text;
        }



        private void FrmMaestro_Load(object sender, EventArgs e)
        {
            mostrarEnGrid();
            limpiarDatosMaestro();
        }

        private void btnAgregarMaestro_Click(object sender, EventArgs e)
        {
            igualarCamposMaestro();

            if (maestro.agregarMaestro())
            {

                mostrarEnGrid();
                MessageBox.Show("El Maestro " + maestro.Nombre + " " + maestro.ApellidoPaterno + " Ha sido Inscrito");
                limpiarDatosMaestro();
                ////////VOVLER A CARGAR DATOS EN GRID"""
            }
            else
                MessageBox.Show("Error al agregar el usuario Administrativo. Verifique los datos");

        }

        public void mostrarEnGrid()
        {
            //CREAN LAS COLUMNAS DEL DATAGRID
            //dgAlumnos.DataSource = null;

            DataTable llenarDatosAdmin = new DataTable();
            llenarDatosAdmin.Columns.Add("ColNumEmpleado");
            llenarDatosAdmin.Columns.Add("ColApPat");
            llenarDatosAdmin.Columns.Add("ColApMat");
            llenarDatosAdmin.Columns.Add("ColNom");
            llenarDatosAdmin.Columns.Add("ColFechaNac");
            llenarDatosAdmin.Columns.Add("ColMateria");
            llenarDatosAdmin.Columns.Add("ColEmail");
            llenarDatosAdmin.Columns.Add("ColFoto").DataType = System.Type.GetType("System.Byte[]");
            llenarDatosAdmin.Columns.Add("colRuta");
            //IGUALAMOS LOS REGISTROS 
            mostrarInfoMaestro = mostrarMaestro.enlistarMaestro();

            for (int i = 0; i < mostrarInfoMaestro.Count; i++)
            {
                //COLOCAMOS REGISTROS EN SUS RESPECTIVOS RENGLONES
                DataRow renglones = llenarDatosAdmin.NewRow();
                renglones["ColNumEmpleado"] = mostrarInfoMaestro[i].NumEmpleado;
                renglones["ColApPat"] = mostrarInfoMaestro[i].ApellidoPaterno;
                renglones["ColApMat"] = mostrarInfoMaestro[i].ApellidoMaterno;
                renglones["ColNom"] = mostrarInfoMaestro[i].Nombre;
                renglones["ColFechaNac"] = mostrarInfoMaestro[i].FechaNac;
                renglones["ColMateria"] = mostrarInfoMaestro[i].Materia;
                //renglones["ColTu"] = mostrarInfoMaestro[i].TipoUsuario;
                renglones["ColEmail"] = mostrarInfoMaestro[i].Email;
                try
                {
                    var imageConverter = new ImageConverter();
                    renglones["ColFoto"] = imageConverter.ConvertTo(Image.FromFile(mostrarInfoMaestro[i].Foto), System.Type.GetType("System.Byte[]"));
                    //pictureBox1.Image = Image.FromFile(mostrarInfoAlumno[i].Foto);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.ToString());
                }
                renglones["colRuta"] = mostrarInfoMaestro[i].Foto;


                //IGUALAR EL ULTIMO VALOR DE ID EN LA BASE DE DATOS
                lblContMaestro.Text = mostrarInfoMaestro[i].NumEmpleado.ToString();
                llenarDatosAdmin.Rows.Add(renglones);
            }
            //OBTENIENDO EL ULTIMO VALOR SE LE AGREGA 1 QUE SERIA EL SIGUIENTE USUARIO A INGRESAR
            //lblCont.Text = (int.Parse(lblCont.Text) + 1).ToString();
            //dgAlumnos.DataSource = null;

            dgMaestro.DataSource = llenarDatosAdmin;

            //AGREGAMOS NUEVO REGISTRO AL DATAGRID
        }

        private void pbFotoMaestro_Click(object sender, EventArgs e)
        {
            //para cargar imagenes en el pictureBox
            openFileDialog3.Filter = "image files (*.gif; *.jpg; *.png)| *.gif; *.jpg; *.png";
            openFileDialog3.ShowDialog();

            archivo = openFileDialog3.FileName;
            string directoryPath = Path.GetDirectoryName(openFileDialog3.FileName);
            //MessageBox.Show("archivo " + archivo.Length);
            if (directoryPath != "openFileDialog1")

                pbFotoMaestro.Image = (Bitmap)Bitmap.FromFile(openFileDialog3.FileName);
        }

        public void limpiarDatosMaestro()
        {
            txtApPatMaestro.Clear();
            txtApMatMaestro.Clear();
            txtNomMaestro.Clear();
            dtFechaNacMaestro.ResetText();
            txtDireccionMaestro.Clear();
            txtTelMaestro.Clear();
            txtEmailMaestro.Clear();
            cbSelecMateria.Text = cbSelecMateria.Items[0].ToString();
            txtCedulaMaestro.Clear();
            txtRfcMaestro.Clear();
            pbFotoMaestro.Image = (Bitmap)Image.FromFile(@"C:\Users\Work-Group\Desktop\Resources\maestra1.png");
            txtUsuarioMaestro.Clear();
            //txtConfirmContrasenaM.Clear();
            txtApPatMaestro.Focus();
        }

       
    }
}
