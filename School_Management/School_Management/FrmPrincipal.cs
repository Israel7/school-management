﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace School_Management
{
    public partial class FrmPrincipal : Form
    {
        public string opcionActivar;
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void inisiarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmLogin ventanaLogin = new FrmLogin();
            this.Hide();
            ventanaLogin.ShowDialog();
        }

        private void administracionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAdministrativo ventanaAdministrativo = new FrmAdministrativo();
            ventanaAdministrativo.ShowDialog();
        }

        private void evaluar1erAñoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FrmCalifPrimerAño ventanaClif1año = new FrmCalifPrimerAño();
            //ventanaClif1año.ShowDialog();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            if (opcionActivar == "Administrativo")
            {
                tsmIngresar.Enabled = true;
                tsmRegistroEscolar.Enabled = true;
                tsmMaestro.Enabled = true;
                tsmAlumno.Enabled = true;
                tsmReportes.Enabled = true;
                tsmAyuda.Enabled = true;
            }
            else if (opcionActivar == "Maestro")
            {
                tsmIngresar.Enabled = true;
                tsmRegistroEscolar.Enabled = false;
                tsmMaestro.Enabled = true;
                tsmAlumno.Enabled = false;
                tsmReportes.Enabled = false;
                tsmAyuda.Enabled = true;
            }
            else if (opcionActivar == "Alumno")
            {
                tsmIngresar.Enabled = true;
                tsmRegistroEscolar.Enabled = false;
                tsmMaestro.Enabled = false;
                tsmAlumno.Enabled = true;
                tsmReportes.Enabled = false;
                tsmAyuda.Enabled = true;
            }
            else
            {
                tsmIngresar.Enabled = true;
                tsmRegistroEscolar.Enabled = false;
                tsmMaestro.Enabled = false;
                tsmAlumno.Enabled = false;
                tsmReportes.Enabled = false;
                tsmAyuda.Enabled = false;
            }
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void informacionDeUsuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FrmInfoUsuario infoUsu = new FrmInfoUsuario();
            //infoUsu.ShowDialog();
        }

        private void evaluar2doAñoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FrmCalifSegundoAño ventanaCalif2ano = new FrmCalifSegundoAño();
            //ventanaCalif2ano.ShowDialog();
        }

        private void evaluar3erAAñoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FrmCalifTercerAño ventanaCalif3ano = new FrmCalifTercerAño();
            //ventanaCalif3ano.ShowDialog();
        }

        private void consultarCalificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FrmConsultarCalif ventanaConsultarCalif = new FrmConsultarCalif();
            //ventanaConsultarCalif.ShowDialog();
        }
    }
}
