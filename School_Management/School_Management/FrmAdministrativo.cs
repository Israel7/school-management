﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using LibSM;


namespace School_Management
{
    public partial class FrmAdministrativo : Form
    {
        Administrativo mostrarAdministrativo = new Administrativo();
        List<Administrativo> mostrarInfoAdmin = new List<Administrativo>();
        //String archivo;
        Administrativo admin = new Administrativo();
        public FrmAdministrativo()
        {
            InitializeComponent();
        }

        private void igualarCamposAdmin()
        {
            admin.NumEmpleado = int.Parse(lblContAdmin.Text);
            admin.ApellidoPaterno = txtApAdmin.Text;
            admin.ApellidoMaterno = txtAmAdmin.Text;
            admin.Nombre = txtNomAdmin.Text;
            admin.FechaNac = Convert.ToDateTime(dtpFechaNacAdmin.Value.ToString("yyyy-MM-dd"));
            admin.TipoUsuario = cbTipoUsuario.Text;
            admin.Direccion = txtDirAdmin.Text;
            admin.Telefono = txtTelAdmin.Text;
            admin.Email = txtEmailAdmin.Text;
            admin.Cedula = txtCedulaAdmin.Text;
            admin.Rfc = txtRfcAdmin.Text;
            //admin.Foto = archivo.Replace(@"\", @"\\");
            admin.UsuarioAdmin = txtUsuarioAdmin.Text;
            admin.ContrasenaAdmin = txtContrasenaAdmin.Text;
        }

        /*private void btnAgregarAdmin_Click(object sender, EventArgs e)
        {
            //Metodo igualar campos en lugar de tenerlo programado en el boton mandamos llamar al metodo
            igualarCamposAdmin();

            if (admin.agregarAdministrativo())
            {
              
                mostrarEnGrid();
                if (admin.TipoUsuario == "Director")
                {
                    MessageBox.Show("El Director " + admin.Nombre + " " + admin.ApellidoPaterno + " ha sido agregado correctamente");
                }
                else if (admin.TipoUsuario == "Prefecto")
                {
                    MessageBox.Show("El Prefecto " + admin.Nombre + " " + admin.ApellidoPaterno + "ha sido agregado con exito");
                }
                else {
                    MessageBox.Show("El Administrativo " + admin.Nombre + " " + admin.ApellidoPaterno + " Ha sido Agregado correctamente");
                }

                limpiarDatosAdmin();
                ////////VOVLER A CARGAR DATOS EN GRID"""
            }
            else
                MessageBox.Show("Error al agregar el usuario Administrativo. Verifique los datos");

        }

        public void mostrarEnGrid()
        {
            //CREAN LAS COLUMNAS DEL DATAGRID
            //dgAlumnos.DataSource = null;

            DataTable llenarDatosAdmin = new DataTable();
            llenarDatosAdmin.Columns.Add("ColNumEmpleado");
            llenarDatosAdmin.Columns.Add("ColAp");
            llenarDatosAdmin.Columns.Add("ColAm");
            llenarDatosAdmin.Columns.Add("ColNom");
            llenarDatosAdmin.Columns.Add("ColFechaNac");
            llenarDatosAdmin.Columns.Add("ColTu");
            llenarDatosAdmin.Columns.Add("ColEmail");
            llenarDatosAdmin.Columns.Add("ColFoto").DataType = System.Type.GetType("System.Byte[]");
            llenarDatosAdmin.Columns.Add("colRuta");
            //IGUALAMOS LOS REGISTROS 
            mostrarInfoAdmin = mostrarAdministrativo.enlistarAdmin();

            for (int i = 0; i < mostrarInfoAdmin.Count; i++)
            {
                //COLOCAMOS REGISTROS EN SUS RESPECTIVOS RENGLONES
                DataRow renglones = llenarDatosAdmin.NewRow();
                renglones["ColNumEmpleado"] = mostrarInfoAdmin[i].NumEmpleado;
                renglones["ColAp"] = mostrarInfoAdmin[i].ApellidoPaterno;
                renglones["ColAm"] = mostrarInfoAdmin[i].ApellidoMaterno;
                renglones["ColNom"] = mostrarInfoAdmin[i].Nombre;
                renglones["ColFechaNac"] = mostrarInfoAdmin[i].FechaNac;
                renglones["ColTu"] = mostrarInfoAdmin[i].TipoUsuario;
                renglones["ColEmail"] = mostrarInfoAdmin[i].Email;
                try
                {
                    var imageConverter = new ImageConverter();
                    renglones["ColFoto"] = imageConverter.ConvertTo(Image.FromFile(mostrarInfoAdmin[i].Foto), System.Type.GetType("System.Byte[]"));
                    //pictureBox1.Image = Image.FromFile(mostrarInfoAlumno[i].Foto);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.ToString());
                }
                renglones["colRuta"] = mostrarInfoAdmin[i].Foto;


                //IGUALAR EL ULTIMO VALOR DE ID EN LA BASE DE DATOS
                lblContAdmin.Text = mostrarInfoAdmin[i].NumEmpleado.ToString();
                llenarDatosAdmin.Rows.Add(renglones);
            }
            //OBTENIENDO EL ULTIMO VALOR SE LE AGREGA 1 QUE SERIA EL SIGUIENTE USUARIO A INGRESAR
            //lblCont.Text = (int.Parse(lblCont.Text) + 1).ToString();
            //dgAlumnos.DataSource = null;

            dgAdministrativo.DataSource = llenarDatosAdmin;

            //AGREGAMOS NUEVO REGISTRO AL DATAGRID
        }

        private void FrmAdministrativo_Load(object sender, EventArgs e)
        {
            mostrarEnGrid();
            limpiarDatosAdmin();
        }

        private void pbFotoAdmin_Click(object sender, EventArgs e)
        {
            //para cargar imagenes en el pictureBox
            openFileDialog2.Filter = "image files (*.gif; *.jpg; *.png)| *.gif; *.jpg; *.png";
            openFileDialog2.ShowDialog();

            archivo = openFileDialog2.FileName;
            string directoryPath = Path.GetDirectoryName(openFileDialog2.FileName);
            //MessageBox.Show("archivo " + archivo.Length);
            if (directoryPath != "openFileDialog1")

                pbFotoAdmin.Image = (Bitmap)Bitmap.FromFile(openFileDialog2.FileName);
        }

        private void btnEliminarAdmin_Click(object sender, EventArgs e)
        {
            if (int.Parse(lblContAdmin.Text) > 0)
            {


                for (int i = 0; i < dgAdministrativo.Rows.Count; i++)
                {
                    if (int.Parse(lblContAdmin.Text) == int.Parse(dgAdministrativo[0, i].Value.ToString()))
                    {
                        if (MessageBox.Show("Se dispone a eliminar a " + mostrarInfoAdmin[i].ApellidoPaterno + " " + mostrarInfoAdmin[i].ApellidoMaterno + " " + mostrarInfoAdmin[i].Nombre + " ", "Eliminar Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            igualarCamposAdmin();
                            admin.eliminarAdministrativo();
                            dgAdministrativo.Rows.RemoveAt(i);
                            limpiarDatosAdmin();
                        }
                    }
                }
            }
        }

        private void btnModificarAdmin_Click(object sender, EventArgs e)
        {
            igualarCamposAdmin();
            admin.modificarAdministrativo();
            mostrarEnGrid();
            limpiarDatosAdmin();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmAlumno ventanaAlumno = new FrmAlumno();
            ventanaAlumno.ShowDialog();
        }

        private void btnDespMaestro_Click(object sender, EventArgs e)
        {
            FrmMaestro ventanaMaestro = new FrmMaestro();
            ventanaMaestro.ShowDialog();
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            FrmUsuarios ventanaUsuarios = new FrmUsuarios();
            ventanaUsuarios.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgAdministrativo_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            for (int i = 0; i < mostrarInfoAdmin.Count; i++)
            {
                if (Convert.ToInt64(dgAdministrativo.CurrentRow.Cells["ColNumEmpleado"].Value) == mostrarInfoAdmin[i].NumEmpleado)
                {
                    lblContAdmin.Text = dgAdministrativo[0, e.RowIndex].Value.ToString();
                    txtApAdmin.Text = dgAdministrativo[1, e.RowIndex].Value.ToString();
                    txtAmAdmin.Text = dgAdministrativo[2, e.RowIndex].Value.ToString();
                    txtNomAdmin.Text = dgAdministrativo[3, e.RowIndex].Value.ToString();
                    dtpFechaNacAdmin.Text = dgAdministrativo[4, e.RowIndex].Value.ToString();
                    cbTipoUsuario.Text = dgAdministrativo[5, e.RowIndex].Value.ToString();
                    txtEmailAdmin.Text = dgAdministrativo[6, e.RowIndex].Value.ToString();
                    txtDirAdmin.Text = mostrarInfoAdmin[i].Direccion;
                    txtTelAdmin.Text = mostrarInfoAdmin[i].Telefono;
                    txtCedulaAdmin.Text = mostrarInfoAdmin[i].Cedula;
                    txtRfcAdmin.Text = mostrarInfoAdmin[i].Rfc;
                    pbFotoAdmin.Image = (Bitmap)Bitmap.FromFile(mostrarInfoAdmin[i].Foto);
                    //archivo = dgAdministrativo[7, e.RowIndex].Value.ToString();
                    archivo = mostrarInfoAdmin[i].Foto;
                }
            }
        }

        private void dgAdministrativo_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgAdministrativo_CellClick(sender, e);
        }

        public void limpiarDatosAdmin()
        {
            txtApAdmin.Clear();
            txtAmAdmin.Clear();
            txtNomAdmin.Clear();
            dtpFechaNacAdmin.ResetText();
            txtDirAdmin.Clear();
            txtTelAdmin.Clear();
            txtEmailAdmin.Clear();
            cbTipoUsuario.Text = cbTipoUsuario.Items[0].ToString();
            txtCedulaAdmin.Clear();
            txtRfcAdmin.Clear();
            pbFotoAdmin.Image = (Bitmap)Image.FromFile(@"C:\Users\Work-Group\Desktop\Resources\Asministrativo.png");
            txtUsuarioAdmin.Clear();
            //txtConfirmContrasenaA.Clear();
            txtApAdmin.Focus();
        }

        private void btnLimpiarAdmin_Click(object sender, EventArgs e)
        {
            limpiarDatosAdmin();
        }

      */
    }
}
