﻿namespace School_Management
{
    partial class FrmAdministrativo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgAdministrativo = new System.Windows.Forms.DataGridView();
            this.ColNumEmpleado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFechaNac = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFoto = new System.Windows.Forms.DataGridViewImageColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAmAdmin = new System.Windows.Forms.TextBox();
            this.txtNomAdmin = new System.Windows.Forms.TextBox();
            this.txtRfcAdmin = new System.Windows.Forms.TextBox();
            this.txtDirAdmin = new System.Windows.Forms.TextBox();
            this.txtEmailAdmin = new System.Windows.Forms.TextBox();
            this.dtpFechaNacAdmin = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtApAdmin = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDespAlumno = new System.Windows.Forms.Button();
            this.btnDespMaestro = new System.Windows.Forms.Button();
            this.btnDespEvaluaciones = new System.Windows.Forms.Button();
            this.btnDespReportes = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtContrasenaAdmin = new System.Windows.Forms.TextBox();
            this.txtUsuarioAdmin = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCedulaAdmin = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbTipoUsuario = new System.Windows.Forms.ComboBox();
            this.txtTelAdmin = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pbFotoAdmin = new System.Windows.Forms.PictureBox();
            this.lblContAdmin = new System.Windows.Forms.Label();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.btnLimpiarAdmin = new System.Windows.Forms.Button();
            this.btnModificarAdmin = new System.Windows.Forms.Button();
            this.btnEliminarAdmin = new System.Windows.Forms.Button();
            this.btnAgregarAdmin = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAdministrativo)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoAdmin)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgAdministrativo);
            this.groupBox1.Location = new System.Drawing.Point(547, 333);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(795, 305);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Personal Administrativo";
            // 
            // dgAdministrativo
            // 
            this.dgAdministrativo.AllowUserToAddRows = false;
            this.dgAdministrativo.AllowUserToDeleteRows = false;
            this.dgAdministrativo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAdministrativo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColNumEmpleado,
            this.ColAp,
            this.ColAm,
            this.ColNom,
            this.ColFechaNac,
            this.ColTu,
            this.ColEmail,
            this.ColFoto});
            this.dgAdministrativo.Location = new System.Drawing.Point(6, 19);
            this.dgAdministrativo.Name = "dgAdministrativo";
            this.dgAdministrativo.ReadOnly = true;
            this.dgAdministrativo.Size = new System.Drawing.Size(783, 280);
            this.dgAdministrativo.TabIndex = 11;
            /*this.dgAdministrativo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAdministrativo_CellClick);
            this.dgAdministrativo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dgAdministrativo.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAdministrativo_CellEnter);*/
            // 
            // ColNumEmpleado
            // 
            this.ColNumEmpleado.DataPropertyName = "ColNumEmpleado";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Silver;
            this.ColNumEmpleado.DefaultCellStyle = dataGridViewCellStyle5;
            this.ColNumEmpleado.HeaderText = "Num Empleado";
            this.ColNumEmpleado.Name = "ColNumEmpleado";
            this.ColNumEmpleado.ReadOnly = true;
            this.ColNumEmpleado.Width = 61;
            // 
            // ColAp
            // 
            this.ColAp.DataPropertyName = "ColAp";
            this.ColAp.HeaderText = "Apellido Paterno";
            this.ColAp.Name = "ColAp";
            this.ColAp.ReadOnly = true;
            this.ColAp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColAm
            // 
            this.ColAm.DataPropertyName = "ColAm";
            this.ColAm.HeaderText = "Apellido Materno";
            this.ColAm.Name = "ColAm";
            this.ColAm.ReadOnly = true;
            this.ColAm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColNom
            // 
            this.ColNom.DataPropertyName = "ColNom";
            this.ColNom.HeaderText = "Nombre";
            this.ColNom.Name = "ColNom";
            this.ColNom.ReadOnly = true;
            // 
            // ColFechaNac
            // 
            this.ColFechaNac.DataPropertyName = "ColFechaNac";
            this.ColFechaNac.HeaderText = "Fecha Nacimiento";
            this.ColFechaNac.Name = "ColFechaNac";
            this.ColFechaNac.ReadOnly = true;
            // 
            // ColTu
            // 
            this.ColTu.DataPropertyName = "ColTu";
            this.ColTu.HeaderText = "Tipo de Usuario";
            this.ColTu.Name = "ColTu";
            this.ColTu.ReadOnly = true;
            this.ColTu.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColEmail
            // 
            this.ColEmail.DataPropertyName = "ColEmail";
            this.ColEmail.HeaderText = "Correo Electronico";
            this.ColEmail.Name = "ColEmail";
            this.ColEmail.ReadOnly = true;
            this.ColEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColFoto
            // 
            this.ColFoto.DataPropertyName = "ColFoto";
            this.ColFoto.HeaderText = "Foto";
            this.ColFoto.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.ColFoto.Name = "ColFoto";
            this.ColFoto.ReadOnly = true;
            this.ColFoto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColFoto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColFoto.Width = 50;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Apellido Materno:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Fecha Nacimiento:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Dirección:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Email:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(81, 269);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "RFC:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(284, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(209, 15);
            this.label7.TabIndex = 15;
            this.label7.Text = "Seleccionar Imagen de Usuario";
            // 
            // txtAmAdmin
            // 
            this.txtAmAdmin.Location = new System.Drawing.Point(126, 57);
            this.txtAmAdmin.Name = "txtAmAdmin";
            this.txtAmAdmin.Size = new System.Drawing.Size(100, 20);
            this.txtAmAdmin.TabIndex = 2;
            // 
            // txtNomAdmin
            // 
            this.txtNomAdmin.Location = new System.Drawing.Point(126, 83);
            this.txtNomAdmin.Name = "txtNomAdmin";
            this.txtNomAdmin.Size = new System.Drawing.Size(100, 20);
            this.txtNomAdmin.TabIndex = 3;
            // 
            // txtRfcAdmin
            // 
            this.txtRfcAdmin.Location = new System.Drawing.Point(126, 266);
            this.txtRfcAdmin.Name = "txtRfcAdmin";
            this.txtRfcAdmin.Size = new System.Drawing.Size(123, 20);
            this.txtRfcAdmin.TabIndex = 10;
            // 
            // txtDirAdmin
            // 
            this.txtDirAdmin.Location = new System.Drawing.Point(126, 135);
            this.txtDirAdmin.Name = "txtDirAdmin";
            this.txtDirAdmin.Size = new System.Drawing.Size(100, 20);
            this.txtDirAdmin.TabIndex = 5;
            // 
            // txtEmailAdmin
            // 
            this.txtEmailAdmin.Location = new System.Drawing.Point(126, 187);
            this.txtEmailAdmin.Name = "txtEmailAdmin";
            this.txtEmailAdmin.Size = new System.Drawing.Size(123, 20);
            this.txtEmailAdmin.TabIndex = 7;
            // 
            // dtpFechaNacAdmin
            // 
            this.dtpFechaNacAdmin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacAdmin.Location = new System.Drawing.Point(126, 109);
            this.dtpFechaNacAdmin.Name = "dtpFechaNacAdmin";
            this.dtpFechaNacAdmin.Size = new System.Drawing.Size(100, 20);
            this.dtpFechaNacAdmin.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Apellido Paterno:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 405);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 24;
            // 
            // txtApAdmin
            // 
            this.txtApAdmin.Location = new System.Drawing.Point(126, 31);
            this.txtApAdmin.Name = "txtApAdmin";
            this.txtApAdmin.Size = new System.Drawing.Size(100, 20);
            this.txtApAdmin.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnDespAlumno);
            this.groupBox2.Controls.Add(this.btnDespMaestro);
            this.groupBox2.Controls.Add(this.btnDespEvaluaciones);
            this.groupBox2.Controls.Add(this.btnDespReportes);
            this.groupBox2.Location = new System.Drawing.Point(115, 60);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(427, 138);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Procesos Administrativos";
            // 
            // btnDespAlumno
            // 
            this.btnDespAlumno.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnDespAlumno.BackgroundImage = global::EscolarSoft.Properties.Resources.alumno;
            this.btnDespAlumno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDespAlumno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDespAlumno.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnDespAlumno.Location = new System.Drawing.Point(20, 27);
            this.btnDespAlumno.Name = "btnDespAlumno";
            this.btnDespAlumno.Size = new System.Drawing.Size(92, 90);
            this.btnDespAlumno.TabIndex = 3;
            this.btnDespAlumno.Text = "Alumno";
            this.btnDespAlumno.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDespAlumno.UseVisualStyleBackColor = false;
            //this.btnDespAlumno.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnDespMaestro
            // 
            this.btnDespMaestro.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnDespMaestro.BackgroundImage = global::EscolarSoft.Properties.Resources.maestra1;
            this.btnDespMaestro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDespMaestro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDespMaestro.ForeColor = System.Drawing.Color.White;
            this.btnDespMaestro.Location = new System.Drawing.Point(118, 27);
            this.btnDespMaestro.Name = "btnDespMaestro";
            this.btnDespMaestro.Size = new System.Drawing.Size(93, 90);
            this.btnDespMaestro.TabIndex = 0;
            this.btnDespMaestro.Text = "Maestro";
            this.btnDespMaestro.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDespMaestro.UseVisualStyleBackColor = false;
            //this.btnDespMaestro.Click += new System.EventHandler(this.btnDespMaestro_Click);
            // 
            // btnDespEvaluaciones
            // 
            this.btnDespEvaluaciones.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnDespEvaluaciones.BackgroundImage = global::EscolarSoft.Properties.Resources.evaluaciones2;
            this.btnDespEvaluaciones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDespEvaluaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDespEvaluaciones.ForeColor = System.Drawing.Color.White;
            this.btnDespEvaluaciones.Location = new System.Drawing.Point(217, 27);
            this.btnDespEvaluaciones.Name = "btnDespEvaluaciones";
            this.btnDespEvaluaciones.Size = new System.Drawing.Size(94, 90);
            this.btnDespEvaluaciones.TabIndex = 1;
            this.btnDespEvaluaciones.Text = "Evaluaciones";
            this.btnDespEvaluaciones.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDespEvaluaciones.UseVisualStyleBackColor = false;
            // 
            // btnDespReportes
            // 
            this.btnDespReportes.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnDespReportes.BackgroundImage = global::EscolarSoft.Properties.Resources.reportes;
            this.btnDespReportes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDespReportes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDespReportes.ForeColor = System.Drawing.Color.White;
            this.btnDespReportes.Location = new System.Drawing.Point(317, 27);
            this.btnDespReportes.Name = "btnDespReportes";
            this.btnDespReportes.Size = new System.Drawing.Size(92, 90);
            this.btnDespReportes.TabIndex = 2;
            this.btnDespReportes.Text = "Reportes";
            this.btnDespReportes.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDespReportes.UseVisualStyleBackColor = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txtContrasenaAdmin);
            this.groupBox3.Controls.Add(this.txtUsuarioAdmin);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtCedulaAdmin);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.cbTipoUsuario);
            this.groupBox3.Controls.Add(this.txtTelAdmin);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtAmAdmin);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtApAdmin);
            this.groupBox3.Controls.Add(this.pbFotoAdmin);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.dtpFechaNacAdmin);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtEmailAdmin);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtDirAdmin);
            this.groupBox3.Controls.Add(this.txtNomAdmin);
            this.groupBox3.Controls.Add(this.txtRfcAdmin);
            this.groupBox3.Location = new System.Drawing.Point(31, 333);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(510, 305);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informacion de personal Administrativo";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(295, 203);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(178, 15);
            this.label15.TabIndex = 38;
            this.label15.Text = "Asignar Cuenta de Usuario";
            // 
            // txtContrasenaAdmin
            // 
            this.txtContrasenaAdmin.Location = new System.Drawing.Point(341, 263);
            this.txtContrasenaAdmin.Name = "txtContrasenaAdmin";
            this.txtContrasenaAdmin.Size = new System.Drawing.Size(113, 20);
            this.txtContrasenaAdmin.TabIndex = 36;
            // 
            // txtUsuarioAdmin
            // 
            this.txtUsuarioAdmin.Location = new System.Drawing.Point(341, 237);
            this.txtUsuarioAdmin.Name = "txtUsuarioAdmin";
            this.txtUsuarioAdmin.Size = new System.Drawing.Size(113, 20);
            this.txtUsuarioAdmin.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(271, 266);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Contraseña:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(295, 240);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Usurio:";
            // 
            // txtCedulaAdmin
            // 
            this.txtCedulaAdmin.Location = new System.Drawing.Point(126, 240);
            this.txtCedulaAdmin.Name = "txtCedulaAdmin";
            this.txtCedulaAdmin.Size = new System.Drawing.Size(123, 20);
            this.txtCedulaAdmin.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 244);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Cédula Profesional:";
            // 
            // cbTipoUsuario
            // 
            this.cbTipoUsuario.FormattingEnabled = true;
            this.cbTipoUsuario.Items.AddRange(new object[] {
            "",
            "Director",
            "Administrativo",
            "Prefecto",
            "Maestro"});
            this.cbTipoUsuario.Location = new System.Drawing.Point(126, 213);
            this.cbTipoUsuario.Name = "cbTipoUsuario";
            this.cbTipoUsuario.Size = new System.Drawing.Size(123, 21);
            this.cbTipoUsuario.TabIndex = 8;
            // 
            // txtTelAdmin
            // 
            this.txtTelAdmin.Location = new System.Drawing.Point(126, 161);
            this.txtTelAdmin.Name = "txtTelAdmin";
            this.txtTelAdmin.Size = new System.Drawing.Size(100, 20);
            this.txtTelAdmin.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(60, 166);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Teléfono:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 216);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Tipo de Usuario:";
            // 
            // pbFotoAdmin
            // 
            this.pbFotoAdmin.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.pbFotoAdmin.Image = global::EscolarSoft.Properties.Resources.Asministrativo;
            this.pbFotoAdmin.Location = new System.Drawing.Point(321, 34);
            this.pbFotoAdmin.Name = "pbFotoAdmin";
            this.pbFotoAdmin.Size = new System.Drawing.Size(133, 147);
            this.pbFotoAdmin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFotoAdmin.TabIndex = 16;
            this.pbFotoAdmin.TabStop = false;
            //this.pbFotoAdmin.Click += new System.EventHandler(this.pbFotoAdmin_Click);
            // 
            // lblContAdmin
            // 
            this.lblContAdmin.AutoSize = true;
            this.lblContAdmin.Location = new System.Drawing.Point(1266, 656);
            this.lblContAdmin.Name = "lblContAdmin";
            this.lblContAdmin.Size = new System.Drawing.Size(41, 13);
            this.lblContAdmin.TabIndex = 28;
            this.lblContAdmin.Text = "label10";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // btnLimpiarAdmin
            // 
            this.btnLimpiarAdmin.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnLimpiarAdmin.BackgroundImage = global::EscolarSoft.Properties.Resources.limpiar;
            this.btnLimpiarAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLimpiarAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiarAdmin.ForeColor = System.Drawing.Color.White;
            this.btnLimpiarAdmin.Location = new System.Drawing.Point(1067, 241);
            this.btnLimpiarAdmin.Name = "btnLimpiarAdmin";
            this.btnLimpiarAdmin.Size = new System.Drawing.Size(75, 69);
            this.btnLimpiarAdmin.TabIndex = 8;
            this.btnLimpiarAdmin.Text = "Limpiar";
            this.btnLimpiarAdmin.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLimpiarAdmin.UseVisualStyleBackColor = false;
            //this.btnLimpiarAdmin.Click += new System.EventHandler(this.btnLimpiarAdmin_Click);
            // 
            // btnModificarAdmin
            // 
            this.btnModificarAdmin.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnModificarAdmin.BackgroundImage = global::EscolarSoft.Properties.Resources.modificarAlumno;
            this.btnModificarAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnModificarAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarAdmin.ForeColor = System.Drawing.Color.White;
            this.btnModificarAdmin.Location = new System.Drawing.Point(919, 241);
            this.btnModificarAdmin.Name = "btnModificarAdmin";
            this.btnModificarAdmin.Size = new System.Drawing.Size(75, 69);
            this.btnModificarAdmin.TabIndex = 7;
            this.btnModificarAdmin.Text = "Actuallizar";
            this.btnModificarAdmin.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnModificarAdmin.UseVisualStyleBackColor = false;
            //this.btnModificarAdmin.Click += new System.EventHandler(this.btnModificarAdmin_Click);
            // 
            // btnEliminarAdmin
            // 
            this.btnEliminarAdmin.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnEliminarAdmin.BackgroundImage = global::EscolarSoft.Properties.Resources.eliminarEstudiante;
            this.btnEliminarAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminarAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarAdmin.ForeColor = System.Drawing.Color.White;
            this.btnEliminarAdmin.Location = new System.Drawing.Point(826, 241);
            this.btnEliminarAdmin.Name = "btnEliminarAdmin";
            this.btnEliminarAdmin.Size = new System.Drawing.Size(75, 69);
            this.btnEliminarAdmin.TabIndex = 6;
            this.btnEliminarAdmin.Text = "Eliminar";
            this.btnEliminarAdmin.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnEliminarAdmin.UseVisualStyleBackColor = false;
            //this.btnEliminarAdmin.Click += new System.EventHandler(this.btnEliminarAdmin_Click);
            // 
            // btnAgregarAdmin
            // 
            this.btnAgregarAdmin.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnAgregarAdmin.BackgroundImage = global::EscolarSoft.Properties.Resources.agregrAdmin;
            this.btnAgregarAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAgregarAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarAdmin.ForeColor = System.Drawing.Color.White;
            this.btnAgregarAdmin.Location = new System.Drawing.Point(731, 241);
            this.btnAgregarAdmin.Name = "btnAgregarAdmin";
            this.btnAgregarAdmin.Size = new System.Drawing.Size(75, 69);
            this.btnAgregarAdmin.TabIndex = 5;
            this.btnAgregarAdmin.Text = "Agregar";
            this.btnAgregarAdmin.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAgregarAdmin.UseVisualStyleBackColor = false;
            //this.btnAgregarAdmin.Click += new System.EventHandler(this.btnAgregarAdmin_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 643);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1354, 22);
            this.statusStrip1.TabIndex = 30;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(140, 17);
            this.toolStripStatusLabel1.Text = "Procesos Administrativos";
            // 
            // FrmAdministrativo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1354, 665);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblContAdmin);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnLimpiarAdmin);
            this.Controls.Add(this.btnModificarAdmin);
            this.Controls.Add(this.btnEliminarAdmin);
            this.Controls.Add(this.btnAgregarAdmin);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmAdministrativo";
            this.Text = "Registro Escolar";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            //this.Load += new System.EventHandler(this.FrmAdministrativo_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgAdministrativo)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoAdmin)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDespMaestro;
        private System.Windows.Forms.Button btnDespEvaluaciones;
        private System.Windows.Forms.Button btnDespReportes;
        private System.Windows.Forms.Button btnDespAlumno;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgAdministrativo;
        private System.Windows.Forms.Button btnAgregarAdmin;
        private System.Windows.Forms.Button btnEliminarAdmin;
        private System.Windows.Forms.Button btnModificarAdmin;
        private System.Windows.Forms.Button btnLimpiarAdmin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pbFotoAdmin;
        private System.Windows.Forms.TextBox txtAmAdmin;
        private System.Windows.Forms.TextBox txtNomAdmin;
        private System.Windows.Forms.TextBox txtRfcAdmin;
        private System.Windows.Forms.TextBox txtDirAdmin;
        private System.Windows.Forms.TextBox txtEmailAdmin;
        private System.Windows.Forms.DateTimePicker dtpFechaNacAdmin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtApAdmin;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblContAdmin;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.ComboBox cbTipoUsuario;
        private System.Windows.Forms.TextBox txtTelAdmin;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCedulaAdmin;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNumEmpleado;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAp;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAm;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFechaNac;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTu;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmail;
        private System.Windows.Forms.DataGridViewImageColumn ColFoto;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtContrasenaAdmin;
        private System.Windows.Forms.TextBox txtUsuarioAdmin;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}