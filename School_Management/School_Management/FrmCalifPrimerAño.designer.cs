﻿namespace School_Management
{
    partial class FrmCalifPrimerAño
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCalifPrimerAño));
            this.btnGuardarCalif1a = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.lblPromFinal1a = new System.Windows.Forms.Label();
            this.btnPromFinal1a = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblPromedioHistoria1a = new System.Windows.Forms.Label();
            this.btnPromediarHist1a = new System.Windows.Forms.Button();
            this.lblPromedioCE1a = new System.Windows.Forms.Label();
            this.btnPromediarCE1a = new System.Windows.Forms.Button();
            this.lblPromedioFisica1a = new System.Windows.Forms.Label();
            this.btnPromediarFis1a = new System.Windows.Forms.Button();
            this.lblPromedioQuimica1a = new System.Windows.Forms.Label();
            this.btnPromediarQuim1a = new System.Windows.Forms.Button();
            this.lblPromedioMat1a = new System.Windows.Forms.Label();
            this.btnPromediarMat1a = new System.Windows.Forms.Button();
            this.lblPromedioEspaañol1a = new System.Windows.Forms.Label();
            this.btnPromediarEsp1a = new System.Windows.Forms.Button();
            this.txtCalifHistoriaB3 = new System.Windows.Forms.TextBox();
            this.txtCalifCEB3 = new System.Windows.Forms.TextBox();
            this.txtCalifFisicaB3 = new System.Windows.Forms.TextBox();
            this.txtCalifQuimicaB3 = new System.Windows.Forms.TextBox();
            this.txtCalifMatB3 = new System.Windows.Forms.TextBox();
            this.txtCalifEspañolB3 = new System.Windows.Forms.TextBox();
            this.txtCalifHistoriaB2 = new System.Windows.Forms.TextBox();
            this.txtCalifCEB2 = new System.Windows.Forms.TextBox();
            this.txtCalifFisicaB2 = new System.Windows.Forms.TextBox();
            this.txtCalifQuimicaB2 = new System.Windows.Forms.TextBox();
            this.txtCalifMatB2 = new System.Windows.Forms.TextBox();
            this.txtCalifEspañolB2 = new System.Windows.Forms.TextBox();
            this.txtCalifHistoriaB1 = new System.Windows.Forms.TextBox();
            this.txtCalifCEB1 = new System.Windows.Forms.TextBox();
            this.txtCalifFisicaB1 = new System.Windows.Forms.TextBox();
            this.txtCalifQuimicaB1 = new System.Windows.Forms.TextBox();
            this.txtCalifMatB1 = new System.Windows.Forms.TextBox();
            this.txtCalifEspañolB1 = new System.Windows.Forms.TextBox();
            this.lblHistoria = new System.Windows.Forms.Label();
            this.lblFce = new System.Windows.Forms.Label();
            this.lblFisica = new System.Windows.Forms.Label();
            this.lblQuimica = new System.Windows.Forms.Label();
            this.lblMatematicas = new System.Windows.Forms.Label();
            this.lblEspañol = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNomMaestro = new System.Windows.Forms.TextBox();
            this.txtNumEmpleado = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgPrimerAño = new System.Windows.Forms.DataGridView();
            this.cbSelecGrupo = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnConsulAlumCalif = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMatricula = new System.Windows.Forms.TextBox();
            this.txtNombreAlum = new System.Windows.Forms.TextBox();
            this.ColMatricula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNomAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colApePat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colApeMat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtApPatAlum = new System.Windows.Forms.TextBox();
            this.txtApMatAlum = new System.Windows.Forms.TextBox();
            this.txtApMatMaestro = new System.Windows.Forms.TextBox();
            this.txtApPatMaestro = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnBuscarMaestro = new System.Windows.Forms.Button();
            this.btnGuardarEspa = new System.Windows.Forms.Button();
            this.btnGuardarMatematicas = new System.Windows.Forms.Button();
            this.btnGuardarQuimica = new System.Windows.Forms.Button();
            this.btnGuardarFisica = new System.Windows.Forms.Button();
            this.btnGuardarCE = new System.Windows.Forms.Button();
            this.btnGuardarHistoria = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrimerAño)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGuardarCalif1a
            // 
            this.btnGuardarCalif1a.Location = new System.Drawing.Point(191, 609);
            this.btnGuardarCalif1a.Name = "btnGuardarCalif1a";
            this.btnGuardarCalif1a.Size = new System.Drawing.Size(136, 43);
            this.btnGuardarCalif1a.TabIndex = 31;
            this.btnGuardarCalif1a.Text = "Guardar";
            this.btnGuardarCalif1a.UseVisualStyleBackColor = true;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(235, 209);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(43, 15);
            this.label51.TabIndex = 292;
            this.label51.Text = "1 Año";
            // 
            // lblPromFinal1a
            // 
            this.lblPromFinal1a.AutoSize = true;
            this.lblPromFinal1a.Location = new System.Drawing.Point(293, 583);
            this.lblPromFinal1a.Name = "lblPromFinal1a";
            this.lblPromFinal1a.Size = new System.Drawing.Size(22, 13);
            this.lblPromFinal1a.TabIndex = 291;
            this.lblPromFinal1a.Text = "0.0";
            // 
            // btnPromFinal1a
            // 
            this.btnPromFinal1a.Location = new System.Drawing.Point(191, 576);
            this.btnPromFinal1a.Name = "btnPromFinal1a";
            this.btnPromFinal1a.Size = new System.Drawing.Size(75, 27);
            this.btnPromFinal1a.TabIndex = 30;
            this.btnPromFinal1a.Text = "PromFinal";
            this.btnPromFinal1a.UseVisualStyleBackColor = true;
            this.btnPromFinal1a.Click += new System.EventHandler(this.btnPromFinal1a_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(293, 230);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(30, 13);
            this.label29.TabIndex = 289;
            this.label29.Text = "3Bim";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(249, 230);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(30, 13);
            this.label28.TabIndex = 288;
            this.label28.Text = "2Bim";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(198, 230);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 13);
            this.label27.TabIndex = 287;
            this.label27.Text = "1Bim";
            // 
            // lblPromedioHistoria1a
            // 
            this.lblPromedioHistoria1a.AutoSize = true;
            this.lblPromedioHistoria1a.Location = new System.Drawing.Point(293, 552);
            this.lblPromedioHistoria1a.Name = "lblPromedioHistoria1a";
            this.lblPromedioHistoria1a.Size = new System.Drawing.Size(22, 13);
            this.lblPromedioHistoria1a.TabIndex = 286;
            this.lblPromedioHistoria1a.Text = "0.0";
            // 
            // btnPromediarHist1a
            // 
            this.btnPromediarHist1a.Location = new System.Drawing.Point(191, 547);
            this.btnPromediarHist1a.Name = "btnPromediarHist1a";
            this.btnPromediarHist1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarHist1a.TabIndex = 29;
            this.btnPromediarHist1a.Text = "Promediar";
            this.btnPromediarHist1a.UseVisualStyleBackColor = true;
            this.btnPromediarHist1a.Click += new System.EventHandler(this.btnPromediarHist1a_Click);
            // 
            // lblPromedioCE1a
            // 
            this.lblPromedioCE1a.AutoSize = true;
            this.lblPromedioCE1a.Location = new System.Drawing.Point(293, 497);
            this.lblPromedioCE1a.Name = "lblPromedioCE1a";
            this.lblPromedioCE1a.Size = new System.Drawing.Size(22, 13);
            this.lblPromedioCE1a.TabIndex = 284;
            this.lblPromedioCE1a.Text = "0.0";
            // 
            // btnPromediarCE1a
            // 
            this.btnPromediarCE1a.Location = new System.Drawing.Point(191, 492);
            this.btnPromediarCE1a.Name = "btnPromediarCE1a";
            this.btnPromediarCE1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarCE1a.TabIndex = 25;
            this.btnPromediarCE1a.Text = "Promediar";
            this.btnPromediarCE1a.UseVisualStyleBackColor = true;
            this.btnPromediarCE1a.Click += new System.EventHandler(this.btnPromediarCE1a_Click);
            // 
            // lblPromedioFisica1a
            // 
            this.lblPromedioFisica1a.AutoSize = true;
            this.lblPromedioFisica1a.Location = new System.Drawing.Point(293, 442);
            this.lblPromedioFisica1a.Name = "lblPromedioFisica1a";
            this.lblPromedioFisica1a.Size = new System.Drawing.Size(22, 13);
            this.lblPromedioFisica1a.TabIndex = 282;
            this.lblPromedioFisica1a.Text = "0.0";
            // 
            // btnPromediarFis1a
            // 
            this.btnPromediarFis1a.Location = new System.Drawing.Point(191, 437);
            this.btnPromediarFis1a.Name = "btnPromediarFis1a";
            this.btnPromediarFis1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarFis1a.TabIndex = 21;
            this.btnPromediarFis1a.Text = "Promediar";
            this.btnPromediarFis1a.UseVisualStyleBackColor = true;
            this.btnPromediarFis1a.Click += new System.EventHandler(this.btnPromediarFis1a_Click);
            // 
            // lblPromedioQuimica1a
            // 
            this.lblPromedioQuimica1a.AutoSize = true;
            this.lblPromedioQuimica1a.Location = new System.Drawing.Point(293, 387);
            this.lblPromedioQuimica1a.Name = "lblPromedioQuimica1a";
            this.lblPromedioQuimica1a.Size = new System.Drawing.Size(22, 13);
            this.lblPromedioQuimica1a.TabIndex = 280;
            this.lblPromedioQuimica1a.Text = "0.0";
            // 
            // btnPromediarQuim1a
            // 
            this.btnPromediarQuim1a.Location = new System.Drawing.Point(191, 382);
            this.btnPromediarQuim1a.Name = "btnPromediarQuim1a";
            this.btnPromediarQuim1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarQuim1a.TabIndex = 17;
            this.btnPromediarQuim1a.Text = "Promediar";
            this.btnPromediarQuim1a.UseVisualStyleBackColor = true;
            this.btnPromediarQuim1a.Click += new System.EventHandler(this.btnPromediarQuim1a_Click);
            // 
            // lblPromedioMat1a
            // 
            this.lblPromedioMat1a.AutoSize = true;
            this.lblPromedioMat1a.Location = new System.Drawing.Point(293, 332);
            this.lblPromedioMat1a.Name = "lblPromedioMat1a";
            this.lblPromedioMat1a.Size = new System.Drawing.Size(22, 13);
            this.lblPromedioMat1a.TabIndex = 278;
            this.lblPromedioMat1a.Text = "0.0";
            // 
            // btnPromediarMat1a
            // 
            this.btnPromediarMat1a.Location = new System.Drawing.Point(191, 327);
            this.btnPromediarMat1a.Name = "btnPromediarMat1a";
            this.btnPromediarMat1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarMat1a.TabIndex = 13;
            this.btnPromediarMat1a.Text = "Promediar";
            this.btnPromediarMat1a.UseVisualStyleBackColor = true;
            this.btnPromediarMat1a.Click += new System.EventHandler(this.btnPromediarMat1a_Click);
            // 
            // lblPromedioEspaañol1a
            // 
            this.lblPromedioEspaañol1a.AutoSize = true;
            this.lblPromedioEspaañol1a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPromedioEspaañol1a.Location = new System.Drawing.Point(293, 277);
            this.lblPromedioEspaañol1a.Name = "lblPromedioEspaañol1a";
            this.lblPromedioEspaañol1a.Size = new System.Drawing.Size(22, 13);
            this.lblPromedioEspaañol1a.TabIndex = 276;
            this.lblPromedioEspaañol1a.Text = "0.0";
            // 
            // btnPromediarEsp1a
            // 
            this.btnPromediarEsp1a.Location = new System.Drawing.Point(191, 272);
            this.btnPromediarEsp1a.Name = "btnPromediarEsp1a";
            this.btnPromediarEsp1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarEsp1a.TabIndex = 9;
            this.btnPromediarEsp1a.Text = " Promediar";
            this.btnPromediarEsp1a.UseVisualStyleBackColor = true;
            this.btnPromediarEsp1a.Click += new System.EventHandler(this.btnPromediarEsp1a_Click);
            // 
            // txtCalifHistoriaB3
            // 
            this.txtCalifHistoriaB3.Location = new System.Drawing.Point(285, 521);
            this.txtCalifHistoriaB3.Name = "txtCalifHistoriaB3";
            this.txtCalifHistoriaB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifHistoriaB3.TabIndex = 28;
            this.txtCalifHistoriaB3.Text = "0";
            // 
            // txtCalifCEB3
            // 
            this.txtCalifCEB3.Location = new System.Drawing.Point(285, 466);
            this.txtCalifCEB3.Name = "txtCalifCEB3";
            this.txtCalifCEB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifCEB3.TabIndex = 24;
            this.txtCalifCEB3.Text = "0";
            // 
            // txtCalifFisicaB3
            // 
            this.txtCalifFisicaB3.Location = new System.Drawing.Point(285, 411);
            this.txtCalifFisicaB3.Name = "txtCalifFisicaB3";
            this.txtCalifFisicaB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifFisicaB3.TabIndex = 20;
            this.txtCalifFisicaB3.Text = "0";
            // 
            // txtCalifQuimicaB3
            // 
            this.txtCalifQuimicaB3.Location = new System.Drawing.Point(285, 356);
            this.txtCalifQuimicaB3.Name = "txtCalifQuimicaB3";
            this.txtCalifQuimicaB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifQuimicaB3.TabIndex = 16;
            this.txtCalifQuimicaB3.Text = "0";
            // 
            // txtCalifMatB3
            // 
            this.txtCalifMatB3.Location = new System.Drawing.Point(285, 302);
            this.txtCalifMatB3.Name = "txtCalifMatB3";
            this.txtCalifMatB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifMatB3.TabIndex = 12;
            this.txtCalifMatB3.Text = "0";
            // 
            // txtCalifEspañolB3
            // 
            this.txtCalifEspañolB3.Location = new System.Drawing.Point(285, 246);
            this.txtCalifEspañolB3.Name = "txtCalifEspañolB3";
            this.txtCalifEspañolB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifEspañolB3.TabIndex = 8;
            this.txtCalifEspañolB3.Text = "0";
            this.txtCalifEspañolB3.TextChanged += new System.EventHandler(this.txtCalifEspañolB3_TextChanged);
            // 
            // txtCalifHistoriaB2
            // 
            this.txtCalifHistoriaB2.Location = new System.Drawing.Point(238, 521);
            this.txtCalifHistoriaB2.Name = "txtCalifHistoriaB2";
            this.txtCalifHistoriaB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifHistoriaB2.TabIndex = 27;
            this.txtCalifHistoriaB2.Text = "0";
            // 
            // txtCalifCEB2
            // 
            this.txtCalifCEB2.Location = new System.Drawing.Point(238, 466);
            this.txtCalifCEB2.Name = "txtCalifCEB2";
            this.txtCalifCEB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifCEB2.TabIndex = 23;
            this.txtCalifCEB2.Text = "0";
            // 
            // txtCalifFisicaB2
            // 
            this.txtCalifFisicaB2.Location = new System.Drawing.Point(238, 411);
            this.txtCalifFisicaB2.Name = "txtCalifFisicaB2";
            this.txtCalifFisicaB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifFisicaB2.TabIndex = 19;
            this.txtCalifFisicaB2.Text = "0";
            // 
            // txtCalifQuimicaB2
            // 
            this.txtCalifQuimicaB2.Location = new System.Drawing.Point(238, 356);
            this.txtCalifQuimicaB2.Name = "txtCalifQuimicaB2";
            this.txtCalifQuimicaB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifQuimicaB2.TabIndex = 15;
            this.txtCalifQuimicaB2.Text = "0";
            // 
            // txtCalifMatB2
            // 
            this.txtCalifMatB2.Location = new System.Drawing.Point(238, 302);
            this.txtCalifMatB2.Name = "txtCalifMatB2";
            this.txtCalifMatB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifMatB2.TabIndex = 11;
            this.txtCalifMatB2.Text = "0";
            // 
            // txtCalifEspañolB2
            // 
            this.txtCalifEspañolB2.Location = new System.Drawing.Point(238, 246);
            this.txtCalifEspañolB2.Name = "txtCalifEspañolB2";
            this.txtCalifEspañolB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifEspañolB2.TabIndex = 7;
            this.txtCalifEspañolB2.Text = "0";
            // 
            // txtCalifHistoriaB1
            // 
            this.txtCalifHistoriaB1.Location = new System.Drawing.Point(191, 521);
            this.txtCalifHistoriaB1.Name = "txtCalifHistoriaB1";
            this.txtCalifHistoriaB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifHistoriaB1.TabIndex = 26;
            this.txtCalifHistoriaB1.Text = "0";
            // 
            // txtCalifCEB1
            // 
            this.txtCalifCEB1.Location = new System.Drawing.Point(191, 466);
            this.txtCalifCEB1.Name = "txtCalifCEB1";
            this.txtCalifCEB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifCEB1.TabIndex = 22;
            this.txtCalifCEB1.Text = "0";
            // 
            // txtCalifFisicaB1
            // 
            this.txtCalifFisicaB1.Location = new System.Drawing.Point(191, 411);
            this.txtCalifFisicaB1.Name = "txtCalifFisicaB1";
            this.txtCalifFisicaB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifFisicaB1.TabIndex = 18;
            this.txtCalifFisicaB1.Text = "0";
            // 
            // txtCalifQuimicaB1
            // 
            this.txtCalifQuimicaB1.Location = new System.Drawing.Point(191, 356);
            this.txtCalifQuimicaB1.Name = "txtCalifQuimicaB1";
            this.txtCalifQuimicaB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifQuimicaB1.TabIndex = 14;
            this.txtCalifQuimicaB1.Text = "0";
            // 
            // txtCalifMatB1
            // 
            this.txtCalifMatB1.Location = new System.Drawing.Point(191, 301);
            this.txtCalifMatB1.Name = "txtCalifMatB1";
            this.txtCalifMatB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifMatB1.TabIndex = 10;
            this.txtCalifMatB1.Text = "0";
            // 
            // txtCalifEspañolB1
            // 
            this.txtCalifEspañolB1.Location = new System.Drawing.Point(191, 246);
            this.txtCalifEspañolB1.Name = "txtCalifEspañolB1";
            this.txtCalifEspañolB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifEspañolB1.TabIndex = 6;
            this.txtCalifEspañolB1.Text = "0";
            // 
            // lblHistoria
            // 
            this.lblHistoria.AutoSize = true;
            this.lblHistoria.Location = new System.Drawing.Point(141, 526);
            this.lblHistoria.Name = "lblHistoria";
            this.lblHistoria.Size = new System.Drawing.Size(42, 13);
            this.lblHistoria.TabIndex = 256;
            this.lblHistoria.Text = "Historia";
            // 
            // lblFce
            // 
            this.lblFce.AutoSize = true;
            this.lblFce.Location = new System.Drawing.Point(90, 469);
            this.lblFce.Name = "lblFce";
            this.lblFce.Size = new System.Drawing.Size(101, 13);
            this.lblFce.TabIndex = 255;
            this.lblFce.Text = "Formacion Civ. Etic.";
            // 
            // lblFisica
            // 
            this.lblFisica.AutoSize = true;
            this.lblFisica.Location = new System.Drawing.Point(141, 414);
            this.lblFisica.Name = "lblFisica";
            this.lblFisica.Size = new System.Drawing.Size(34, 13);
            this.lblFisica.TabIndex = 254;
            this.lblFisica.Text = "Fisica";
            // 
            // lblQuimica
            // 
            this.lblQuimica.AutoSize = true;
            this.lblQuimica.Location = new System.Drawing.Point(138, 359);
            this.lblQuimica.Name = "lblQuimica";
            this.lblQuimica.Size = new System.Drawing.Size(45, 13);
            this.lblQuimica.TabIndex = 253;
            this.lblQuimica.Text = "Quimica";
            // 
            // lblMatematicas
            // 
            this.lblMatematicas.AutoSize = true;
            this.lblMatematicas.Location = new System.Drawing.Point(116, 305);
            this.lblMatematicas.Name = "lblMatematicas";
            this.lblMatematicas.Size = new System.Drawing.Size(67, 13);
            this.lblMatematicas.TabIndex = 252;
            this.lblMatematicas.Text = "Matematicas";
            // 
            // lblEspañol
            // 
            this.lblEspañol.AutoSize = true;
            this.lblEspañol.Location = new System.Drawing.Point(138, 251);
            this.lblEspañol.Name = "lblEspañol";
            this.lblEspañol.Size = new System.Drawing.Size(45, 13);
            this.lblEspañol.TabIndex = 251;
            this.lblEspañol.Text = "Español";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBuscarMaestro);
            this.groupBox1.Controls.Add(this.txtApMatMaestro);
            this.groupBox1.Controls.Add(this.txtApPatMaestro);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtNomMaestro);
            this.groupBox1.Controls.Add(this.txtNumEmpleado);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(493, 88);
            this.groupBox1.TabIndex = 294;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del Maestro";
            // 
            // txtNomMaestro
            // 
            this.txtNomMaestro.Location = new System.Drawing.Point(53, 49);
            this.txtNomMaestro.Name = "txtNomMaestro";
            this.txtNomMaestro.Size = new System.Drawing.Size(100, 20);
            this.txtNomMaestro.TabIndex = 2;
            // 
            // txtNumEmpleado
            // 
            this.txtNumEmpleado.Location = new System.Drawing.Point(96, 20);
            this.txtNumEmpleado.Name = "txtNumEmpleado";
            this.txtNumEmpleado.Size = new System.Drawing.Size(71, 20);
            this.txtNumEmpleado.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "NumEmpleado:";
            // 
            // dgPrimerAño
            // 
            this.dgPrimerAño.AllowUserToAddRows = false;
            this.dgPrimerAño.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPrimerAño.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColMatricula,
            this.colGrupo,
            this.colNomAlum,
            this.colApePat,
            this.colApeMat});
            this.dgPrimerAño.Location = new System.Drawing.Point(511, 146);
            this.dgPrimerAño.Name = "dgPrimerAño";
            this.dgPrimerAño.Size = new System.Drawing.Size(380, 506);
            this.dgPrimerAño.TabIndex = 297;
            this.dgPrimerAño.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrimerAño_CellClick);
            this.dgPrimerAño.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrimerAño_CellContentClick);
            this.dgPrimerAño.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrimerAño_CellEnter);
            // 
            // cbSelecGrupo
            // 
            this.cbSelecGrupo.FormattingEnabled = true;
            this.cbSelecGrupo.Items.AddRange(new object[] {
            "1-A",
            "1-B"});
            this.cbSelecGrupo.Location = new System.Drawing.Point(24, 49);
            this.cbSelecGrupo.Name = "cbSelecGrupo";
            this.cbSelecGrupo.Size = new System.Drawing.Size(89, 21);
            this.cbSelecGrupo.TabIndex = 298;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.btnConsulAlumCalif);
            this.groupBox2.Controls.Add(this.cbSelecGrupo);
            this.groupBox2.Location = new System.Drawing.Point(570, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(252, 99);
            this.groupBox2.TabIndex = 299;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Información de Alumnos";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 13);
            this.label9.TabIndex = 299;
            this.label9.Text = "Seleccionar Grupo";
            // 
            // btnConsulAlumCalif
            // 
            this.btnConsulAlumCalif.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnConsulAlumCalif.BackgroundImage = global::EscolarSoft.Properties.Resources.consultar3;
            this.btnConsulAlumCalif.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConsulAlumCalif.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsulAlumCalif.ForeColor = System.Drawing.Color.White;
            this.btnConsulAlumCalif.Location = new System.Drawing.Point(139, 12);
            this.btnConsulAlumCalif.Name = "btnConsulAlumCalif";
            this.btnConsulAlumCalif.Size = new System.Drawing.Size(86, 79);
            this.btnConsulAlumCalif.TabIndex = 296;
            this.btnConsulAlumCalif.Text = "Consultar\r\n";
            this.btnConsulAlumCalif.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnConsulAlumCalif.UseVisualStyleBackColor = false;
            this.btnConsulAlumCalif.Click += new System.EventHandler(this.btnConsulAlumCalif_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 665);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(903, 22);
            this.statusStrip1.TabIndex = 300;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(132, 17);
            this.toolStripStatusLabel1.Text = "Ealuaciones Primer Año";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtApMatAlum);
            this.groupBox3.Controls.Add(this.txtApPatAlum);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtNombreAlum);
            this.groupBox3.Controls.Add(this.txtMatricula);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(12, 121);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(493, 85);
            this.groupBox3.TabIndex = 301;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Datos del Alumno";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Matricula:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Nombre:";
            // 
            // txtMatricula
            // 
            this.txtMatricula.Location = new System.Drawing.Point(94, 23);
            this.txtMatricula.Name = "txtMatricula";
            this.txtMatricula.Size = new System.Drawing.Size(71, 20);
            this.txtMatricula.TabIndex = 2;
            // 
            // txtNombreAlum
            // 
            this.txtNombreAlum.Location = new System.Drawing.Point(53, 49);
            this.txtNombreAlum.Name = "txtNombreAlum";
            this.txtNombreAlum.Size = new System.Drawing.Size(100, 20);
            this.txtNombreAlum.TabIndex = 3;
            // 
            // ColMatricula
            // 
            this.ColMatricula.DataPropertyName = "ColMatricula";
            this.ColMatricula.HeaderText = "Matricula";
            this.ColMatricula.Name = "ColMatricula";
            this.ColMatricula.Visible = false;
            // 
            // colGrupo
            // 
            this.colGrupo.DataPropertyName = "colGrupo";
            this.colGrupo.HeaderText = "Grupo";
            this.colGrupo.Name = "colGrupo";
            this.colGrupo.Width = 50;
            // 
            // colNomAlum
            // 
            this.colNomAlum.DataPropertyName = "colNomAlum";
            this.colNomAlum.HeaderText = "Nombre";
            this.colNomAlum.Name = "colNomAlum";
            // 
            // colApePat
            // 
            this.colApePat.DataPropertyName = "colApePat";
            this.colApePat.HeaderText = "Apellido Paterno";
            this.colApePat.Name = "colApePat";
            // 
            // colApeMat
            // 
            this.colApeMat.DataPropertyName = "colApeMat";
            this.colApeMat.HeaderText = "Apellido Materno";
            this.colApeMat.Name = "colApeMat";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(156, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "ApPaterno:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(320, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "ApMaterno:";
            // 
            // txtApPatAlum
            // 
            this.txtApPatAlum.Location = new System.Drawing.Point(214, 49);
            this.txtApPatAlum.Name = "txtApPatAlum";
            this.txtApPatAlum.Size = new System.Drawing.Size(100, 20);
            this.txtApPatAlum.TabIndex = 6;
            // 
            // txtApMatAlum
            // 
            this.txtApMatAlum.Location = new System.Drawing.Point(378, 49);
            this.txtApMatAlum.Name = "txtApMatAlum";
            this.txtApMatAlum.Size = new System.Drawing.Size(100, 20);
            this.txtApMatAlum.TabIndex = 7;
            // 
            // txtApMatMaestro
            // 
            this.txtApMatMaestro.Location = new System.Drawing.Point(378, 49);
            this.txtApMatMaestro.Name = "txtApMatMaestro";
            this.txtApMatMaestro.Size = new System.Drawing.Size(100, 20);
            this.txtApMatMaestro.TabIndex = 11;
            // 
            // txtApPatMaestro
            // 
            this.txtApPatMaestro.Location = new System.Drawing.Point(215, 49);
            this.txtApPatMaestro.Name = "txtApPatMaestro";
            this.txtApPatMaestro.Size = new System.Drawing.Size(100, 20);
            this.txtApPatMaestro.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(317, 52);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "ApMaterno:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(156, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "ApPaterno:";
            // 
            // btnBuscarMaestro
            // 
            this.btnBuscarMaestro.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBuscarMaestro.BackgroundImage")));
            this.btnBuscarMaestro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscarMaestro.Location = new System.Drawing.Point(173, 12);
            this.btnBuscarMaestro.Name = "btnBuscarMaestro";
            this.btnBuscarMaestro.Size = new System.Drawing.Size(43, 34);
            this.btnBuscarMaestro.TabIndex = 12;
            this.btnBuscarMaestro.UseVisualStyleBackColor = true;
            this.btnBuscarMaestro.Click += new System.EventHandler(this.btnBuscarMaestro_Click);
            // 
            // btnGuardarEspa
            // 
            this.btnGuardarEspa.Location = new System.Drawing.Point(332, 244);
            this.btnGuardarEspa.Name = "btnGuardarEspa";
            this.btnGuardarEspa.Size = new System.Drawing.Size(95, 23);
            this.btnGuardarEspa.TabIndex = 302;
            this.btnGuardarEspa.Text = "Guardar Parcial";
            this.btnGuardarEspa.UseVisualStyleBackColor = true;
            this.btnGuardarEspa.Click += new System.EventHandler(this.btnEvaluaEspa_Click);
            // 
            // btnGuardarMatematicas
            // 
            this.btnGuardarMatematicas.Location = new System.Drawing.Point(333, 301);
            this.btnGuardarMatematicas.Name = "btnGuardarMatematicas";
            this.btnGuardarMatematicas.Size = new System.Drawing.Size(95, 23);
            this.btnGuardarMatematicas.TabIndex = 303;
            this.btnGuardarMatematicas.Text = "Guardar Parcial";
            this.btnGuardarMatematicas.UseVisualStyleBackColor = true;
            this.btnGuardarMatematicas.Click += new System.EventHandler(this.btnGuardarMatematicas_Click);
            // 
            // btnGuardarQuimica
            // 
            this.btnGuardarQuimica.Location = new System.Drawing.Point(332, 354);
            this.btnGuardarQuimica.Name = "btnGuardarQuimica";
            this.btnGuardarQuimica.Size = new System.Drawing.Size(95, 23);
            this.btnGuardarQuimica.TabIndex = 304;
            this.btnGuardarQuimica.Text = "Guardar Parcial";
            this.btnGuardarQuimica.UseVisualStyleBackColor = true;
            this.btnGuardarQuimica.Click += new System.EventHandler(this.btnGuardarQuimica_Click);
            // 
            // btnGuardarFisica
            // 
            this.btnGuardarFisica.Location = new System.Drawing.Point(332, 409);
            this.btnGuardarFisica.Name = "btnGuardarFisica";
            this.btnGuardarFisica.Size = new System.Drawing.Size(95, 23);
            this.btnGuardarFisica.TabIndex = 305;
            this.btnGuardarFisica.Text = "Guardar Parcial";
            this.btnGuardarFisica.UseVisualStyleBackColor = true;
            this.btnGuardarFisica.Click += new System.EventHandler(this.btnGuardarFisica_Click);
            // 
            // btnGuardarCE
            // 
            this.btnGuardarCE.Location = new System.Drawing.Point(332, 464);
            this.btnGuardarCE.Name = "btnGuardarCE";
            this.btnGuardarCE.Size = new System.Drawing.Size(95, 23);
            this.btnGuardarCE.TabIndex = 306;
            this.btnGuardarCE.Text = "Guardar Parcial";
            this.btnGuardarCE.UseVisualStyleBackColor = true;
            this.btnGuardarCE.Click += new System.EventHandler(this.btnGuardarCE_Click);
            // 
            // btnGuardarHistoria
            // 
            this.btnGuardarHistoria.Location = new System.Drawing.Point(332, 519);
            this.btnGuardarHistoria.Name = "btnGuardarHistoria";
            this.btnGuardarHistoria.Size = new System.Drawing.Size(95, 23);
            this.btnGuardarHistoria.TabIndex = 307;
            this.btnGuardarHistoria.Text = "Guardar Parcial";
            this.btnGuardarHistoria.UseVisualStyleBackColor = true;
            this.btnGuardarHistoria.Click += new System.EventHandler(this.btnGuardarHistoria_Click);
            // 
            // FrmCalifPrimerAño
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(903, 687);
            this.Controls.Add(this.btnGuardarHistoria);
            this.Controls.Add(this.btnGuardarCE);
            this.Controls.Add(this.btnGuardarFisica);
            this.Controls.Add(this.btnGuardarQuimica);
            this.Controls.Add(this.btnGuardarMatematicas);
            this.Controls.Add(this.btnGuardarEspa);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgPrimerAño);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnGuardarCalif1a);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.lblPromFinal1a);
            this.Controls.Add(this.btnPromFinal1a);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.lblPromedioHistoria1a);
            this.Controls.Add(this.btnPromediarHist1a);
            this.Controls.Add(this.lblPromedioCE1a);
            this.Controls.Add(this.btnPromediarCE1a);
            this.Controls.Add(this.lblPromedioFisica1a);
            this.Controls.Add(this.btnPromediarFis1a);
            this.Controls.Add(this.lblPromedioQuimica1a);
            this.Controls.Add(this.btnPromediarQuim1a);
            this.Controls.Add(this.lblPromedioMat1a);
            this.Controls.Add(this.btnPromediarMat1a);
            this.Controls.Add(this.lblPromedioEspaañol1a);
            this.Controls.Add(this.btnPromediarEsp1a);
            this.Controls.Add(this.txtCalifHistoriaB3);
            this.Controls.Add(this.txtCalifCEB3);
            this.Controls.Add(this.txtCalifFisicaB3);
            this.Controls.Add(this.txtCalifQuimicaB3);
            this.Controls.Add(this.txtCalifMatB3);
            this.Controls.Add(this.txtCalifEspañolB3);
            this.Controls.Add(this.txtCalifHistoriaB2);
            this.Controls.Add(this.txtCalifCEB2);
            this.Controls.Add(this.txtCalifFisicaB2);
            this.Controls.Add(this.txtCalifQuimicaB2);
            this.Controls.Add(this.txtCalifMatB2);
            this.Controls.Add(this.txtCalifEspañolB2);
            this.Controls.Add(this.txtCalifHistoriaB1);
            this.Controls.Add(this.txtCalifCEB1);
            this.Controls.Add(this.txtCalifFisicaB1);
            this.Controls.Add(this.txtCalifQuimicaB1);
            this.Controls.Add(this.txtCalifMatB1);
            this.Controls.Add(this.txtCalifEspañolB1);
            this.Controls.Add(this.lblHistoria);
            this.Controls.Add(this.lblFce);
            this.Controls.Add(this.lblFisica);
            this.Controls.Add(this.lblQuimica);
            this.Controls.Add(this.lblMatematicas);
            this.Controls.Add(this.lblEspañol);
            this.Name = "FrmCalifPrimerAño";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmCalifPrimerAño_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrimerAño)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGuardarCalif1a;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label lblPromFinal1a;
        private System.Windows.Forms.Button btnPromFinal1a;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblPromedioHistoria1a;
        private System.Windows.Forms.Button btnPromediarHist1a;
        private System.Windows.Forms.Label lblPromedioCE1a;
        private System.Windows.Forms.Button btnPromediarCE1a;
        private System.Windows.Forms.Label lblPromedioFisica1a;
        private System.Windows.Forms.Button btnPromediarFis1a;
        private System.Windows.Forms.Label lblPromedioQuimica1a;
        private System.Windows.Forms.Button btnPromediarQuim1a;
        private System.Windows.Forms.Label lblPromedioMat1a;
        private System.Windows.Forms.Button btnPromediarMat1a;
        private System.Windows.Forms.Label lblPromedioEspaañol1a;
        private System.Windows.Forms.Button btnPromediarEsp1a;
        private System.Windows.Forms.TextBox txtCalifHistoriaB3;
        private System.Windows.Forms.TextBox txtCalifCEB3;
        private System.Windows.Forms.TextBox txtCalifFisicaB3;
        private System.Windows.Forms.TextBox txtCalifQuimicaB3;
        private System.Windows.Forms.TextBox txtCalifMatB3;
        private System.Windows.Forms.TextBox txtCalifEspañolB3;
        private System.Windows.Forms.TextBox txtCalifHistoriaB2;
        private System.Windows.Forms.TextBox txtCalifCEB2;
        private System.Windows.Forms.TextBox txtCalifFisicaB2;
        private System.Windows.Forms.TextBox txtCalifQuimicaB2;
        private System.Windows.Forms.TextBox txtCalifMatB2;
        private System.Windows.Forms.TextBox txtCalifEspañolB2;
        private System.Windows.Forms.TextBox txtCalifHistoriaB1;
        private System.Windows.Forms.TextBox txtCalifCEB1;
        private System.Windows.Forms.TextBox txtCalifFisicaB1;
        private System.Windows.Forms.TextBox txtCalifQuimicaB1;
        private System.Windows.Forms.TextBox txtCalifMatB1;
        private System.Windows.Forms.TextBox txtCalifEspañolB1;
        private System.Windows.Forms.Label lblHistoria;
        private System.Windows.Forms.Label lblFce;
        private System.Windows.Forms.Label lblFisica;
        private System.Windows.Forms.Label lblQuimica;
        private System.Windows.Forms.Label lblMatematicas;
        private System.Windows.Forms.Label lblEspañol;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNomMaestro;
        private System.Windows.Forms.TextBox txtNumEmpleado;
        private System.Windows.Forms.Button btnConsulAlumCalif;
        private System.Windows.Forms.DataGridView dgPrimerAño;
        private System.Windows.Forms.ComboBox cbSelecGrupo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtNombreAlum;
        private System.Windows.Forms.TextBox txtMatricula;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColMatricula;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGrupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNomAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn colApePat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colApeMat;
        private System.Windows.Forms.TextBox txtApMatAlum;
        private System.Windows.Forms.TextBox txtApPatAlum;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtApMatMaestro;
        private System.Windows.Forms.TextBox txtApPatMaestro;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnBuscarMaestro;
        private System.Windows.Forms.Button btnGuardarEspa;
        private System.Windows.Forms.Button btnGuardarMatematicas;
        private System.Windows.Forms.Button btnGuardarQuimica;
        private System.Windows.Forms.Button btnGuardarFisica;
        private System.Windows.Forms.Button btnGuardarCE;
        private System.Windows.Forms.Button btnGuardarHistoria;
    }
}