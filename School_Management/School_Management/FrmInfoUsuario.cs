﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace School_Management
{
    public partial class FrmInfoUsuario : Form
    {
        public FrmInfoUsuario()
        {
            InitializeComponent();
        }

        public string fotoUsu;
        public string clave;
        public string apPat;
        public string apMat;
        public string nommbreUsu;
        public string tipUsu;

        private void FrmInfoUsuario_Load(object sender, EventArgs e)
        {
            pbImagenUsuario.Image = (Bitmap)Bitmap.FromFile(fotoUsu);
            txtApMatInfoUsu.Text = clave;
            txtApPatInfoUsu.Text = apPat;
            txtApMatInfoUsu.Text = apMat;
            txtNomInfoUsu.Text = nommbreUsu;
            txtTipoUsuarioInfo.Text = tipUsu;
        }
    }
}
