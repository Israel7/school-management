﻿namespace School_Management
{
    partial class FrmInfoUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMatriculaUuario = new System.Windows.Forms.TextBox();
            this.txtApPatInfoUsu = new System.Windows.Forms.TextBox();
            this.txtApMatInfoUsu = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNomInfoUsu = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTipoUsuarioInfo = new System.Windows.Forms.TextBox();
            this.pbImagenUsuario = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenUsuario)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Matricula";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 190);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Apellido Paterno";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 229);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "pellido Materno";
            // 
            // txtMatriculaUuario
            // 
            this.txtMatriculaUuario.Location = new System.Drawing.Point(153, 149);
            this.txtMatriculaUuario.Name = "txtMatriculaUuario";
            this.txtMatriculaUuario.Size = new System.Drawing.Size(100, 20);
            this.txtMatriculaUuario.TabIndex = 4;
            // 
            // txtApPatInfoUsu
            // 
            this.txtApPatInfoUsu.Location = new System.Drawing.Point(153, 187);
            this.txtApPatInfoUsu.Name = "txtApPatInfoUsu";
            this.txtApPatInfoUsu.Size = new System.Drawing.Size(100, 20);
            this.txtApPatInfoUsu.TabIndex = 5;
            // 
            // txtApMatInfoUsu
            // 
            this.txtApMatInfoUsu.Location = new System.Drawing.Point(153, 222);
            this.txtApMatInfoUsu.Name = "txtApMatInfoUsu";
            this.txtApMatInfoUsu.Size = new System.Drawing.Size(100, 20);
            this.txtApMatInfoUsu.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(91, 258);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nombre";
            // 
            // txtNomInfoUsu
            // 
            this.txtNomInfoUsu.Location = new System.Drawing.Point(153, 255);
            this.txtNomInfoUsu.Name = "txtNomInfoUsu";
            this.txtNomInfoUsu.Size = new System.Drawing.Size(100, 20);
            this.txtNomInfoUsu.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(65, 293);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Tipo de Usuario";
            // 
            // txtTipoUsuarioInfo
            // 
            this.txtTipoUsuarioInfo.Location = new System.Drawing.Point(153, 290);
            this.txtTipoUsuarioInfo.Name = "txtTipoUsuarioInfo";
            this.txtTipoUsuarioInfo.Size = new System.Drawing.Size(100, 20);
            this.txtTipoUsuarioInfo.TabIndex = 10;
            // 
            // pbImagenUsuario
            // 
            this.pbImagenUsuario.Location = new System.Drawing.Point(96, 25);
            this.pbImagenUsuario.Name = "pbImagenUsuario";
            this.pbImagenUsuario.Size = new System.Drawing.Size(136, 107);
            this.pbImagenUsuario.TabIndex = 0;
            this.pbImagenUsuario.TabStop = false;
            // 
            // FrmInfoUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 328);
            this.Controls.Add(this.txtTipoUsuarioInfo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNomInfoUsu);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtApMatInfoUsu);
            this.Controls.Add(this.txtApPatInfoUsu);
            this.Controls.Add(this.txtMatriculaUuario);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbImagenUsuario);
            this.Name = "FrmInfoUsuario";
            this.Text = "FrmInfoUsuario";
            this.Load += new System.EventHandler(this.FrmInfoUsuario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenUsuario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbImagenUsuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMatriculaUuario;
        private System.Windows.Forms.TextBox txtApPatInfoUsu;
        private System.Windows.Forms.TextBox txtApMatInfoUsu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNomInfoUsu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTipoUsuarioInfo;
    }
}