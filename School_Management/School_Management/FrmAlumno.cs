﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibSM;
using System.IO;

namespace School_Management
{
   
    public partial class FrmAlumno : Form
    {
      
        Alumno mostrarAlumno = new Alumno();
        List<Alumno> mostrarInfoAlumno = new List<Alumno>();
        String archivo;
        Alumno alumno = new Alumno();
        Validaciones valid = new Validaciones();
        
        public FrmAlumno()
        {
            InitializeComponent();
        }

        public void igualarCampos()
        {
            
            alumno.Matricula = int.Parse(lblCont.Text);
            alumno.ApellidoPaterno = txtApPat.Text;
            alumno.ApellidoMaterno = txtApMat.Text;
            alumno.Nombre = txtNombre.Text;
            alumno.FechaNac = Convert.ToDateTime(dtpFechaNac.Value.ToString("yyyy-MM-dd"));
            alumno.Dirección = txtDir.Text;
            alumno.Telefono = txtTel.Text;
            alumno.Curp = txtCurp.Text;
            alumno.Materias = cbMaterias.Text;
            alumno.Grupo = cbGrupo.Text;
            alumno.Turno = cbTurno.Text;
            
            alumno.Foto = archivo;
            alumno.Usuario = txtUsuarioAlum.Text;
            alumno.Contrasena = txtContrasenaAlum.Text;            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //METODO igualarCampos LO MANDAMOS LLAMAR EN LUGAR DE TENERLO PROGRAMADO EN EL BOTON
            igualarCampos();

            if (alumno.agregarAlumno())
                {
                    alumno.Matricula = int.Parse(lblmatri.Text);
                    alumno.agregarAlumnoGrupo();
                    mostrarEnGrid();
                    MessageBox.Show("El Alumno " + alumno.Nombre + " " + alumno.ApellidoPaterno + " Ha sido Inscrito");
                    limpiar();
                    ////////VOVLER A CARGAR DATOS EN GRID"""

                }
                else
                    MessageBox.Show("Error al inscribir Alumnos. Verifique los datos");
            }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////7
        public void mostrarEspesificGrid()
        {
            alumno.ApellidoPaterno = txtApPat.Text;

            DataTable llenarDatosAlum = new DataTable();
            llenarDatosAlum.Columns.Add("Colid");
            llenarDatosAlum.Columns.Add("ColApat");
            llenarDatosAlum.Columns.Add("ColAmat");
            llenarDatosAlum.Columns.Add("ColNom");
            llenarDatosAlum.Columns.Add("ColFechaNac");
            llenarDatosAlum.Columns.Add("ColMat");
            llenarDatosAlum.Columns.Add("ColGrup");
            llenarDatosAlum.Columns.Add("ColTurno");
            llenarDatosAlum.Columns.Add("ColFoto").DataType = System.Type.GetType("System.Byte[]");
            llenarDatosAlum.Columns.Add("colRuta");
            //llenarDatosAlum.Columns.Add("ColUsuario");
            //llenarDatosAlum.Columns.Add("ColContrasena");
            //IGUALAMOS LOS REGISTROS 
            mostrarInfoAlumno = mostrarAlumno.ListConsultarApellido();

            for (int i = 0; i < mostrarInfoAlumno.Count; i++)
            {
                //COLOCAMOS REGISTROS EN SUS RESPECTIVOS RENGLONES
                DataRow renglones = llenarDatosAlum.NewRow();
                renglones["Colid"] = mostrarInfoAlumno[i].Matricula;
                renglones["ColApat"] = mostrarInfoAlumno[i].ApellidoPaterno;
                renglones["ColAmat"] = mostrarInfoAlumno[i].ApellidoMaterno;
                renglones["ColNom"] = mostrarInfoAlumno[i].Nombre;
                renglones["ColFechaNac"] = mostrarInfoAlumno[i].FechaNac;
                renglones["ColMat"] = mostrarInfoAlumno[i].Materias;
                renglones["ColGrup"] = mostrarInfoAlumno[i].Grupo;
                renglones["ColTurno"] = mostrarInfoAlumno[i].Turno;
                try
                {
                    var imageConverter = new ImageConverter();
                    renglones["ColFoto"] = imageConverter.ConvertTo(Image.FromFile(mostrarInfoAlumno[i].Foto), System.Type.GetType("System.Byte[]"));
                    //pictureBox1.Image = Image.FromFile(mostrarInfoAlumno[i].Foto);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.ToString());
                }
                renglones["colRuta"] = mostrarInfoAlumno[i].Foto;


                //IGUALAR EL ULTIMO VALOR DE ID EN LA BASE DE DATOS
                //lblCont.Text = mostrarInfoAlumno[i].Matricula.ToString();
                //lblmatri.Text = mostrarInfoAlumno[i].Matricula.ToString();
                //lblmatri.Text = (int.Parse(lblmatri.Text) + 1).ToString();
                //llenarDatosAlum.Rows.Add(renglones);
            }
            //OBTENIENDO EL ULTIMO VALOR SE LE AGREGA 1 QUE SERIA EL SIGUIENTE USUARIO A INGRESAR
            //lblCont.Text = (int.Parse(lblCont.Text) + 1).ToString();
            //dgAlumnos.DataSource = null;

            dgAlumnos.DataSource = llenarDatosAlum;

        }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public void mostrarEnGrid()
        {
            //CREAN LAS COLUMNAS DEL DATAGRID
            //dgAlumnos.DataSource = null;

            DataTable llenarDatosAlum = new DataTable();
            llenarDatosAlum.Columns.Add("Colid");
            llenarDatosAlum.Columns.Add("ColApat");
            llenarDatosAlum.Columns.Add("ColAmat");
            llenarDatosAlum.Columns.Add("ColNom");
            llenarDatosAlum.Columns.Add("ColFechaNac");
            llenarDatosAlum.Columns.Add("ColMat");
            llenarDatosAlum.Columns.Add("ColGrup");
            llenarDatosAlum.Columns.Add("ColTurno");
            llenarDatosAlum.Columns.Add("ColFoto").DataType = System.Type.GetType("System.Byte[]");
            llenarDatosAlum.Columns.Add("colRuta");
            //llenarDatosAlum.Columns.Add("ColUsuario");
            //llenarDatosAlum.Columns.Add("ColContrasena");
            //IGUALAMOS LOS REGISTROS 
            mostrarInfoAlumno = mostrarAlumno.enlistarAlumno();

            for (int i = 0; i < mostrarInfoAlumno.Count; i++)
            {
                //COLOCAMOS REGISTROS EN SUS RESPECTIVOS RENGLONES
                DataRow renglones = llenarDatosAlum.NewRow();
                renglones["Colid"] = mostrarInfoAlumno[i].Matricula;
                renglones["ColApat"] = mostrarInfoAlumno[i].ApellidoPaterno;
                renglones["ColAmat"] = mostrarInfoAlumno[i].ApellidoMaterno;
                renglones["ColNom"] = mostrarInfoAlumno[i].Nombre;
                renglones["ColFechaNac"] = mostrarInfoAlumno[i].FechaNac;
                renglones["ColMat"] = mostrarInfoAlumno[i].Materias;
                renglones["ColGrup"] = mostrarInfoAlumno[i].Grupo;
                renglones["ColTurno"] = mostrarInfoAlumno[i].Turno;
                try
                {
                    var imageConverter = new ImageConverter();
                    renglones["ColFoto"] = imageConverter.ConvertTo(Image.FromFile(mostrarInfoAlumno[i].Foto), System.Type.GetType("System.Byte[]"));
                    //pictureBox1.Image = Image.FromFile(mostrarInfoAlumno[i].Foto);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.ToString());
                }
                renglones["colRuta"] = mostrarInfoAlumno[i].Foto;


                //IGUALAR EL ULTIMO VALOR DE ID EN LA BASE DE DATOS
                lblCont.Text = mostrarInfoAlumno[i].Matricula.ToString();
                lblmatri.Text = mostrarInfoAlumno[i].Matricula.ToString();
                lblmatri.Text = (int.Parse(lblmatri.Text) + 1).ToString();
                llenarDatosAlum.Rows.Add(renglones);
            }
            //OBTENIENDO EL ULTIMO VALOR SE LE AGREGA 1 QUE SERIA EL SIGUIENTE USUARIO A INGRESAR
            //lblCont.Text = (int.Parse(lblCont.Text) + 1).ToString();
            //dgAlumnos.DataSource = null;

            dgAlumnos.DataSource = llenarDatosAlum;

            //AGREGAMOS NUEVO REGISTRO AL DATAGRID
        }

        private void pbFoto_Click(object sender, EventArgs e)
        {
            //para cargar imagenes en el pictureBox
            openFileDialog1.Filter = "image files (*.gif; *.jpg; *.png)| *.gif; *.jpg; *.png";
            openFileDialog1.ShowDialog();

            archivo = openFileDialog1.FileName;
            string directoryPath = Path.GetDirectoryName(openFileDialog1.FileName);
            //MessageBox.Show("archivo " + archivo.Length);
            if (directoryPath != "openFileDialog1")

                pbFoto.Image = (Bitmap)Bitmap.FromFile(openFileDialog1.FileName);
        }

        private void btnEliminarAlum_Click(object sender, EventArgs e)
        {
            if (int.Parse(lblCont.Text) > 0)
            {


                for (int i = 0; i < dgAlumnos.Rows.Count; i++)
                {
                    if (int.Parse(lblCont.Text) == int.Parse(dgAlumnos[0, i].Value.ToString()))
                    {
                        if (MessageBox.Show("Se dispone a eliminar a " + mostrarInfoAlumno[i].ApellidoPaterno + "" + mostrarInfoAlumno[i].ApellidoMaterno + " " + mostrarInfoAlumno[i].Nombre + " ", "Eliminar Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            igualarCampos();
                            alumno.eliminarAlumnoGrupo();
                            alumno.eliminarAlumno();

                            dgAlumnos.Rows.RemoveAt(i);
                            limpiar();
                        }
                    }
                }
            }
        }

        private void btnModificarAlum_Click(object sender, EventArgs e)
        {
            igualarCampos();
            alumno.modificarAlumno();
            mostrarEnGrid();
            limpiar();
        }

        private void FrmAlumno_Load(object sender, EventArgs e)
        {
            mostrarEnGrid();
            limpiar();
           
        }

        private void dgAlumnos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < mostrarInfoAlumno.Count; i++)
            {
                if (Convert.ToInt64(dgAlumnos.CurrentRow.Cells["Colid"].Value) == mostrarInfoAlumno[i].Matricula)
                {
                    lblCont.Text = dgAlumnos[0, e.RowIndex].Value.ToString();
                    txtApPat.Text = dgAlumnos[1, e.RowIndex].Value.ToString();
                    txtApMat.Text = dgAlumnos[2, e.RowIndex].Value.ToString();
                    txtNombre.Text = dgAlumnos[3, e.RowIndex].Value.ToString();
                    dtpFechaNac.Text = dgAlumnos[4, e.RowIndex].Value.ToString();
                    cbMaterias.Text = dgAlumnos[5, e.RowIndex].Value.ToString();
                    cbGrupo.Text = dgAlumnos[6, e.RowIndex].Value.ToString();
                    cbTurno.Text = dgAlumnos[7, e.RowIndex].Value.ToString();
                    txtDir.Text = mostrarInfoAlumno[i].Dirección;
                    txtTel.Text = mostrarInfoAlumno[i].Telefono;
                    txtCurp.Text = mostrarInfoAlumno[i].Curp;
                    pbFoto.Image = (Bitmap)Bitmap.FromFile(mostrarInfoAlumno[i].Foto);
                    //archivo = dgAlumnos[8, e.RowIndex].Value.ToString();
                    archivo = mostrarInfoAlumno[i].Foto;
                }
            }
        }

        private void dgAlumnos_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgAlumnos_CellClick(sender, e);
        }

        public void limpiar()
        {
            txtApPat.Clear();
            txtApMat.Clear();
            txtNombre.Clear();
            dtpFechaNac.ResetText();
            txtDir.Clear();
            txtTel.Clear();
            txtCurp.Clear();
            cbMaterias.Text = cbMaterias.Items[0].ToString();
            cbGrupo.Text = cbGrupo.Items[0].ToString();
            cbTurno.Text = cbTurno.Items[0].ToString();
            txtUsuarioAlum.Clear();
            txtContrasenaAlum.Clear();
           // txtConfirmContrasenaA.Clear();
            pbFoto.Image = (Bitmap)Image.FromFile(@"C:\Users\Work-Group\Desktop\Resources\agregar.png");
        }

        private void txtApPat_KeyPress(object sender, KeyPressEventArgs e)
        {
            valid.soloLetrasSinEspacio(e);
        }

        private void txtApMat_TextChanged(object sender, EventArgs e)
        {
            //valid.soloLetrasSinEspacio(e);
        }

        private void txtApMat_KeyPress(object sender, KeyPressEventArgs e)
        {
            valid.soloLetrasSinEspacio(e);
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            valid.soloLetrasConEspacios(e);
        }

        private void txtDir_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDir_KeyPress(object sender, KeyPressEventArgs e)
        {
            valid.numerosYletras(e);
        }

        private void txtTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            valid.soloNumeros(e);
        }

        private void txtUsuarioAlum_KeyPress(object sender, KeyPressEventArgs e)
        {
            valid.numerosYletrasSinEspacios(e);
        }

        private void txtCurp_KeyPress(object sender, KeyPressEventArgs e)
        {
            valid.numerosYletrasSinEspacios(e);
        }

        private void txtContrasenaAlum_KeyPress(object sender, KeyPressEventArgs e)
        {
            valid.numerosYletrasSinEspacios(e);
        }
/////Consultar de los CRUDs
        private void button4_Click(object sender, EventArgs e)
        {
            //Alumno btnConsultarAlumno = new Alumno();
            //btnConsultarAlumno.ApellidoPaterno = txtApPat.Text;
            //btnConsultarAlumno.ListConsultarApellido();
            //dgAlumnos.Rows.Clear();
            mostrarEspesificGrid();
        }

        private void txtApPat_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
       
