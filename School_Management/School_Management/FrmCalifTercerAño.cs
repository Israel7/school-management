﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibSM;

namespace School_Management
{
    public partial class FrmCalifTercerAño : Form
    {
        Grupos grupos3 = new Grupos();
        List<Grupos> enlistarAlumnos = new List<Grupos>();

        public FrmCalifTercerAño()
        {
            InitializeComponent();
        }

        private void FrmCalifTercerAño_Load(object sender, EventArgs e)
        {

        }

        private void btnConsulAlumCalif3_Click(object sender, EventArgs e)
        {
            if (cbSelecGrupo.Text == "3-A")
            {
                cargarGridA();
            }
            else if (cbSelecGrupo.Text == "3-B")
            {
                cargarGridB();
            }
        }

        public void cargarGridA()
        {
            DataTable tabla = new DataTable();
            tabla.Columns.Add("colGrupo");
            tabla.Columns.Add("colNomAlum");
            tabla.Columns.Add("colApePat");
            tabla.Columns.Add("colApeMat");

            enlistarAlumnos = grupos3.datosCombinados3a();
            for (int i = 0; i < enlistarAlumnos.Count; i++)
            {
                DataRow row = tabla.NewRow();
                row["colGrupo"] = enlistarAlumnos[i].NomGrupo;
                row["colNomAlum"] = enlistarAlumnos[i].NombreAlumno;
                row["colApePat"] = enlistarAlumnos[i].ApelPater;
                row["colApeMat"] = enlistarAlumnos[i].ApelMater;

                tabla.Rows.Add(row);
            }
            dgTercerAño.DataSource = tabla;
        }
        public void cargarGridB()
        {
            DataTable tabla = new DataTable();
            tabla.Columns.Add("colGrupo");
            tabla.Columns.Add("colNomAlum");
            tabla.Columns.Add("colApePat");
            tabla.Columns.Add("colApeMat");

            enlistarAlumnos = grupos3.datosCombinados3b();
            for (int i = 0; i < enlistarAlumnos.Count; i++)
            {
                DataRow row = tabla.NewRow();
                row["colGrupo"] = enlistarAlumnos[i].NomGrupo;
                row["colNomAlum"] = enlistarAlumnos[i].NombreAlumno;
                row["colApePat"] = enlistarAlumnos[i].ApelPater;
                row["colApeMat"] = enlistarAlumnos[i].ApelMater;

                tabla.Rows.Add(row);
            }
            dgTercerAño.DataSource = tabla;
        }
    }
}
