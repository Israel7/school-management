﻿namespace School_Management
{
    partial class FrmCalifTercerAño
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnConsulAlumCalif3 = new System.Windows.Forms.Button();
            this.cbSelecGrupo = new System.Windows.Forms.ComboBox();
            this.dgTercerAño = new System.Windows.Forms.DataGridView();
            this.colGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNomAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colApePat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colApeMat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtNumEmpleado = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGuardarCalif1a = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.lblPromFinal1a = new System.Windows.Forms.Label();
            this.btnPromFinal1a = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblPromedioHistoria1a = new System.Windows.Forms.Label();
            this.btnPromediarHist1a = new System.Windows.Forms.Button();
            this.lblPromedioCE1a = new System.Windows.Forms.Label();
            this.btnPromediarCE1a = new System.Windows.Forms.Button();
            this.lblPromedioFisica1a = new System.Windows.Forms.Label();
            this.btnPromediarFis1a = new System.Windows.Forms.Button();
            this.lblPromedioQuimica1a = new System.Windows.Forms.Label();
            this.btnPromediarQuim1a = new System.Windows.Forms.Button();
            this.lblPromedioMat1a = new System.Windows.Forms.Label();
            this.btnPromediarMat1a = new System.Windows.Forms.Button();
            this.lblPromedioEspaañol1a = new System.Windows.Forms.Label();
            this.btnPromediarEsp1a = new System.Windows.Forms.Button();
            this.txtCalifHistoriaB3 = new System.Windows.Forms.TextBox();
            this.txtCalifCEB3 = new System.Windows.Forms.TextBox();
            this.txtCalifFisicaB3 = new System.Windows.Forms.TextBox();
            this.txtCalifQuimicaB3 = new System.Windows.Forms.TextBox();
            this.txtCalifMatB3 = new System.Windows.Forms.TextBox();
            this.txtCalifEspañolB3 = new System.Windows.Forms.TextBox();
            this.txtCalifHistoriaB2 = new System.Windows.Forms.TextBox();
            this.txtCalifCEB2 = new System.Windows.Forms.TextBox();
            this.txtCalifFisicaB2 = new System.Windows.Forms.TextBox();
            this.txtCalifQuimicaB2 = new System.Windows.Forms.TextBox();
            this.txtCalifMatB2 = new System.Windows.Forms.TextBox();
            this.txtCalifEspañolB2 = new System.Windows.Forms.TextBox();
            this.txtCalifHistoriaB1 = new System.Windows.Forms.TextBox();
            this.txtCalifCEB1 = new System.Windows.Forms.TextBox();
            this.txtCalifFisicaB1 = new System.Windows.Forms.TextBox();
            this.txtCalifQuimicaB1 = new System.Windows.Forms.TextBox();
            this.txtCalifMatB1 = new System.Windows.Forms.TextBox();
            this.txtCalifEspañolB1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTercerAño)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.btnConsulAlumCalif3);
            this.groupBox2.Controls.Add(this.cbSelecGrupo);
            this.groupBox2.Location = new System.Drawing.Point(480, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(252, 99);
            this.groupBox2.TabIndex = 391;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Información de Alumnos";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 13);
            this.label9.TabIndex = 299;
            this.label9.Text = "Seleccionar Grupo";
            // 
            // btnConsulAlumCalif3
            // 
            this.btnConsulAlumCalif3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnConsulAlumCalif3.BackgroundImage = global::EscolarSoft.Properties.Resources.consultar3;
            this.btnConsulAlumCalif3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConsulAlumCalif3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsulAlumCalif3.ForeColor = System.Drawing.Color.White;
            this.btnConsulAlumCalif3.Location = new System.Drawing.Point(139, 12);
            this.btnConsulAlumCalif3.Name = "btnConsulAlumCalif3";
            this.btnConsulAlumCalif3.Size = new System.Drawing.Size(86, 79);
            this.btnConsulAlumCalif3.TabIndex = 296;
            this.btnConsulAlumCalif3.Text = "Consultar\r\n";
            this.btnConsulAlumCalif3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnConsulAlumCalif3.UseVisualStyleBackColor = false;
            this.btnConsulAlumCalif3.Click += new System.EventHandler(this.btnConsulAlumCalif3_Click);
            // 
            // cbSelecGrupo
            // 
            this.cbSelecGrupo.FormattingEnabled = true;
            this.cbSelecGrupo.Items.AddRange(new object[] {
            "2-A",
            "2-B"});
            this.cbSelecGrupo.Location = new System.Drawing.Point(24, 49);
            this.cbSelecGrupo.Name = "cbSelecGrupo";
            this.cbSelecGrupo.Size = new System.Drawing.Size(89, 21);
            this.cbSelecGrupo.TabIndex = 298;
            // 
            // dgTercerAño
            // 
            this.dgTercerAño.AllowUserToAddRows = false;
            this.dgTercerAño.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTercerAño.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colGrupo,
            this.colNomAlum,
            this.colApePat,
            this.colApeMat});
            this.dgTercerAño.Location = new System.Drawing.Point(415, 150);
            this.dgTercerAño.Name = "dgTercerAño";
            this.dgTercerAño.Size = new System.Drawing.Size(392, 407);
            this.dgTercerAño.TabIndex = 390;
            // 
            // colGrupo
            // 
            this.colGrupo.DataPropertyName = "colGrupo";
            this.colGrupo.HeaderText = "Grupo";
            this.colGrupo.Name = "colGrupo";
            this.colGrupo.Width = 50;
            // 
            // colNomAlum
            // 
            this.colNomAlum.DataPropertyName = "colNomAlum";
            this.colNomAlum.HeaderText = "Nombre";
            this.colNomAlum.Name = "colNomAlum";
            // 
            // colApePat
            // 
            this.colApePat.DataPropertyName = "colApePat";
            this.colApePat.HeaderText = "Apellido Paterno";
            this.colApePat.Name = "colApePat";
            // 
            // colApeMat
            // 
            this.colApeMat.DataPropertyName = "colApeMat";
            this.colApeMat.HeaderText = "Apellido Materno";
            this.colApeMat.Name = "colApeMat";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.txtNumEmpleado);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(50, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(369, 88);
            this.groupBox1.TabIndex = 389;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del Maestro";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(152, 50);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(172, 20);
            this.textBox2.TabIndex = 2;
            // 
            // txtNumEmpleado
            // 
            this.txtNumEmpleado.Location = new System.Drawing.Point(152, 28);
            this.txtNumEmpleado.Name = "txtNumEmpleado";
            this.txtNumEmpleado.Size = new System.Drawing.Size(100, 20);
            this.txtNumEmpleado.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(99, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero de Empleado:";
            // 
            // btnGuardarCalif1a
            // 
            this.btnGuardarCalif1a.Location = new System.Drawing.Point(175, 514);
            this.btnGuardarCalif1a.Name = "btnGuardarCalif1a";
            this.btnGuardarCalif1a.Size = new System.Drawing.Size(136, 43);
            this.btnGuardarCalif1a.TabIndex = 371;
            this.btnGuardarCalif1a.Text = "Guardar";
            this.btnGuardarCalif1a.UseVisualStyleBackColor = true;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(219, 122);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(40, 13);
            this.label51.TabIndex = 388;
            this.label51.Text = "3 Año";
            // 
            // lblPromFinal1a
            // 
            this.lblPromFinal1a.AutoSize = true;
            this.lblPromFinal1a.Location = new System.Drawing.Point(259, 492);
            this.lblPromFinal1a.Name = "lblPromFinal1a";
            this.lblPromFinal1a.Size = new System.Drawing.Size(52, 13);
            this.lblPromFinal1a.TabIndex = 387;
            this.lblPromFinal1a.Text = "...............";
            // 
            // btnPromFinal1a
            // 
            this.btnPromFinal1a.Location = new System.Drawing.Point(175, 481);
            this.btnPromFinal1a.Name = "btnPromFinal1a";
            this.btnPromFinal1a.Size = new System.Drawing.Size(75, 27);
            this.btnPromFinal1a.TabIndex = 370;
            this.btnPromFinal1a.Text = "PromFinal";
            this.btnPromFinal1a.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(277, 135);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(30, 13);
            this.label29.TabIndex = 386;
            this.label29.Text = "3Bim";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(233, 135);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(30, 13);
            this.label28.TabIndex = 385;
            this.label28.Text = "2Bim";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(182, 135);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 13);
            this.label27.TabIndex = 384;
            this.label27.Text = "1Bim";
            // 
            // lblPromedioHistoria1a
            // 
            this.lblPromedioHistoria1a.AutoSize = true;
            this.lblPromedioHistoria1a.Location = new System.Drawing.Point(265, 457);
            this.lblPromedioHistoria1a.Name = "lblPromedioHistoria1a";
            this.lblPromedioHistoria1a.Size = new System.Drawing.Size(46, 13);
            this.lblPromedioHistoria1a.TabIndex = 383;
            this.lblPromedioHistoria1a.Text = ".............";
            // 
            // btnPromediarHist1a
            // 
            this.btnPromediarHist1a.Location = new System.Drawing.Point(175, 452);
            this.btnPromediarHist1a.Name = "btnPromediarHist1a";
            this.btnPromediarHist1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarHist1a.TabIndex = 369;
            this.btnPromediarHist1a.Text = "Promediar";
            this.btnPromediarHist1a.UseVisualStyleBackColor = true;
            // 
            // lblPromedioCE1a
            // 
            this.lblPromedioCE1a.AutoSize = true;
            this.lblPromedioCE1a.Location = new System.Drawing.Point(265, 402);
            this.lblPromedioCE1a.Name = "lblPromedioCE1a";
            this.lblPromedioCE1a.Size = new System.Drawing.Size(46, 13);
            this.lblPromedioCE1a.TabIndex = 382;
            this.lblPromedioCE1a.Text = ".............";
            // 
            // btnPromediarCE1a
            // 
            this.btnPromediarCE1a.Location = new System.Drawing.Point(175, 397);
            this.btnPromediarCE1a.Name = "btnPromediarCE1a";
            this.btnPromediarCE1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarCE1a.TabIndex = 365;
            this.btnPromediarCE1a.Text = "Promediar";
            this.btnPromediarCE1a.UseVisualStyleBackColor = true;
            // 
            // lblPromedioFisica1a
            // 
            this.lblPromedioFisica1a.AutoSize = true;
            this.lblPromedioFisica1a.Location = new System.Drawing.Point(265, 347);
            this.lblPromedioFisica1a.Name = "lblPromedioFisica1a";
            this.lblPromedioFisica1a.Size = new System.Drawing.Size(46, 13);
            this.lblPromedioFisica1a.TabIndex = 381;
            this.lblPromedioFisica1a.Text = ".............";
            // 
            // btnPromediarFis1a
            // 
            this.btnPromediarFis1a.Location = new System.Drawing.Point(175, 342);
            this.btnPromediarFis1a.Name = "btnPromediarFis1a";
            this.btnPromediarFis1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarFis1a.TabIndex = 361;
            this.btnPromediarFis1a.Text = "Promediar";
            this.btnPromediarFis1a.UseVisualStyleBackColor = true;
            // 
            // lblPromedioQuimica1a
            // 
            this.lblPromedioQuimica1a.AutoSize = true;
            this.lblPromedioQuimica1a.Location = new System.Drawing.Point(266, 290);
            this.lblPromedioQuimica1a.Name = "lblPromedioQuimica1a";
            this.lblPromedioQuimica1a.Size = new System.Drawing.Size(46, 13);
            this.lblPromedioQuimica1a.TabIndex = 380;
            this.lblPromedioQuimica1a.Text = ".............";
            // 
            // btnPromediarQuim1a
            // 
            this.btnPromediarQuim1a.Location = new System.Drawing.Point(175, 287);
            this.btnPromediarQuim1a.Name = "btnPromediarQuim1a";
            this.btnPromediarQuim1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarQuim1a.TabIndex = 357;
            this.btnPromediarQuim1a.Text = "Promediar";
            this.btnPromediarQuim1a.UseVisualStyleBackColor = true;
            // 
            // lblPromedioMat1a
            // 
            this.lblPromedioMat1a.AutoSize = true;
            this.lblPromedioMat1a.Location = new System.Drawing.Point(262, 237);
            this.lblPromedioMat1a.Name = "lblPromedioMat1a";
            this.lblPromedioMat1a.Size = new System.Drawing.Size(49, 13);
            this.lblPromedioMat1a.TabIndex = 379;
            this.lblPromedioMat1a.Text = "..............";
            // 
            // btnPromediarMat1a
            // 
            this.btnPromediarMat1a.Location = new System.Drawing.Point(175, 232);
            this.btnPromediarMat1a.Name = "btnPromediarMat1a";
            this.btnPromediarMat1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarMat1a.TabIndex = 353;
            this.btnPromediarMat1a.Text = "Promediar";
            this.btnPromediarMat1a.UseVisualStyleBackColor = true;
            // 
            // lblPromedioEspaañol1a
            // 
            this.lblPromedioEspaañol1a.AutoSize = true;
            this.lblPromedioEspaañol1a.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPromedioEspaañol1a.Location = new System.Drawing.Point(259, 182);
            this.lblPromedioEspaañol1a.Name = "lblPromedioEspaañol1a";
            this.lblPromedioEspaañol1a.Size = new System.Drawing.Size(52, 13);
            this.lblPromedioEspaañol1a.TabIndex = 378;
            this.lblPromedioEspaañol1a.Text = "...............";
            // 
            // btnPromediarEsp1a
            // 
            this.btnPromediarEsp1a.Location = new System.Drawing.Point(175, 177);
            this.btnPromediarEsp1a.Name = "btnPromediarEsp1a";
            this.btnPromediarEsp1a.Size = new System.Drawing.Size(75, 23);
            this.btnPromediarEsp1a.TabIndex = 349;
            this.btnPromediarEsp1a.Text = " Promediar";
            this.btnPromediarEsp1a.UseVisualStyleBackColor = true;
            // 
            // txtCalifHistoriaB3
            // 
            this.txtCalifHistoriaB3.Location = new System.Drawing.Point(269, 426);
            this.txtCalifHistoriaB3.Name = "txtCalifHistoriaB3";
            this.txtCalifHistoriaB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifHistoriaB3.TabIndex = 368;
            // 
            // txtCalifCEB3
            // 
            this.txtCalifCEB3.Location = new System.Drawing.Point(269, 371);
            this.txtCalifCEB3.Name = "txtCalifCEB3";
            this.txtCalifCEB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifCEB3.TabIndex = 364;
            // 
            // txtCalifFisicaB3
            // 
            this.txtCalifFisicaB3.Location = new System.Drawing.Point(269, 316);
            this.txtCalifFisicaB3.Name = "txtCalifFisicaB3";
            this.txtCalifFisicaB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifFisicaB3.TabIndex = 360;
            // 
            // txtCalifQuimicaB3
            // 
            this.txtCalifQuimicaB3.Location = new System.Drawing.Point(269, 261);
            this.txtCalifQuimicaB3.Name = "txtCalifQuimicaB3";
            this.txtCalifQuimicaB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifQuimicaB3.TabIndex = 356;
            // 
            // txtCalifMatB3
            // 
            this.txtCalifMatB3.Location = new System.Drawing.Point(269, 207);
            this.txtCalifMatB3.Name = "txtCalifMatB3";
            this.txtCalifMatB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifMatB3.TabIndex = 352;
            // 
            // txtCalifEspañolB3
            // 
            this.txtCalifEspañolB3.Location = new System.Drawing.Point(269, 151);
            this.txtCalifEspañolB3.Name = "txtCalifEspañolB3";
            this.txtCalifEspañolB3.Size = new System.Drawing.Size(42, 20);
            this.txtCalifEspañolB3.TabIndex = 348;
            // 
            // txtCalifHistoriaB2
            // 
            this.txtCalifHistoriaB2.Location = new System.Drawing.Point(222, 426);
            this.txtCalifHistoriaB2.Name = "txtCalifHistoriaB2";
            this.txtCalifHistoriaB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifHistoriaB2.TabIndex = 367;
            // 
            // txtCalifCEB2
            // 
            this.txtCalifCEB2.Location = new System.Drawing.Point(222, 371);
            this.txtCalifCEB2.Name = "txtCalifCEB2";
            this.txtCalifCEB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifCEB2.TabIndex = 363;
            // 
            // txtCalifFisicaB2
            // 
            this.txtCalifFisicaB2.Location = new System.Drawing.Point(222, 316);
            this.txtCalifFisicaB2.Name = "txtCalifFisicaB2";
            this.txtCalifFisicaB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifFisicaB2.TabIndex = 359;
            // 
            // txtCalifQuimicaB2
            // 
            this.txtCalifQuimicaB2.Location = new System.Drawing.Point(222, 261);
            this.txtCalifQuimicaB2.Name = "txtCalifQuimicaB2";
            this.txtCalifQuimicaB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifQuimicaB2.TabIndex = 355;
            // 
            // txtCalifMatB2
            // 
            this.txtCalifMatB2.Location = new System.Drawing.Point(222, 207);
            this.txtCalifMatB2.Name = "txtCalifMatB2";
            this.txtCalifMatB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifMatB2.TabIndex = 351;
            // 
            // txtCalifEspañolB2
            // 
            this.txtCalifEspañolB2.Location = new System.Drawing.Point(222, 151);
            this.txtCalifEspañolB2.Name = "txtCalifEspañolB2";
            this.txtCalifEspañolB2.Size = new System.Drawing.Size(41, 20);
            this.txtCalifEspañolB2.TabIndex = 347;
            // 
            // txtCalifHistoriaB1
            // 
            this.txtCalifHistoriaB1.Location = new System.Drawing.Point(175, 426);
            this.txtCalifHistoriaB1.Name = "txtCalifHistoriaB1";
            this.txtCalifHistoriaB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifHistoriaB1.TabIndex = 366;
            // 
            // txtCalifCEB1
            // 
            this.txtCalifCEB1.Location = new System.Drawing.Point(175, 371);
            this.txtCalifCEB1.Name = "txtCalifCEB1";
            this.txtCalifCEB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifCEB1.TabIndex = 362;
            // 
            // txtCalifFisicaB1
            // 
            this.txtCalifFisicaB1.Location = new System.Drawing.Point(175, 316);
            this.txtCalifFisicaB1.Name = "txtCalifFisicaB1";
            this.txtCalifFisicaB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifFisicaB1.TabIndex = 358;
            // 
            // txtCalifQuimicaB1
            // 
            this.txtCalifQuimicaB1.Location = new System.Drawing.Point(175, 261);
            this.txtCalifQuimicaB1.Name = "txtCalifQuimicaB1";
            this.txtCalifQuimicaB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifQuimicaB1.TabIndex = 354;
            // 
            // txtCalifMatB1
            // 
            this.txtCalifMatB1.Location = new System.Drawing.Point(175, 206);
            this.txtCalifMatB1.Name = "txtCalifMatB1";
            this.txtCalifMatB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifMatB1.TabIndex = 350;
            // 
            // txtCalifEspañolB1
            // 
            this.txtCalifEspañolB1.Location = new System.Drawing.Point(175, 151);
            this.txtCalifEspañolB1.Name = "txtCalifEspañolB1";
            this.txtCalifEspañolB1.Size = new System.Drawing.Size(41, 20);
            this.txtCalifEspañolB1.TabIndex = 346;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(117, 429);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 377;
            this.label8.Text = "Quimica3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(125, 374);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 376;
            this.label7.Text = "Fisica3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(61, 319);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 375;
            this.label6.Text = "Desarrollo Personal2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(90, 264);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 374;
            this.label5.Text = "Administracion";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(117, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 373;
            this.label4.Text = "Calculo2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(114, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 372;
            this.label3.Text = "Español3";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 566);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(870, 22);
            this.statusStrip1.TabIndex = 392;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(135, 17);
            this.toolStripStatusLabel1.Text = "Evaluaciones Tercer Año";
            // 
            // FrmCalifTercerAño
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(870, 588);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgTercerAño);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnGuardarCalif1a);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.lblPromFinal1a);
            this.Controls.Add(this.btnPromFinal1a);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.lblPromedioHistoria1a);
            this.Controls.Add(this.btnPromediarHist1a);
            this.Controls.Add(this.lblPromedioCE1a);
            this.Controls.Add(this.btnPromediarCE1a);
            this.Controls.Add(this.lblPromedioFisica1a);
            this.Controls.Add(this.btnPromediarFis1a);
            this.Controls.Add(this.lblPromedioQuimica1a);
            this.Controls.Add(this.btnPromediarQuim1a);
            this.Controls.Add(this.lblPromedioMat1a);
            this.Controls.Add(this.btnPromediarMat1a);
            this.Controls.Add(this.lblPromedioEspaañol1a);
            this.Controls.Add(this.btnPromediarEsp1a);
            this.Controls.Add(this.txtCalifHistoriaB3);
            this.Controls.Add(this.txtCalifCEB3);
            this.Controls.Add(this.txtCalifFisicaB3);
            this.Controls.Add(this.txtCalifQuimicaB3);
            this.Controls.Add(this.txtCalifMatB3);
            this.Controls.Add(this.txtCalifEspañolB3);
            this.Controls.Add(this.txtCalifHistoriaB2);
            this.Controls.Add(this.txtCalifCEB2);
            this.Controls.Add(this.txtCalifFisicaB2);
            this.Controls.Add(this.txtCalifQuimicaB2);
            this.Controls.Add(this.txtCalifMatB2);
            this.Controls.Add(this.txtCalifEspañolB2);
            this.Controls.Add(this.txtCalifHistoriaB1);
            this.Controls.Add(this.txtCalifCEB1);
            this.Controls.Add(this.txtCalifFisicaB1);
            this.Controls.Add(this.txtCalifQuimicaB1);
            this.Controls.Add(this.txtCalifMatB1);
            this.Controls.Add(this.txtCalifEspañolB1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Name = "FrmCalifTercerAño";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmCalifTercerAño";
            this.Load += new System.EventHandler(this.FrmCalifTercerAño_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTercerAño)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnConsulAlumCalif3;
        private System.Windows.Forms.ComboBox cbSelecGrupo;
        private System.Windows.Forms.DataGridView dgTercerAño;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txtNumEmpleado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGuardarCalif1a;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label lblPromFinal1a;
        private System.Windows.Forms.Button btnPromFinal1a;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblPromedioHistoria1a;
        private System.Windows.Forms.Button btnPromediarHist1a;
        private System.Windows.Forms.Label lblPromedioCE1a;
        private System.Windows.Forms.Button btnPromediarCE1a;
        private System.Windows.Forms.Label lblPromedioFisica1a;
        private System.Windows.Forms.Button btnPromediarFis1a;
        private System.Windows.Forms.Label lblPromedioQuimica1a;
        private System.Windows.Forms.Button btnPromediarQuim1a;
        private System.Windows.Forms.Label lblPromedioMat1a;
        private System.Windows.Forms.Button btnPromediarMat1a;
        private System.Windows.Forms.Label lblPromedioEspaañol1a;
        private System.Windows.Forms.Button btnPromediarEsp1a;
        private System.Windows.Forms.TextBox txtCalifHistoriaB3;
        private System.Windows.Forms.TextBox txtCalifCEB3;
        private System.Windows.Forms.TextBox txtCalifFisicaB3;
        private System.Windows.Forms.TextBox txtCalifQuimicaB3;
        private System.Windows.Forms.TextBox txtCalifMatB3;
        private System.Windows.Forms.TextBox txtCalifEspañolB3;
        private System.Windows.Forms.TextBox txtCalifHistoriaB2;
        private System.Windows.Forms.TextBox txtCalifCEB2;
        private System.Windows.Forms.TextBox txtCalifFisicaB2;
        private System.Windows.Forms.TextBox txtCalifQuimicaB2;
        private System.Windows.Forms.TextBox txtCalifMatB2;
        private System.Windows.Forms.TextBox txtCalifEspañolB2;
        private System.Windows.Forms.TextBox txtCalifHistoriaB1;
        private System.Windows.Forms.TextBox txtCalifCEB1;
        private System.Windows.Forms.TextBox txtCalifFisicaB1;
        private System.Windows.Forms.TextBox txtCalifQuimicaB1;
        private System.Windows.Forms.TextBox txtCalifMatB1;
        private System.Windows.Forms.TextBox txtCalifEspañolB1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGrupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNomAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn colApePat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colApeMat;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}