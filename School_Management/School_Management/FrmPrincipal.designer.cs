﻿namespace School_Management
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmIngresar = new System.Windows.Forms.ToolStripMenuItem();
            this.inisiarSesiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRegistroEscolar = new System.Windows.Forms.ToolStripMenuItem();
            this.administracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMaestro = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluar1erAñoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluar2doAñoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluar3erAAñoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAlumno = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarCalificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmReportes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAyuda = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmIngresar,
            this.tsmRegistroEscolar,
            this.tsmMaestro,
            this.tsmAlumno,
            this.tsmReportes,
            this.tsmAyuda});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(743, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmIngresar
            // 
            this.tsmIngresar.BackColor = System.Drawing.SystemColors.Control;
            this.tsmIngresar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inisiarSesiónToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.tsmIngresar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tsmIngresar.Name = "tsmIngresar";
            this.tsmIngresar.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.tsmIngresar.Size = new System.Drawing.Size(71, 25);
            this.tsmIngresar.Text = "Ingresar";
            // 
            // inisiarSesiónToolStripMenuItem
            // 
            this.inisiarSesiónToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inisiarSesiónToolStripMenuItem.Name = "inisiarSesiónToolStripMenuItem";
            this.inisiarSesiónToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.inisiarSesiónToolStripMenuItem.Text = "Inisiar Sesión";
            this.inisiarSesiónToolStripMenuItem.Click += new System.EventHandler(this.inisiarSesiónToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // tsmRegistroEscolar
            // 
            this.tsmRegistroEscolar.BackColor = System.Drawing.SystemColors.Control;
            this.tsmRegistroEscolar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tsmRegistroEscolar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administracionToolStripMenuItem});
            this.tsmRegistroEscolar.Name = "tsmRegistroEscolar";
            this.tsmRegistroEscolar.Size = new System.Drawing.Size(133, 25);
            this.tsmRegistroEscolar.Text = "Registro Escolar";
            this.tsmRegistroEscolar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // administracionToolStripMenuItem
            // 
            this.administracionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.administracionToolStripMenuItem.Name = "administracionToolStripMenuItem";
            this.administracionToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.administracionToolStripMenuItem.Text = "Administracion";
            this.administracionToolStripMenuItem.Click += new System.EventHandler(this.administracionToolStripMenuItem_Click);
            // 
            // tsmMaestro
            // 
            this.tsmMaestro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evaluacionesToolStripMenuItem});
            this.tsmMaestro.Name = "tsmMaestro";
            this.tsmMaestro.Size = new System.Drawing.Size(79, 25);
            this.tsmMaestro.Text = "Maestro";
            // 
            // evaluacionesToolStripMenuItem
            // 
            this.evaluacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evaluar1erAñoToolStripMenuItem,
            this.evaluar2doAñoToolStripMenuItem,
            this.evaluar3erAAñoToolStripMenuItem});
            this.evaluacionesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluacionesToolStripMenuItem.Name = "evaluacionesToolStripMenuItem";
            this.evaluacionesToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.evaluacionesToolStripMenuItem.Text = "Evaluaciones";
            // 
            // evaluar1erAñoToolStripMenuItem
            // 
            this.evaluar1erAñoToolStripMenuItem.Name = "evaluar1erAñoToolStripMenuItem";
            this.evaluar1erAñoToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.evaluar1erAñoToolStripMenuItem.Text = "Evaluar 1er Año";
            this.evaluar1erAñoToolStripMenuItem.Click += new System.EventHandler(this.evaluar1erAñoToolStripMenuItem_Click);
            // 
            // evaluar2doAñoToolStripMenuItem
            // 
            this.evaluar2doAñoToolStripMenuItem.Name = "evaluar2doAñoToolStripMenuItem";
            this.evaluar2doAñoToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.evaluar2doAñoToolStripMenuItem.Text = "Evaluar 2do Año";
            this.evaluar2doAñoToolStripMenuItem.Click += new System.EventHandler(this.evaluar2doAñoToolStripMenuItem_Click);
            // 
            // evaluar3erAAñoToolStripMenuItem
            // 
            this.evaluar3erAAñoToolStripMenuItem.Name = "evaluar3erAAñoToolStripMenuItem";
            this.evaluar3erAAñoToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.evaluar3erAAñoToolStripMenuItem.Text = "Evaluar 3er AAño";
            this.evaluar3erAAñoToolStripMenuItem.Click += new System.EventHandler(this.evaluar3erAAñoToolStripMenuItem_Click);
            // 
            // tsmAlumno
            // 
            this.tsmAlumno.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarCalificacionesToolStripMenuItem});
            this.tsmAlumno.Name = "tsmAlumno";
            this.tsmAlumno.Size = new System.Drawing.Size(77, 25);
            this.tsmAlumno.Text = "Alumno";
            // 
            // consultarCalificacionesToolStripMenuItem
            // 
            this.consultarCalificacionesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consultarCalificacionesToolStripMenuItem.Name = "consultarCalificacionesToolStripMenuItem";
            this.consultarCalificacionesToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.consultarCalificacionesToolStripMenuItem.Text = "Consultar Calificaciones";
            this.consultarCalificacionesToolStripMenuItem.Click += new System.EventHandler(this.consultarCalificacionesToolStripMenuItem_Click);
            // 
            // tsmReportes
            // 
            this.tsmReportes.Name = "tsmReportes";
            this.tsmReportes.Size = new System.Drawing.Size(84, 25);
            this.tsmReportes.Text = "Reportes";
            // 
            // tsmAyuda
            // 
            this.tsmAyuda.Name = "tsmAyuda";
            this.tsmAyuda.Size = new System.Drawing.Size(66, 25);
            this.tsmAyuda.Text = "Ayuda";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 408);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(743, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(86, 17);
            this.toolStripStatusLabel1.Text = "Menu de Inicio";
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(743, 430);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmPrincipal";
            this.Text = "School Management";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmIngresar;
        private System.Windows.Forms.ToolStripMenuItem inisiarSesiónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmRegistroEscolar;
        private System.Windows.Forms.ToolStripMenuItem administracionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmMaestro;
        private System.Windows.Forms.ToolStripMenuItem evaluacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluar1erAñoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluar2doAñoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluar3erAAñoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmAlumno;
        private System.Windows.Forms.ToolStripMenuItem consultarCalificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmReportes;
        private System.Windows.Forms.ToolStripMenuItem tsmAyuda;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}

