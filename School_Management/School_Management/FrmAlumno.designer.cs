﻿namespace School_Management
{
    partial class FrmAlumno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtContrasenaAlum = new System.Windows.Forms.TextBox();
            this.cbTurno = new System.Windows.Forms.ComboBox();
            this.txtUsuarioAlum = new System.Windows.Forms.TextBox();
            this.cbGrupo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbMaterias = new System.Windows.Forms.ComboBox();
            this.dtpFechaNac = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.txtDir = new System.Windows.Forms.TextBox();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtApMat = new System.Windows.Forms.TextBox();
            this.txtApPat = new System.Windows.Forms.TextBox();
            this.pbFoto = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgAlumnos = new System.Windows.Forms.DataGridView();
            this.Colid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColApat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFechaNac = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColMat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColGrup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTurno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFoto = new System.Windows.Forms.DataGridViewImageColumn();
            this.colRuta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblCont = new System.Windows.Forms.Label();
            this.lblmatri = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.button4 = new System.Windows.Forms.Button();
            this.btnModificarAlum = new System.Windows.Forms.Button();
            this.btnEliminarAlum = new System.Windows.Forms.Button();
            this.btnAgregarAlum = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAlumnos)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtContrasenaAlum);
            this.groupBox1.Controls.Add(this.cbTurno);
            this.groupBox1.Controls.Add(this.txtUsuarioAlum);
            this.groupBox1.Controls.Add(this.cbGrupo);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cbMaterias);
            this.groupBox1.Controls.Add(this.dtpFechaNac);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtTel);
            this.groupBox1.Controls.Add(this.txtDir);
            this.groupBox1.Controls.Add(this.txtCurp);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.txtApMat);
            this.groupBox1.Controls.Add(this.txtApPat);
            this.groupBox1.Controls.Add(this.pbFoto);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(882, 245);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del Alumno";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(346, 134);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(178, 15);
            this.label15.TabIndex = 28;
            this.label15.Text = "Asignar Cuenta de Usuario";
            // 
            // txtContrasenaAlum
            // 
            this.txtContrasenaAlum.Location = new System.Drawing.Point(411, 183);
            this.txtContrasenaAlum.Name = "txtContrasenaAlum";
            this.txtContrasenaAlum.Size = new System.Drawing.Size(113, 20);
            this.txtContrasenaAlum.TabIndex = 26;
            this.txtContrasenaAlum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContrasenaAlum_KeyPress);
            // 
            // cbTurno
            // 
            this.cbTurno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTurno.FormattingEnabled = true;
            this.cbTurno.Items.AddRange(new object[] {
            "",
            "Matutino",
            "Vespertino"});
            this.cbTurno.Location = new System.Drawing.Point(349, 88);
            this.cbTurno.Name = "cbTurno";
            this.cbTurno.Size = new System.Drawing.Size(121, 21);
            this.cbTurno.TabIndex = 21;
            // 
            // txtUsuarioAlum
            // 
            this.txtUsuarioAlum.Location = new System.Drawing.Point(411, 157);
            this.txtUsuarioAlum.Name = "txtUsuarioAlum";
            this.txtUsuarioAlum.Size = new System.Drawing.Size(113, 20);
            this.txtUsuarioAlum.TabIndex = 25;
            this.txtUsuarioAlum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUsuarioAlum_KeyPress);
            // 
            // cbGrupo
            // 
            this.cbGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGrupo.FormattingEnabled = true;
            this.cbGrupo.Items.AddRange(new object[] {
            "",
            "1-A",
            "1-B",
            "2-A",
            "2-B",
            "3-A",
            "3-B"});
            this.cbGrupo.Location = new System.Drawing.Point(349, 61);
            this.cbGrupo.Name = "cbGrupo";
            this.cbGrupo.Size = new System.Drawing.Size(121, 21);
            this.cbGrupo.TabIndex = 20;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(341, 186);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "Contraseña:";
            // 
            // cbMaterias
            // 
            this.cbMaterias.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterias.FormattingEnabled = true;
            this.cbMaterias.Items.AddRange(new object[] {
            "",
            "1.-(Esp, Mat, Quimica, Fisica, FormC.E, Hist)",
            "2.-(Esp2, Calc1, Quimica2, Fisica2, DesarrolloPersonal, Historia2)",
            "3.-(Esp3, Calc2, Admin, DesarrolloPersonal2, Fisica3, Quimica3)"});
            this.cbMaterias.Location = new System.Drawing.Point(349, 34);
            this.cbMaterias.Name = "cbMaterias";
            this.cbMaterias.Size = new System.Drawing.Size(269, 21);
            this.cbMaterias.TabIndex = 19;
            // 
            // dtpFechaNac
            // 
            this.dtpFechaNac.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNac.Location = new System.Drawing.Point(134, 108);
            this.dtpFechaNac.Name = "dtpFechaNac";
            this.dtpFechaNac.Size = new System.Drawing.Size(118, 20);
            this.dtpFechaNac.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(365, 160);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Usurio:";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(134, 158);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(100, 20);
            this.txtTel.TabIndex = 17;
            this.txtTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTel_KeyPress);
            // 
            // txtDir
            // 
            this.txtDir.Location = new System.Drawing.Point(134, 134);
            this.txtDir.Name = "txtDir";
            this.txtDir.Size = new System.Drawing.Size(100, 20);
            this.txtDir.TabIndex = 16;
            this.txtDir.TextChanged += new System.EventHandler(this.txtDir_TextChanged);
            this.txtDir.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDir_KeyPress);
            // 
            // txtCurp
            // 
            this.txtCurp.Location = new System.Drawing.Point(134, 184);
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(141, 20);
            this.txtCurp.TabIndex = 18;
            this.txtCurp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurp_KeyPress);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(134, 85);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 14;
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // txtApMat
            // 
            this.txtApMat.Location = new System.Drawing.Point(134, 62);
            this.txtApMat.Name = "txtApMat";
            this.txtApMat.Size = new System.Drawing.Size(100, 20);
            this.txtApMat.TabIndex = 13;
            this.txtApMat.TextChanged += new System.EventHandler(this.txtApMat_TextChanged);
            this.txtApMat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApMat_KeyPress);
            // 
            // txtApPat
            // 
            this.txtApPat.Location = new System.Drawing.Point(134, 36);
            this.txtApPat.Name = "txtApPat";
            this.txtApPat.Size = new System.Drawing.Size(100, 20);
            this.txtApPat.TabIndex = 12;
            this.txtApPat.TextChanged += new System.EventHandler(this.txtApPat_TextChanged);
            this.txtApPat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApPat_KeyPress);
            // 
            // pbFoto
            // 
            this.pbFoto.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.pbFoto.Image = global::EscolarSoft.Properties.Resources.agregar;
            this.pbFoto.Location = new System.Drawing.Point(692, 62);
            this.pbFoto.Name = "pbFoto";
            this.pbFoto.Size = new System.Drawing.Size(124, 127);
            this.pbFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFoto.TabIndex = 11;
            this.pbFoto.TabStop = false;
            this.pbFoto.Click += new System.EventHandler(this.pbFoto_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(650, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(211, 15);
            this.label11.TabIndex = 10;
            this.label11.Text = "Seleccionar Imagen del Alumno";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(305, 91);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Turno:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(305, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Grupo:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(293, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Materias:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(88, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "CURP:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(76, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Teléfono:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(73, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Dirección:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Fecha Nacimiento:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido Materno:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Apellido Paterno:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgAlumnos);
            this.groupBox2.Location = new System.Drawing.Point(12, 353);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(888, 307);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Alumnos Inscritos";
            // 
            // dgAlumnos
            // 
            this.dgAlumnos.AllowUserToAddRows = false;
            this.dgAlumnos.AllowUserToDeleteRows = false;
            this.dgAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAlumnos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Colid,
            this.ColApat,
            this.colAmat,
            this.ColNom,
            this.ColFechaNac,
            this.ColMat,
            this.ColGrup,
            this.ColTurno,
            this.ColFoto,
            this.colRuta});
            this.dgAlumnos.Location = new System.Drawing.Point(6, 19);
            this.dgAlumnos.Name = "dgAlumnos";
            this.dgAlumnos.ReadOnly = true;
            this.dgAlumnos.Size = new System.Drawing.Size(876, 282);
            this.dgAlumnos.TabIndex = 26;
            this.dgAlumnos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAlumnos_CellClick);
            this.dgAlumnos.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAlumnos_CellEnter);
            // 
            // Colid
            // 
            this.Colid.DataPropertyName = "Colid";
            this.Colid.HeaderText = "Matricula";
            this.Colid.Name = "Colid";
            this.Colid.ReadOnly = true;
            this.Colid.Width = 60;
            // 
            // ColApat
            // 
            this.ColApat.DataPropertyName = "ColApat";
            this.ColApat.HeaderText = "Apellido Paterno";
            this.ColApat.Name = "ColApat";
            this.ColApat.ReadOnly = true;
            // 
            // colAmat
            // 
            this.colAmat.DataPropertyName = "colAmat";
            this.colAmat.HeaderText = "Apellido Materno";
            this.colAmat.Name = "colAmat";
            this.colAmat.ReadOnly = true;
            // 
            // ColNom
            // 
            this.ColNom.DataPropertyName = "ColNom";
            this.ColNom.HeaderText = "Nombre";
            this.ColNom.Name = "ColNom";
            this.ColNom.ReadOnly = true;
            // 
            // ColFechaNac
            // 
            this.ColFechaNac.DataPropertyName = "ColFechaNac";
            this.ColFechaNac.HeaderText = "Fecha Nacimiento";
            this.ColFechaNac.Name = "ColFechaNac";
            this.ColFechaNac.ReadOnly = true;
            // 
            // ColMat
            // 
            this.ColMat.DataPropertyName = "ColMat";
            this.ColMat.HeaderText = "Materias";
            this.ColMat.Name = "ColMat";
            this.ColMat.ReadOnly = true;
            // 
            // ColGrup
            // 
            this.ColGrup.DataPropertyName = "ColGrup";
            this.ColGrup.HeaderText = "Grupo";
            this.ColGrup.Name = "ColGrup";
            this.ColGrup.ReadOnly = true;
            // 
            // ColTurno
            // 
            this.ColTurno.DataPropertyName = "ColTurno";
            this.ColTurno.HeaderText = "Turno";
            this.ColTurno.Name = "ColTurno";
            this.ColTurno.ReadOnly = true;
            // 
            // ColFoto
            // 
            this.ColFoto.DataPropertyName = "ColFoto";
            this.ColFoto.FillWeight = 80F;
            this.ColFoto.HeaderText = "Foto";
            this.ColFoto.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.ColFoto.Name = "ColFoto";
            this.ColFoto.ReadOnly = true;
            this.ColFoto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColFoto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColFoto.Width = 50;
            // 
            // colRuta
            // 
            this.colRuta.DataPropertyName = "colRuta";
            this.colRuta.HeaderText = "Ubicación";
            this.colRuta.Name = "colRuta";
            this.colRuta.ReadOnly = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblCont
            // 
            this.lblCont.AutoSize = true;
            this.lblCont.Location = new System.Drawing.Point(817, 337);
            this.lblCont.Name = "lblCont";
            this.lblCont.Size = new System.Drawing.Size(41, 13);
            this.lblCont.TabIndex = 26;
            this.lblCont.Text = "label12";
            this.lblCont.Visible = false;
            // 
            // lblmatri
            // 
            this.lblmatri.AutoSize = true;
            this.lblmatri.Location = new System.Drawing.Point(760, 337);
            this.lblmatri.Name = "lblmatri";
            this.lblmatri.Size = new System.Drawing.Size(41, 13);
            this.lblmatri.TabIndex = 27;
            this.lblmatri.Text = "label16";
            this.lblmatri.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 663);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(912, 22);
            this.statusStrip1.TabIndex = 28;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(132, 17);
            this.toolStripStatusLabel1.Text = "Inscripción de Alumnos";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ControlDark;
            //this.button4.BackgroundImage = global::EscolarSoft.Properties.Resources.consultar2;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(591, 280);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(81, 67);
            this.button4.TabIndex = 25;
            this.button4.Text = "Consultar";
            this.button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnModificarAlum
            // 
            this.btnModificarAlum.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnModificarAlum.BackgroundImage = global::EscolarSoft.Properties.Resources.modificarAlumno;
            this.btnModificarAlum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnModificarAlum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarAlum.ForeColor = System.Drawing.Color.White;
            this.btnModificarAlum.Location = new System.Drawing.Point(400, 281);
            this.btnModificarAlum.Name = "btnModificarAlum";
            this.btnModificarAlum.Size = new System.Drawing.Size(75, 66);
            this.btnModificarAlum.TabIndex = 24;
            this.btnModificarAlum.Text = "Modificar";
            this.btnModificarAlum.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnModificarAlum.UseVisualStyleBackColor = false;
            this.btnModificarAlum.Click += new System.EventHandler(this.btnModificarAlum_Click);
            // 
            // btnEliminarAlum
            // 
            this.btnEliminarAlum.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnEliminarAlum.BackgroundImage = global::EscolarSoft.Properties.Resources.eliminarEstudiante;
            this.btnEliminarAlum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminarAlum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarAlum.ForeColor = System.Drawing.Color.White;
            this.btnEliminarAlum.Location = new System.Drawing.Point(308, 282);
            this.btnEliminarAlum.Name = "btnEliminarAlum";
            this.btnEliminarAlum.Size = new System.Drawing.Size(75, 64);
            this.btnEliminarAlum.TabIndex = 23;
            this.btnEliminarAlum.Text = "Eliminar";
            this.btnEliminarAlum.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnEliminarAlum.UseVisualStyleBackColor = false;
            this.btnEliminarAlum.Click += new System.EventHandler(this.btnEliminarAlum_Click);
            // 
            // btnAgregarAlum
            // 
            this.btnAgregarAlum.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnAgregarAlum.BackgroundImage = global::EscolarSoft.Properties.Resources.agregarAlumno1;
            this.btnAgregarAlum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAgregarAlum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarAlum.ForeColor = System.Drawing.Color.White;
            this.btnAgregarAlum.Location = new System.Drawing.Point(216, 282);
            this.btnAgregarAlum.Name = "btnAgregarAlum";
            this.btnAgregarAlum.Size = new System.Drawing.Size(75, 64);
            this.btnAgregarAlum.TabIndex = 22;
            this.btnAgregarAlum.Text = "Agregar";
            this.btnAgregarAlum.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAgregarAlum.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnAgregarAlum.UseVisualStyleBackColor = false;
            this.btnAgregarAlum.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(912, 685);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblmatri);
            this.Controls.Add(this.lblCont);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnModificarAlum);
            this.Controls.Add(this.btnEliminarAlum);
            this.Controls.Add(this.btnAgregarAlum);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmAlumno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administracion de Alumnos";
            this.Load += new System.EventHandler(this.FrmAlumno_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgAlumnos)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAgregarAlum;
        private System.Windows.Forms.Button btnEliminarAlum;
        private System.Windows.Forms.Button btnModificarAlum;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox cbTurno;
        private System.Windows.Forms.ComboBox cbGrupo;
        private System.Windows.Forms.ComboBox cbMaterias;
        private System.Windows.Forms.DateTimePicker dtpFechaNac;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.TextBox txtDir;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtApMat;
        private System.Windows.Forms.TextBox txtApPat;
        private System.Windows.Forms.PictureBox pbFoto;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgAlumnos;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblCont;
        private System.Windows.Forms.DataGridViewTextBoxColumn Colid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColApat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFechaNac;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColMat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColGrup;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTurno;
        private System.Windows.Forms.DataGridViewImageColumn ColFoto;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRuta;
        private System.Windows.Forms.TextBox txtContrasenaAlum;
        private System.Windows.Forms.TextBox txtUsuarioAlum;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblmatri;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}