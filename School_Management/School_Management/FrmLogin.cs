﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibSM;

namespace School_Management
{
    public partial class FrmLogin : Form
    {
        Alumno alum = new Alumno();
        List<Alumno> listaUsuarios = new List<Alumno>();
        Maestro maestro = new Maestro();
        List<Maestro> listaMaestro = new List<Maestro>();
        Administrativo admin = new Administrativo();
        List<Administrativo> listaAdministrativo = new List<Administrativo>();
        FrmPrincipal princ = new FrmPrincipal();
        FrmInfoUsuario infousu = new FrmInfoUsuario();
        
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            
            
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void cbTipoUsuLogin_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            listaAdministrativo = admin.enlistarAdmin();
            listaMaestro = maestro.enlistarMaestro();
            listaUsuarios = alum.enlistarAlumno();
            bool aux = false;

            if (cbTipoUsuLogin.Text == "Administrativo")
            {
                for (int i = 0; i < listaAdministrativo.Count; i++)
                {
                    if (txtUsuarioAlum.Text == listaAdministrativo[i].UsuarioAdmin && txtContraAlum.Text == listaAdministrativo[i].ContrasenaAdmin)
                    {
                        MessageBox.Show("Bienvenido " + listaAdministrativo[i].Nombre + " " + listaAdministrativo[i].ApellidoPaterno + " ", "Bienvenido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        infousu.fotoUsu = listaAdministrativo[i].Foto;
                        infousu.tipUsu = listaAdministrativo[i].NumEmpleado.ToString();
                        princ.opcionActivar = "Administrativo";
                        infousu.apPat = listaAdministrativo[i].ApellidoPaterno;
                        infousu.apMat = listaAdministrativo[i].ApellidoMaterno;
                        infousu.nommbreUsu = listaAdministrativo[i].Nombre;

                        princ.Show();
                        aux = false;
                        this.Close();
                        break;
                    }
                    else
                    {
                        aux = true;
                    }

                }
                if (aux)
                {
                    MessageBox.Show("Error al ingresar.. Usuario incorrecto verifique los datos", " Acceso Denegado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUsuarioAlum.Text = "";
                    txtContraAlum.Text = "";
                    aux = false;
                }
            }
            else if (cbTipoUsuLogin.Text == "Alumno")
            {
                for (int i = 0; i < listaUsuarios.Count; i++)
                {
                    if (txtUsuarioAlum.Text == listaUsuarios[i].Usuario && txtContraAlum.Text == listaUsuarios[i].Contrasena)
                    {
                        MessageBox.Show("Bienvenido " + listaUsuarios[i].Nombre + " " + listaUsuarios[i].ApellidoPaterno + " ", "Bienvenido", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        infousu.fotoUsu = listaUsuarios[i].Foto;
                        infousu.tipUsu = listaUsuarios[i].Matricula.ToString();
                        princ.opcionActivar = "Alumno";
                        infousu.apPat = listaUsuarios[i].ApellidoPaterno;
                        infousu.apMat = listaUsuarios[i].ApellidoMaterno;
                        infousu.nommbreUsu = listaUsuarios[i].Nombre;

                        princ.Show();
                        aux = false;
                        this.Close();
                        break;
                    }
                    else
                    {
                        aux = true;
                    }

                }
                if (aux)
                {
                    MessageBox.Show("Error al ingresar.. Usuario incorrecto verifique los datos", " Acceso Denegado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUsuarioAlum.Text = "";
                    txtContraAlum.Text = "";
                    aux = false;
                }
            }
            else if (cbTipoUsuLogin.Text == "Maestro")
            {


                for (int i = 0; i < listaMaestro.Count; i++)
                {
                    if (txtUsuarioAlum.Text == listaMaestro[i].UsuarioMaestro && txtContraAlum.Text == listaMaestro[i].ContrasenaMaestro)
                    {
                        MessageBox.Show("Bienvenido " + listaMaestro[i].Nombre + " " + listaMaestro[i].ApellidoPaterno + " ", "Bienvenido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        infousu.fotoUsu = listaMaestro[i].Foto;
                        infousu.tipUsu = listaMaestro[i].NumEmpleado.ToString();
                        princ.opcionActivar = "Maestro";
                        infousu.apPat = listaMaestro[i].ApellidoPaterno;
                        infousu.apMat = listaMaestro[i].ApellidoMaterno;
                        infousu.nommbreUsu = listaMaestro[i].Nombre;

                        princ.Show();
                        aux = false;
                        this.Close();
                        break;
                    }
                    else
                    {
                        aux = true;
                    }

                }
                if (aux)
                {
                    MessageBox.Show("Error al ingresar.. Usuario incorrecto verifique los datos", " Acceso Denegado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUsuarioAlum.Text = "";
                    txtContraAlum.Text = "";
                    aux = false;
                }
            }
        }
    }
}
