﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibSM;

namespace School_Management
{
    public partial class FrmCalifSegundoAño : Form
    {
        Grupos grupos2 = new Grupos();
        List<Grupos> enlistarAlumnos = new List<Grupos>();

        public FrmCalifSegundoAño()
        {
            InitializeComponent();
        }

        private void FrmCalifSegundoAño_Load(object sender, EventArgs e)
        {

        }

        private void btnConsulAlumCalif2_Click(object sender, EventArgs e)
        {
            if (cbSelecGrupo.Text == "2-A")
            {
                cargarGridA();
            }
            else if (cbSelecGrupo.Text == "2-B")
            {
                cargarGridB();
            }
        }

        public void cargarGridA()
        {
            DataTable tabla = new DataTable();
            tabla.Columns.Add("colGrupo");
            tabla.Columns.Add("colNomAlum");
            tabla.Columns.Add("colApePat");
            tabla.Columns.Add("colApeMat");

            enlistarAlumnos = grupos2.datosCombinados2a();
            for (int i = 0; i < enlistarAlumnos.Count; i++)
            {
                DataRow row = tabla.NewRow();
                row["colGrupo"] = enlistarAlumnos[i].NomGrupo;
                row["colNomAlum"] = enlistarAlumnos[i].NombreAlumno;
                row["colApePat"] = enlistarAlumnos[i].ApelPater;
                row["colApeMat"] = enlistarAlumnos[i].ApelMater;

                tabla.Rows.Add(row);
            }
            dgSegundoAño.DataSource = tabla;
        }
        public void cargarGridB()
        {
            DataTable tabla = new DataTable();
            tabla.Columns.Add("colGrupo");
            tabla.Columns.Add("colNomAlum");
            tabla.Columns.Add("colApePat");
            tabla.Columns.Add("colApeMat");

            enlistarAlumnos = grupos2.datosCombinados2b();
            for (int i = 0; i < enlistarAlumnos.Count; i++)
            {
                DataRow row = tabla.NewRow();
                row["colGrupo"] = enlistarAlumnos[i].NomGrupo;
                row["colNomAlum"] = enlistarAlumnos[i].NombreAlumno;
                row["colApePat"] = enlistarAlumnos[i].ApelPater;
                row["colApeMat"] = enlistarAlumnos[i].ApelMater;

                tabla.Rows.Add(row);
            }
            dgSegundoAño.DataSource = tabla;
        }
    }
}
