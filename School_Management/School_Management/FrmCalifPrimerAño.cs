﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibSM;
 

namespace School_Management
{
    public partial class FrmCalifPrimerAño : Form
    {
        public FrmCalifPrimerAño()
        {
            InitializeComponent();
        }

        string materias = "";

        Calificar calificaciones = new Calificar();
        Grupos grupos = new Grupos();
        Maestro maestros = new Maestro();
        List<Maestro> listaDeMaestros = new List<Maestro>();
        List<Grupos> enlistarAlumnos = new List<Grupos>();
        private void btnPromediarEsp1a_Click(object sender, EventArgs e)
        {
            calificaciones.IdAlumno = int.Parse(txtMatricula.Text);
            lblPromedioEspaañol1a.Text = ((double.Parse(txtCalifEspañolB1.Text) + double.Parse(txtCalifEspañolB2.Text) + double.Parse(txtCalifEspañolB3.Text)) / 3).ToString();
            calificaciones.CalFinal = double.Parse(lblPromedioEspaañol1a.Text);
            calificaciones.modificarCalificaciones();

        }

        private void btnPromediarMat1a_Click(object sender, EventArgs e)
        {
            calificaciones.IdAlumno = int.Parse(txtMatricula.Text);
            lblPromedioMat1a.Text = (float.Parse(txtCalifMatB1.Text) + float.Parse(txtCalifMatB2.Text) + float.Parse(txtCalifMatB3.Text) / 3).ToString();
            calificaciones.CalFinal = float.Parse(lblPromedioMat1a.Text);
            calificaciones.modificarCalificaciones();
        }

        private void btnPromediarQuim1a_Click(object sender, EventArgs e)
        {
            lblPromedioQuimica1a.Text = string.Format("{0:#.00}", ((double.Parse(txtCalifQuimicaB1.Text) + double.Parse(txtCalifQuimicaB2.Text) + double.Parse(txtCalifQuimicaB3.Text)) / 3)).ToString();
        }

        private void btnPromediarFis1a_Click(object sender, EventArgs e)
        {
            lblPromedioFisica1a.Text = string.Format("{0:#.00}", ((double.Parse(txtCalifFisicaB1.Text) + double.Parse(txtCalifFisicaB2.Text) + double.Parse(txtCalifFisicaB3.Text)) / 3)).ToString();
        }

        private void btnPromediarCE1a_Click(object sender, EventArgs e)
        {
            lblPromedioCE1a.Text = string.Format("{0:#.00}", ((double.Parse(txtCalifCEB1.Text) + double.Parse(txtCalifCEB2.Text) + double.Parse(txtCalifCEB3.Text)) / 3)).ToString();
        }

        private void btnPromediarHist1a_Click(object sender, EventArgs e)
        {
            lblPromedioHistoria1a.Text = string.Format("{0:#.00}", ((double.Parse(txtCalifHistoriaB1.Text) + double.Parse(txtCalifHistoriaB2.Text) + double.Parse(txtCalifHistoriaB3.Text)) / 3)).ToString();
            btnPromFinal1a.Enabled = true;
        }

        public void calcularCalifPrimerAño()
        {
            try
            {
                lblPromFinal1a.Text = string.Format("{0:#.00}", ((double.Parse(lblPromedioEspaañol1a.Text) + double.Parse(lblPromedioMat1a.Text) + double.Parse(lblPromedioQuimica1a.Text) + double.Parse(lblPromedioFisica1a.Text) + double.Parse(lblPromedioCE1a.Text) + double.Parse(lblPromedioHistoria1a.Text)) / 6)).ToString();
                btnGuardarCalif1a.Enabled = true;
            }
            catch (FormatException fe)
            {
                MessageBox.Show("Campos Vasios verifique de nuevo" + fe);
            }
        }

        private void btnPromFinal1a_Click(object sender, EventArgs e)
        {
            calcularCalifPrimerAño();
        }

        private void FrmCalifPrimerAño_Load(object sender, EventArgs e)
        {
            bloquearMaterias();
            
           
        }
        public void bloquearMaterias()
        {
            txtNomMaestro.Enabled = false;
            txtApMatMaestro.Enabled=false;
            txtApPatMaestro.Enabled = false;

            txtMatricula.Enabled = false;
            txtNombreAlum.Enabled = false;
            txtApPatAlum.Enabled = false;
            txtApMatAlum.Enabled = false;
            btnGuardarMatematicas.Enabled = false;
            btnGuardarEspa.Enabled = false;
            btnGuardarFisica.Enabled = false;
            btnGuardarHistoria.Enabled = false;
            btnGuardarQuimica.Enabled = false;
     
            txtCalifEspañolB1.Enabled = false;
            txtCalifEspañolB2.Enabled = false;
            txtCalifEspañolB3.Enabled = false;
            btnPromediarEsp1a.Enabled = false;
            txtCalifCEB1.Enabled = false;
            txtCalifCEB2.Enabled = false;
            txtCalifCEB3.Enabled = false;
            btnPromediarCE1a.Enabled = false;
            txtCalifFisicaB1.Enabled = false;
            txtCalifFisicaB2.Enabled = false;
            txtCalifFisicaB3.Enabled = false;
            btnPromediarFis1a.Enabled = false;
            txtCalifHistoriaB1.Enabled = false;
            txtCalifHistoriaB2.Enabled = false;
            txtCalifHistoriaB3.Enabled = false;
            btnPromediarHist1a.Enabled = false;
            txtCalifMatB1.Enabled = false;
            txtCalifMatB2.Enabled = false;
            txtCalifMatB3.Enabled = false;
            btnPromediarMat1a.Enabled = false;
            txtCalifQuimicaB1.Enabled = false;
            txtCalifQuimicaB2.Enabled = false;
            txtCalifQuimicaB3.Enabled = false;
            btnPromediarQuim1a.Enabled = false;
            //txtCatMaestroAño1.Enabled = false;
            btnPromFinal1a.Enabled = false;
            btnGuardarCalif1a.Enabled = false;
        }

        private void btnConsulAlumCalif_Click(object sender, EventArgs e)
        {
            if (cbSelecGrupo.Text == "1-A")
            {
                cargarGridA();
            }
            else if (cbSelecGrupo.Text == "1-B")
            {
                cargarGridB();
            }
        }
        public void cargarGridA()
        {
            DataTable tabla = new DataTable();
            tabla.Columns.Add("ColMatricula");
            tabla.Columns.Add("colGrupo");
            tabla.Columns.Add("colNomAlum");
            tabla.Columns.Add("colApePat");
            tabla.Columns.Add("colApeMat");

            enlistarAlumnos = grupos.datosCombinados1a();
            for (int i = 0; i < enlistarAlumnos.Count; i++)
            {
                DataRow row = tabla.NewRow();
                row["ColMatricula"] = enlistarAlumnos[i].MatriculaGrid;
                row["colGrupo"] = enlistarAlumnos[i].NomGrupo;
                row["colNomAlum"] = enlistarAlumnos[i].NombreAlumno;
                row["colApePat"] = enlistarAlumnos[i].ApelPater;
                row["colApeMat"] = enlistarAlumnos[i].ApelMater;

                tabla.Rows.Add(row);
            }
            dgPrimerAño.DataSource = tabla;
        }
        public void cargarGridB()
        {
            DataTable tabla = new DataTable();
            tabla.Columns.Add("ColMatricula");
            tabla.Columns.Add("colGrupo");
            tabla.Columns.Add("colNomAlum");
            tabla.Columns.Add("colApePat");
            tabla.Columns.Add("colApeMat");

            enlistarAlumnos = grupos.datosCombinados1b();
            for (int i = 0; i < enlistarAlumnos.Count; i++)
            {
                DataRow row = tabla.NewRow();
                row["ColMatricula"] = enlistarAlumnos[i].MatriculaGrid;
                row["colGrupo"] = enlistarAlumnos[i].NomGrupo;
                row["colNomAlum"] = enlistarAlumnos[i].NombreAlumno;
                row["colApePat"] = enlistarAlumnos[i].ApelPater;
                row["colApeMat"] = enlistarAlumnos[i].ApelMater;

                tabla.Rows.Add(row);
            }
            dgPrimerAño.DataSource = tabla;
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void dgPrimerAño_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgPrimerAño_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            txtMatricula.Text = dgPrimerAño[0, e.RowIndex].Value.ToString();
            txtNombreAlum.Text = dgPrimerAño[2, e.RowIndex].Value.ToString();
            txtApPatAlum.Text = dgPrimerAño[3, e.RowIndex].Value.ToString();
            txtApMatAlum.Text = dgPrimerAño[4, e.RowIndex].Value.ToString();
           
        }

        private void dgPrimerAño_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgPrimerAño_CellClick(sender, e);
        }

        private void btnBuscarMaestro_Click(object sender, EventArgs e)
        {
            bloquearMaterias();
            string opcionParaDesbloquear = "";
            listaDeMaestros = maestros.enlistarMaestro();
            for (int i = 0; i < listaDeMaestros.Count; i++)
            {
                txtApMatMaestro.Text = listaDeMaestros[i].ApellidoMaterno;
                txtApPatMaestro.Text = listaDeMaestros[i].ApellidoPaterno;
                txtNomMaestro.Text = listaDeMaestros[i].Nombre;
                opcionParaDesbloquear = listaDeMaestros[i].Materia;
                materias = listaDeMaestros[i].Materia;
                if (txtNumEmpleado.Text == listaDeMaestros[i].NumEmpleado.ToString())
                {
                    break;
                }
             }
           
             if (opcionParaDesbloquear == "Español")
             {
                 txtCalifEspañolB1.Enabled = true;
                 txtCalifEspañolB2.Enabled = true;
                 txtCalifEspañolB3.Enabled = true;
                 btnPromediarEsp1a.Enabled = true;
                 btnGuardarEspa.Enabled = true;
            }
             else if (opcionParaDesbloquear == "Matematicas")
             {
                  txtCalifMatB1.Enabled = true;
                  txtCalifMatB2.Enabled = true;
                  txtCalifMatB3.Enabled = true;
                  btnGuardarMatematicas.Enabled = true;
                  btnPromediarMat1a.Enabled = true;

             }
            else if (opcionParaDesbloquear == "Quimica")
            {
                txtCalifQuimicaB1.Enabled = true;
                txtCalifQuimicaB2.Enabled = true;
                txtCalifQuimicaB3.Enabled = true;
            }
            else if (opcionParaDesbloquear == "Fisica")
            {
                txtCalifFisicaB1.Enabled = true;
                txtCalifFisicaB2.Enabled = true;
                txtCalifFisicaB3.Enabled = true;
            }
            else if (opcionParaDesbloquear == "FormacionCE")
            {
                txtCalifCEB1.Enabled = true;
                txtCalifCEB2.Enabled = true;
                txtCalifCEB3.Enabled = true;
            }
            else if (opcionParaDesbloquear == "Historia")
            {
                txtCalifHistoriaB1.Enabled = true;
                txtCalifHistoriaB2.Enabled = true;
                txtCalifHistoriaB3.Enabled = true;
            }
            
        }

        private void btnEvaluaEspa_Click(object sender, EventArgs e)
        {
            igualarEspañol();
            calificaciones.ingresarCalificaciones();
        }
        public void igualarEspañol()
        { 
            calificaciones.IdMaestro = int.Parse(txtNumEmpleado.Text);
            calificaciones.IdAlumno = int.Parse(txtMatricula.Text);
            calificaciones.NomMAteria = materias;
            calificaciones.Bim1 = double.Parse(txtCalifEspañolB1.Text);
            calificaciones.Bim2 = double.Parse(txtCalifEspañolB2.Text);
            calificaciones.Bim3 = double.Parse(txtCalifEspañolB3.Text);
        }

        private void txtCalifEspañolB3_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnGuardarMatematicas_Click(object sender, EventArgs e)
        {
            igualarMatematicas();
            calificaciones.ingresarCalificaciones();
        }
        public void igualarMatematicas()
        {
            calificaciones.IdMaestro = int.Parse(txtNumEmpleado.Text);
            calificaciones.IdAlumno = int.Parse(txtMatricula.Text);
            calificaciones.NomMAteria = materias;
            calificaciones.Bim1 = double.Parse(txtCalifMatB1.Text);
            calificaciones.Bim2 = double.Parse(txtCalifMatB2.Text);
            calificaciones.Bim3 = double.Parse(txtCalifMatB3.Text);
        }

        private void btnGuardarQuimica_Click(object sender, EventArgs e)
        {
            igualarQuimica();
            calificaciones.ingresarCalificaciones();
        }
        public void igualarQuimica()
        {
            calificaciones.IdMaestro = int.Parse(txtNumEmpleado.Text);
            calificaciones.IdAlumno = int.Parse(txtMatricula.Text);
            calificaciones.NomMAteria = materias;
            calificaciones.Bim1 = double.Parse(txtCalifQuimicaB1.Text);
            calificaciones.Bim2 = double.Parse(txtCalifQuimicaB2.Text);
            calificaciones.Bim3 = double.Parse(txtCalifQuimicaB3.Text);
        }

        private void btnGuardarFisica_Click(object sender, EventArgs e)
        {
            igualarFisica();
            calificaciones.ingresarCalificaciones();
        }
        public void igualarFisica()
        {
            calificaciones.IdMaestro = int.Parse(txtNumEmpleado.Text);
            calificaciones.IdAlumno = int.Parse(txtMatricula.Text);
            calificaciones.NomMAteria = materias;
            calificaciones.Bim1 = double.Parse(txtCalifFisicaB1.Text);
            calificaciones.Bim2 = double.Parse(txtCalifFisicaB2.Text);
            calificaciones.Bim3 = double.Parse(txtCalifFisicaB3.Text);
        }

        private void btnGuardarCE_Click(object sender, EventArgs e)
        {
            igualarCE();
            calificaciones.ingresarCalificaciones();
        }
        public void igualarCE()
        {
            calificaciones.IdMaestro = int.Parse(txtNumEmpleado.Text);
            calificaciones.IdAlumno = int.Parse(txtMatricula.Text);
            calificaciones.NomMAteria = materias;
            calificaciones.Bim1 = double.Parse(txtCalifCEB1.Text);
            calificaciones.Bim2 = double.Parse(txtCalifCEB2.Text);
            calificaciones.Bim3 = double.Parse(txtCalifCEB3.Text);
        }

        private void btnGuardarHistoria_Click(object sender, EventArgs e)
        {
            igualarHistoria();
            calificaciones.ingresarCalificaciones();
        }
        public void igualarHistoria()
        {
            calificaciones.IdMaestro = int.Parse(txtNumEmpleado.Text);
            calificaciones.IdAlumno = int.Parse(txtMatricula.Text);
            calificaciones.NomMAteria = materias;
            calificaciones.Bim1 = double.Parse(txtCalifHistoriaB1.Text);
            calificaciones.Bim2 = double.Parse(txtCalifHistoriaB2.Text);
            calificaciones.Bim3 = double.Parse(txtCalifHistoriaB3.Text);
        }
    }
}
