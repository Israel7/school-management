﻿namespace School_Management
{
    partial class FrmMaestro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtContrasenaMaestro = new System.Windows.Forms.TextBox();
            this.txtUsuarioMaestro = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtRfcMaestro = new System.Windows.Forms.TextBox();
            this.txtCedulaMaestro = new System.Windows.Forms.TextBox();
            this.cbSelecMateria = new System.Windows.Forms.ComboBox();
            this.txtEmailMaestro = new System.Windows.Forms.TextBox();
            this.txtTelMaestro = new System.Windows.Forms.TextBox();
            this.txtDireccionMaestro = new System.Windows.Forms.TextBox();
            this.dtFechaNacMaestro = new System.Windows.Forms.DateTimePicker();
            this.txtNomMaestro = new System.Windows.Forms.TextBox();
            this.txtApMatMaestro = new System.Windows.Forms.TextBox();
            this.txtApPatMaestro = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pbFotoMaestro = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgMaestro = new System.Windows.Forms.DataGridView();
            this.lblContMaestro = new System.Windows.Forms.Label();
            this.openFileDialog3 = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnConsultarMaestro = new System.Windows.Forms.Button();
            this.btnModificarMaestro = new System.Windows.Forms.Button();
            this.btnEliminarMaestro = new System.Windows.Forms.Button();
            this.btnAgregarMaestro = new System.Windows.Forms.Button();
            this.ColNumEmpleado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColApPat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColApMat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFechaNac = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColMateria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFoto = new System.Windows.Forms.DataGridViewImageColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoMaestro)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMaestro)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtContrasenaMaestro);
            this.groupBox1.Controls.Add(this.txtUsuarioMaestro);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtRfcMaestro);
            this.groupBox1.Controls.Add(this.txtCedulaMaestro);
            this.groupBox1.Controls.Add(this.cbSelecMateria);
            this.groupBox1.Controls.Add(this.txtEmailMaestro);
            this.groupBox1.Controls.Add(this.txtTelMaestro);
            this.groupBox1.Controls.Add(this.txtDireccionMaestro);
            this.groupBox1.Controls.Add(this.dtFechaNacMaestro);
            this.groupBox1.Controls.Add(this.txtNomMaestro);
            this.groupBox1.Controls.Add(this.txtApMatMaestro);
            this.groupBox1.Controls.Add(this.txtApPatMaestro);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.pbFotoMaestro);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(865, 246);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del Maestro";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(391, 131);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(178, 15);
            this.label15.TabIndex = 45;
            this.label15.Text = "Asignar Cuenta de Usuario";
            // 
            // txtContrasenaMaestro
            // 
            this.txtContrasenaMaestro.Location = new System.Drawing.Point(437, 188);
            this.txtContrasenaMaestro.Name = "txtContrasenaMaestro";
            this.txtContrasenaMaestro.Size = new System.Drawing.Size(113, 20);
            this.txtContrasenaMaestro.TabIndex = 43;
            // 
            // txtUsuarioMaestro
            // 
            this.txtUsuarioMaestro.Location = new System.Drawing.Point(437, 162);
            this.txtUsuarioMaestro.Name = "txtUsuarioMaestro";
            this.txtUsuarioMaestro.Size = new System.Drawing.Size(113, 20);
            this.txtUsuarioMaestro.TabIndex = 42;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(367, 191);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "Contraseña:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(391, 165);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "Usurio:";
            // 
            // txtRfcMaestro
            // 
            this.txtRfcMaestro.Location = new System.Drawing.Point(437, 92);
            this.txtRfcMaestro.Name = "txtRfcMaestro";
            this.txtRfcMaestro.Size = new System.Drawing.Size(100, 20);
            this.txtRfcMaestro.TabIndex = 22;
            // 
            // txtCedulaMaestro
            // 
            this.txtCedulaMaestro.Location = new System.Drawing.Point(437, 66);
            this.txtCedulaMaestro.Name = "txtCedulaMaestro";
            this.txtCedulaMaestro.Size = new System.Drawing.Size(100, 20);
            this.txtCedulaMaestro.TabIndex = 21;
            // 
            // cbSelecMateria
            // 
            this.cbSelecMateria.FormattingEnabled = true;
            this.cbSelecMateria.Items.AddRange(new object[] {
            "Español",
            "Español2",
            "Español3",
            "Matematicas",
            "Calculo1",
            "Calculo2",
            "Quimica",
            "Quimica2",
            "Quimica3",
            "Fisica",
            "Fisica2",
            "Fisica3",
            "FormacionCE",
            "DesarrolloPersonal1",
            "DesarrolloPersonal2",
            "Historia",
            "Historia2",
            "Historia3"});
            this.cbSelecMateria.Location = new System.Drawing.Point(437, 40);
            this.cbSelecMateria.Name = "cbSelecMateria";
            this.cbSelecMateria.Size = new System.Drawing.Size(159, 21);
            this.cbSelecMateria.TabIndex = 20;
            // 
            // txtEmailMaestro
            // 
            this.txtEmailMaestro.Location = new System.Drawing.Point(143, 195);
            this.txtEmailMaestro.Name = "txtEmailMaestro";
            this.txtEmailMaestro.Size = new System.Drawing.Size(100, 20);
            this.txtEmailMaestro.TabIndex = 19;
            // 
            // txtTelMaestro
            // 
            this.txtTelMaestro.Location = new System.Drawing.Point(143, 166);
            this.txtTelMaestro.Name = "txtTelMaestro";
            this.txtTelMaestro.Size = new System.Drawing.Size(119, 20);
            this.txtTelMaestro.TabIndex = 18;
            // 
            // txtDireccionMaestro
            // 
            this.txtDireccionMaestro.Location = new System.Drawing.Point(143, 140);
            this.txtDireccionMaestro.Name = "txtDireccionMaestro";
            this.txtDireccionMaestro.Size = new System.Drawing.Size(200, 20);
            this.txtDireccionMaestro.TabIndex = 17;
            // 
            // dtFechaNacMaestro
            // 
            this.dtFechaNacMaestro.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFechaNacMaestro.Location = new System.Drawing.Point(143, 114);
            this.dtFechaNacMaestro.Name = "dtFechaNacMaestro";
            this.dtFechaNacMaestro.Size = new System.Drawing.Size(147, 20);
            this.dtFechaNacMaestro.TabIndex = 16;
            // 
            // txtNomMaestro
            // 
            this.txtNomMaestro.Location = new System.Drawing.Point(143, 91);
            this.txtNomMaestro.Name = "txtNomMaestro";
            this.txtNomMaestro.Size = new System.Drawing.Size(119, 20);
            this.txtNomMaestro.TabIndex = 15;
            // 
            // txtApMatMaestro
            // 
            this.txtApMatMaestro.Location = new System.Drawing.Point(143, 65);
            this.txtApMatMaestro.Name = "txtApMatMaestro";
            this.txtApMatMaestro.Size = new System.Drawing.Size(119, 20);
            this.txtApMatMaestro.TabIndex = 14;
            // 
            // txtApPatMaestro
            // 
            this.txtApPatMaestro.Location = new System.Drawing.Point(143, 40);
            this.txtApPatMaestro.Name = "txtApPatMaestro";
            this.txtApPatMaestro.Size = new System.Drawing.Size(119, 20);
            this.txtApPatMaestro.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(584, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 12;
            // 
            // pbFotoMaestro
            // 
            //this.pbFotoMaestro.Image = global::EscolarSoft.Properties.Resources.maestra1;
            this.pbFotoMaestro.Location = new System.Drawing.Point(660, 69);
            this.pbFotoMaestro.Name = "pbFotoMaestro";
            this.pbFotoMaestro.Size = new System.Drawing.Size(142, 145);
            this.pbFotoMaestro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFotoMaestro.TabIndex = 11;
            this.pbFotoMaestro.TabStop = false;
            this.pbFotoMaestro.Click += new System.EventHandler(this.pbFotoMaestro_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(618, 45);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(211, 15);
            this.label11.TabIndex = 10;
            this.label11.Text = "Seleccionar Imagen de Maestro";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(400, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "RFC:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(332, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Cedula Profecional:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(386, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Materia:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(102, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Email:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Teléfono:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(82, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Dirección:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Fecha Nacimiento:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(90, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido Materno:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Apellido Paterno:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgMaestro);
            this.groupBox2.Location = new System.Drawing.Point(12, 366);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(865, 299);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Maestros dados de Alta";
            // 
            // dgMaestro
            // 
            this.dgMaestro.AllowUserToAddRows = false;
            this.dgMaestro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMaestro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColNumEmpleado,
            this.ColApPat,
            this.ColApMat,
            this.ColNom,
            this.ColFechaNac,
            this.ColMateria,
            this.ColEmail,
            this.ColFoto});
            this.dgMaestro.Location = new System.Drawing.Point(6, 19);
            this.dgMaestro.Name = "dgMaestro";
            this.dgMaestro.Size = new System.Drawing.Size(853, 274);
            this.dgMaestro.TabIndex = 0;
            // 
            // lblContMaestro
            // 
            this.lblContMaestro.AutoSize = true;
            this.lblContMaestro.Location = new System.Drawing.Point(760, 330);
            this.lblContMaestro.Name = "lblContMaestro";
            this.lblContMaestro.Size = new System.Drawing.Size(41, 13);
            this.lblContMaestro.TabIndex = 6;
            this.lblContMaestro.Text = "label17";
            this.lblContMaestro.Visible = false;
            // 
            // openFileDialog3
            // 
            this.openFileDialog3.FileName = "openFileDialog1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 668);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(889, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(95, 17);
            this.toolStripStatusLabel1.Text = "Alta de Maestros";
            // 
            // btnConsultarMaestro
            // 
            this.btnConsultarMaestro.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnConsultarMaestro.BackgroundImage = global::EscolarSoft.Properties.Resources.consultar21;
            this.btnConsultarMaestro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConsultarMaestro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultarMaestro.ForeColor = System.Drawing.Color.White;
            this.btnConsultarMaestro.Location = new System.Drawing.Point(572, 284);
            this.btnConsultarMaestro.Name = "btnConsultarMaestro";
            this.btnConsultarMaestro.Size = new System.Drawing.Size(75, 68);
            this.btnConsultarMaestro.TabIndex = 5;
            this.btnConsultarMaestro.Text = "Consultar";
            this.btnConsultarMaestro.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnConsultarMaestro.UseVisualStyleBackColor = false;
            // 
            // btnModificarMaestro
            // 
            this.btnModificarMaestro.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnModificarMaestro.BackgroundImage = global::EscolarSoft.Properties.Resources.modificarAlumno1;
            this.btnModificarMaestro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnModificarMaestro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarMaestro.ForeColor = System.Drawing.Color.White;
            this.btnModificarMaestro.Location = new System.Drawing.Point(357, 284);
            this.btnModificarMaestro.Name = "btnModificarMaestro";
            this.btnModificarMaestro.Size = new System.Drawing.Size(75, 68);
            this.btnModificarMaestro.TabIndex = 4;
            this.btnModificarMaestro.Text = "Modificar";
            this.btnModificarMaestro.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnModificarMaestro.UseVisualStyleBackColor = false;
            // 
            // btnEliminarMaestro
            // 
            this.btnEliminarMaestro.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnEliminarMaestro.BackgroundImage = global::EscolarSoft.Properties.Resources.eliminarEstudiante1;
            this.btnEliminarMaestro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminarMaestro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarMaestro.ForeColor = System.Drawing.Color.White;
            this.btnEliminarMaestro.Location = new System.Drawing.Point(266, 284);
            this.btnEliminarMaestro.Name = "btnEliminarMaestro";
            this.btnEliminarMaestro.Size = new System.Drawing.Size(75, 68);
            this.btnEliminarMaestro.TabIndex = 3;
            this.btnEliminarMaestro.Text = "Eliminar";
            this.btnEliminarMaestro.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnEliminarMaestro.UseVisualStyleBackColor = false;
            // 
            // btnAgregarMaestro
            // 
            this.btnAgregarMaestro.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            //this.btnAgregarMaestro.BackgroundImage = global::EscolarSoft.Properties.Resources.agregrAdmin1;
            this.btnAgregarMaestro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAgregarMaestro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarMaestro.ForeColor = System.Drawing.Color.White;
            this.btnAgregarMaestro.Location = new System.Drawing.Point(175, 284);
            this.btnAgregarMaestro.Name = "btnAgregarMaestro";
            this.btnAgregarMaestro.Size = new System.Drawing.Size(75, 68);
            this.btnAgregarMaestro.TabIndex = 2;
            this.btnAgregarMaestro.Text = "Agregar";
            this.btnAgregarMaestro.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAgregarMaestro.UseVisualStyleBackColor = false;
            this.btnAgregarMaestro.Click += new System.EventHandler(this.btnAgregarMaestro_Click);
            // 
            // ColNumEmpleado
            // 
            this.ColNumEmpleado.DataPropertyName = "ColNumEmpleado";
            this.ColNumEmpleado.HeaderText = "NumEmpleado";
            this.ColNumEmpleado.Name = "ColNumEmpleado";
            // 
            // ColApPat
            // 
            this.ColApPat.DataPropertyName = "ColApPat";
            this.ColApPat.HeaderText = "Apellido Paterno";
            this.ColApPat.Name = "ColApPat";
            // 
            // ColApMat
            // 
            this.ColApMat.DataPropertyName = "ColApMat";
            this.ColApMat.HeaderText = "Apellido Materno";
            this.ColApMat.Name = "ColApMat";
            // 
            // ColNom
            // 
            this.ColNom.DataPropertyName = "ColNom";
            this.ColNom.HeaderText = "Nombre";
            this.ColNom.Name = "ColNom";
            // 
            // ColFechaNac
            // 
            this.ColFechaNac.DataPropertyName = "ColFechaNac";
            this.ColFechaNac.HeaderText = "Fecha Nacimiento";
            this.ColFechaNac.Name = "ColFechaNac";
            // 
            // ColMateria
            // 
            this.ColMateria.DataPropertyName = "ColMateria";
            this.ColMateria.HeaderText = "Materia";
            this.ColMateria.Name = "ColMateria";
            // 
            // ColEmail
            // 
            this.ColEmail.DataPropertyName = "ColEmail";
            this.ColEmail.HeaderText = "Email";
            this.ColEmail.Name = "ColEmail";
            // 
            // ColFoto
            // 
            this.ColFoto.DataPropertyName = "ColFoto";
            this.ColFoto.HeaderText = "Foto";
            this.ColFoto.Name = "ColFoto";
            this.ColFoto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColFoto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FrmMaestro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(889, 690);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblContMaestro);
            this.Controls.Add(this.btnConsultarMaestro);
            this.Controls.Add(this.btnModificarMaestro);
            this.Controls.Add(this.btnEliminarMaestro);
            this.Controls.Add(this.btnAgregarMaestro);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmMaestro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administracion de Maestros";
            this.Load += new System.EventHandler(this.FrmMaestro_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoMaestro)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMaestro)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtRfcMaestro;
        private System.Windows.Forms.TextBox txtCedulaMaestro;
        private System.Windows.Forms.ComboBox cbSelecMateria;
        private System.Windows.Forms.TextBox txtEmailMaestro;
        private System.Windows.Forms.TextBox txtTelMaestro;
        private System.Windows.Forms.TextBox txtDireccionMaestro;
        private System.Windows.Forms.DateTimePicker dtFechaNacMaestro;
        private System.Windows.Forms.TextBox txtNomMaestro;
        private System.Windows.Forms.TextBox txtApMatMaestro;
        private System.Windows.Forms.TextBox txtApPatMaestro;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pbFotoMaestro;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgMaestro;
        private System.Windows.Forms.Button btnAgregarMaestro;
        private System.Windows.Forms.Button btnEliminarMaestro;
        private System.Windows.Forms.Button btnModificarMaestro;
        private System.Windows.Forms.Button btnConsultarMaestro;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtContrasenaMaestro;
        private System.Windows.Forms.TextBox txtUsuarioMaestro;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblContMaestro;
        private System.Windows.Forms.OpenFileDialog openFileDialog3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNumEmpleado;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColApPat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColApMat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFechaNac;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColMateria;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmail;
        private System.Windows.Forms.DataGridViewImageColumn ColFoto;
    }
}