﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using AccesoDB;

namespace LibSM
{
    public class Calificar
    {
        AccesoDBMySql acceso = new AccesoDBMySql("localhost", "ArvayoAdmin", "escolarsoft", "escolarsoft");
        List<Calificar> calificaciones = new List<Calificar>();
        private int idCalificacion;
        private int idMaestro;
        private int idAlumno;
        private string nomMAteria;
        private double bim1;
        private double bim2;
        private double bim3;
        private double calFinal = 0;

        public List<Calificar> enlistarCalificaciones()
        {
            MySqlDataReader dataread = acceso.mostrar("calificaciones");
            calificaciones.Clear();
            if (dataread.HasRows)
            {
                while (dataread.Read())
                {
                    Calificar alumnos = new Calificar();
                    alumnos.IdCalificacion = int.Parse(dataread[0].ToString());
                    alumnos.IdMaestro = int.Parse(dataread[1].ToString());
                    alumnos.IdAlumno = int.Parse(dataread[2].ToString());
                    alumnos.NomMAteria = dataread[3].ToString();
                    alumnos.Bim1 = double.Parse(dataread[4].ToString());
                    alumnos.Bim2 = double.Parse(dataread[5].ToString());
                    alumnos.Bim3 = double.Parse(dataread[6].ToString());
                    alumnos.CalFinal = double.Parse(dataread[7].ToString());
                    calificaciones.Add(alumnos);
                }
                dataread.Close();
            }
            else
            {
                return null;
            }
            acceso.desconectar();
            return calificaciones;
        }
        public bool ingresarCalificaciones()
        {
            acceso.conectar();
            bool agregar = acceso.insertar("calificaciones", "idCalificacion,idMaestro,idAlumno,nomMateria,bimestre1,bimestre2,bimestre3,califFinal", "NULL," + idMaestro + "," + idAlumno + ",'" + nomMAteria + "'," + bim1 + "," + bim2 + "," + bim3 + ","+calFinal);
            acceso.desconectar();
            return agregar;
        }
        public bool modificarCalificaciones()
        {
            acceso.conectar();
            bool modificar = acceso.modificar("calificaciones", "califFinal=" + calFinal, "idAlumno=" + idAlumno);
            acceso.desconectar();
            return modificar;
        }

        public int IdCalificacion
        {
            get
            {
                return idCalificacion;
            }

            set
            {
                idCalificacion = value;
            }
        }

        public int IdMaestro
        {
            get
            {
                return idMaestro;
            }

            set
            {
                idMaestro = value;
            }
        }

        public int IdAlumno
        {
            get
            {
                return idAlumno;
            }

            set
            {
                idAlumno = value;
            }
        }

        public string NomMAteria
        {
            get
            {
                return nomMAteria;
            }

            set
            {
                nomMAteria = value;
            }
        }

        public double Bim1
        {
            get
            {
                return bim1;
            }

            set
            {
                bim1 = value;
            }
        }

        public double Bim2
        {
            get
            {
                return bim2;
            }

            set
            {
                bim2 = value;
            }
        }

        public double Bim3
        {
            get
            {
                return bim3;
            }

            set
            {
                bim3 = value;
            }
        }

        public double CalFinal
        {
            get
            {
                return calFinal;
            }

            set
            {
                calFinal = value;
            }
        }
    }
}
