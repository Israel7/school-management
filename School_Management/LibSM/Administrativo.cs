﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using AccesoDB;

namespace LibSM
{
    public class Administrativo
    {
        AccesoDBMySql acceso = new AccesoDBMySql("localhost", "root", "1234", "escolarsoft");

        List<Administrativo> listaAdmin = new List<Administrativo>();

        private int numEmpleado;
        private string apellidoPaterno;
        private string apellidoMaterno;
        private string nombre;
        private DateTime fechaNac;
        private string direccion;
        private string tipoUsuario;
        private string telefono;
        private string email;
        private string cedula;
        private string rfc;
        private string foto;
        private string usuarioAdmin;
        private string contrasenaAdmin;

        public Administrativo()
        {

        }

        public Administrativo(int numEmpleado, string apellidoPaterno, string apellidoMaterno, string nombre, DateTime fechaNac, string direccion, string tipoUsuario, string telefono, string email, string cedula, string rfc, string foto, string usuarioAdmin, string contrasenaAdmin)
        {
            this.numEmpleado = numEmpleado;
            this.apellidoPaterno = apellidoPaterno;
            this.apellidoMaterno = apellidoMaterno;
            this.nombre = nombre;
            this.fechaNac = fechaNac;
            this.direccion = direccion;
            this.tipoUsuario = tipoUsuario;
            this.telefono = telefono;
            this.email = email;
            this.cedula = cedula;
            this.rfc = rfc;
            this.foto = foto;
            this.usuarioAdmin = usuarioAdmin;
            this.contrasenaAdmin = contrasenaAdmin;
        }

        public int NumEmpleado
        {
            get
            {
                return numEmpleado;
            }

            set
            {
                numEmpleado = value;
            }
        }

        public string ApellidoPaterno
        {
            get
            {
                return apellidoPaterno;
            }

            set
            {
                apellidoPaterno = value;
            }
        }

        public string ApellidoMaterno
        {
            get
            {
                return apellidoMaterno;
            }

            set
            {
                apellidoMaterno = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public DateTime FechaNac
        {
            get
            {
                return fechaNac;
            }

            set
            {
                fechaNac = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Rfc
        {
            get
            {
                return rfc;
            }

            set
            {
                rfc = value;
            }
        }

        public string Foto
        {
            get
            {
                return foto;
            }

            set
            {
                foto = value;
            }
        }

        public string TipoUsuario
        {
            get
            {
                return tipoUsuario;
            }

            set
            {
                tipoUsuario = value;
            }
        }

        public string UsuarioAdmin
        {
            get
            {
                return usuarioAdmin;
            }

            set
            {
                usuarioAdmin = value;
            }
        }

        public string ContrasenaAdmin
        {
            get
            {
                return contrasenaAdmin;
            }

            set
            {
                contrasenaAdmin = value;
            }
        }



        public List<Administrativo> enlistarAdmin()
        {
            MySqlDataReader dtr = acceso.mostrar("administrativo");
            listaAdmin.Clear();
            if (dtr.HasRows)
            {
                while (dtr.Read())
                {
                    Administrativo admon = new Administrativo();
                    admon.NumEmpleado = int.Parse(dtr[0].ToString());
                    admon.ApellidoPaterno = dtr[1].ToString();
                    admon.ApellidoMaterno = dtr[2].ToString();
                    admon.Nombre = dtr[3].ToString();
                    //admon.FechaNac = Convert.ToDateTime(dtr[4].ToString());
                    admon.TipoUsuario = dtr[5].ToString();
                    admon.Direccion = dtr[6].ToString();
                    admon.Telefono = dtr[7].ToString();
                    admon.Email = dtr[8].ToString();
                    admon.Cedula = dtr[9].ToString();
                    admon.Rfc = dtr[10].ToString();
                    admon.Foto = dtr[11].ToString();
                    admon.usuarioAdmin = dtr[12].ToString();
                    admon.contrasenaAdmin = dtr[13].ToString();

                    //Agregar a la lista
                    listaAdmin.Add(admon);

                }
                dtr.Close();

            }
            else
            {
                return null;
            }
            return listaAdmin;
        }

        public bool agregarAdministrativo()
        {
            acceso.conectar();
            bool agregar = acceso.insertar("administrativo", "numEmpleado, apellidoPaterno, apellidoMaterno, nombre, fechaNacimiento, tipoUsuario, direccion, telefono, email, cedula, rfc, foto, usuario, contrasena", "NULL,'" + apellidoPaterno + "','" + apellidoMaterno + "','" + nombre + "','" + fechaNac.ToString("yyyy-MM-dd") + "','" + tipoUsuario + "','" + direccion + "','" + telefono + "','" + email + "','" + cedula + "','" + rfc + "','" + Foto.Replace(@"\", @"\\") + "','" + UsuarioAdmin + "','" + ContrasenaAdmin + "'");

            acceso.desconectar();
            return agregar;
        }

        public bool eliminarAdministrativo()
        {
            acceso.conectar();
            bool eliminar = acceso.Eliminar("administrativo", "numEmpleado", numEmpleado + "");
            acceso.desconectar();
            return eliminar;
        }

        public bool modificarAdministrativo()
        {
            acceso.conectar();
            bool modificar = acceso.modificar("administrativo", "apellidoPaterno= \"" + apellidoPaterno + "\",apellidoMaterno= \"" + apellidoMaterno + "\",nombre= \"" + nombre + "\",fechaNacimiento= \"" + fechaNac.ToString("yyyy-MM-dd") + "\",direccion= \"" + direccion + "\",tipoUsuario= \"" + tipoUsuario +
                                               "\",telefono= \"" + telefono + "\",email= \"" + email + "\",cedula= \"" + cedula + "\",rfc= \"" + rfc + "\",foto= \"" + foto.Replace(@"\", @"\\") + "\"", "numEmpleado= " + numEmpleado);

           //bool modificar = acceso.modificar("alumno", "apellidoPaterno= \"" + apellidoPaterno + "\",apellidoMaterno= \"" + apellidoMaterno + "\",nombre= \"" + nombre + "\",fechaNacimiento= \"" + fechaNac.ToString("yyyy-MM-dd") + "\",direccion= \"" + dirección + "\",telefono= \"" + telefono +
           //                                   "\",curp= \"" + curp + "\",materias= \"" + materias + "\",grupo= \"" + grupo + "\",turno= \"" + turno + "\",foto= \"" + foto.Replace(@"\", @"\\") + "\"", "matricula= " + matricula);
            acceso.desconectar();
            return modificar;
        }
    }
}
