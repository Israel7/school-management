﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSM
{
    class Reportes
    {
        private int Id_reporte;
        private double calificaciones;
        private DateTime fechaEvaluacion;
        private string nomAlumno;
        private string nomMateria;

        public int Id_reporte1
        {
            get
            {
                return Id_reporte;
            }

            set
            {
                Id_reporte = value;
            }
        }

        public double Calificaciones
        {
            get
            {
                return calificaciones;
            }

            set
            {
                calificaciones = value;
            }
        }

        public DateTime FechaEvaluacion
        {
            get
            {
                return fechaEvaluacion;
            }

            set
            {
                fechaEvaluacion = value;
            }
        }

        public string NomAlumno
        {
            get
            {
                return nomAlumno;
            }

            set
            {
                nomAlumno = value;
            }
        }

        public string NomMateria
        {
            get
            {
                return nomMateria;
            }

            set
            {
                nomMateria = value;
            }
        }
    }
}
