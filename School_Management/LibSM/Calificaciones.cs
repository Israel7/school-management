﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDB;

namespace LibSM
{
    public class Calificaciones
    {
        AccesoDBMySql acceso = new AccesoDBMySql("localhost", "ArvayoAdmin", "escolarsoft", "escolarsoft");

        private int numEmpleadoM;
        private string apPaternoM;
        private string apMaternoM;
        private string nombreM;
        private DateTime fecha;
        private double espanol;
        private double matematicas;
        private double quimica;
        private double fisica;
        private double fce;
        private double histotia;
        private int matricula;
        private string apPaternoA;
        private string apMaternoA;
        private string nombreA;
        private string grupo;

        public bool guardarCalificaciones()
        {
            acceso.conectar();
            bool agregar = acceso.insertar("calificaciones1ano", "numEmpleadoM, apPaternoM, apMaternoM, nombreM, fecha, espanol, matematicas, quimica, fisica, fce, historia, matricula, apPaternoA, apMaternoA, nombreA, grupo", "NULL,'" + numEmpleadoM + "','" + apPaternoM + "','" + apMaternoM + "','" + nombreM + "','" + fecha.ToString("yyyy-MM-dd") + "','" + espanol + "','" + matematicas + "','" + quimica + "','" + fisica + "','" + fce + "','" + histotia + "','" + matricula + "','" + apPaternoA + "','" + apMaternoA + "','" + nombreA + "','" + grupo + "','");
            acceso.desconectar();
            return agregar;
        }
            

        public int NumEmpleadoM
        {
            get
            {
                return numEmpleadoM;
            }

            set
            {
                numEmpleadoM = value;
            }
        }

        public string ApPaternoM
        {
            get
            {
                return apPaternoM;
            }

            set
            {
                apPaternoM = value;
            }
        }

        public string ApMaternoM
        {
            get
            {
                return apMaternoM;
            }

            set
            {
                apMaternoM = value;
            }
        }

        public string NombreM
        {
            get
            {
                return nombreM;
            }

            set
            {
                nombreM = value;
            }
        }

        public double Espanol
        {
            get
            {
                return espanol;
            }

            set
            {
                espanol = value;
            }
        }

        public double Matematicas
        {
            get
            {
                return matematicas;
            }

            set
            {
                matematicas = value;
            }
        }

        public double Quimica
        {
            get
            {
                return quimica;
            }

            set
            {
                quimica = value;
            }
        }

        public double Fisica
        {
            get
            {
                return fisica;
            }

            set
            {
                fisica = value;
            }
        }

        public double Fce
        {
            get
            {
                return fce;
            }

            set
            {
                fce = value;
            }
        }

        public double Histotia
        {
            get
            {
                return histotia;
            }

            set
            {
                histotia = value;
            }
        }

        public int Matricula
        {
            get
            {
                return matricula;
            }

            set
            {
                matricula = value;
            }
        }

        public string ApPaternoA
        {
            get
            {
                return apPaternoA;
            }

            set
            {
                apPaternoA = value;
            }
        }

        public string ApMaternoA
        {
            get
            {
                return apMaternoA;
            }

            set
            {
                apMaternoA = value;
            }
        }

        public string NombreA
        {
            get
            {
                return nombreA;
            }

            set
            {
                nombreA = value;
            }
        }

        public string Grupo
        {
            get
            {
                return grupo;
            }

            set
            {
                grupo = value;
            }
        }
    }
}
