﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using AccesoDB;


namespace LibSM
{
    public class Alumno
    {
        AccesoDBMySql acceso = new AccesoDBMySql("localhost", "ArvayoAdmin", "escolarsoft", "escolarsoft");

        List<Alumno> listaAlumnos = new List<Alumno>();
       
        List<Alumno> mostrarSegundosDatos = new List<Alumno>();
        
        private int matricula;
        private string apellidoPaterno;
        private string apellidoMaterno;
        private string nombre;
        private DateTime fechaNac;
        private string dirección;
        private string telefono;
        private string curp;
        private string materias;
        private string grupo;
        private string turno;
        private string foto;
        private string usuario;
        private string contrasena;

        public Alumno()
        {

        }

        public Alumno(int matricula, string apellidoPaterno, string apellidoMaterno, string nombre, DateTime fechaNac, string dirección, string telefono, string curp, string materias, string grupo, string turno, string foto, string usuario, string contrasena)
        {
            this.matricula = matricula;
            this.apellidoPaterno = apellidoPaterno;
            this.apellidoMaterno = apellidoMaterno;
            this.nombre = nombre;
            this.fechaNac = fechaNac;
            this.dirección = dirección;
            this.telefono = telefono;
            this.curp = curp;
            this.materias = materias;
            this.grupo = grupo;
            this.turno = turno;
            this.foto = foto;
            this.usuario = usuario;
            this.contrasena = contrasena;
        }

        public int Matricula
        {
            get
            {
                return matricula;
            }

            set
            {
                matricula = value;
            }
        }

        public string ApellidoPaterno
        {
            get
            {
                return apellidoPaterno;
            }

            set
            {
                apellidoPaterno = value;
            }
        }

        public string ApellidoMaterno
        {
            get
            {
                return apellidoMaterno;
            }

            set
            {
                apellidoMaterno = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public DateTime FechaNac
        {
            get
            {
                return fechaNac;
            }

            set
            {
                fechaNac = value;
            }
        }

        public string Dirección
        {
            get
            {
                return dirección;
            }

            set
            {
                dirección = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Curp
        {
            get
            {
                return curp;
            }

            set
            {
                curp = value;
            }
        }

        public string Materias
        {
            get
            {
                return materias;
            }

            set
            {
                materias = value;
            }
        }

        public string Grupo
        {
            get
            {
                return grupo;
            }

            set
            {
                grupo = value;
            }
        }

        public string Turno
        {
            get
            {
                return turno;
            }

            set
            {
                turno = value;
            }
        }

        public string Foto
        {
            get
            {
                return foto;
            }

            set
            {
                foto = value;
            }
        }

        public string Usuario
        {
            get
            {
                return usuario;
            }

            set
            {
                usuario = value;
            }
        }

        public string Contrasena
        {
            get
            {
                return contrasena;
            }

            set
            {
                contrasena = value;
            }
        }
        public List<Alumno> enlistarAlumno()
        {
            MySqlDataReader dtr = acceso.mostrar("alumno");
            listaAlumnos.Clear();
            try
            {
                if (dtr.HasRows)
                {
                    while (dtr.Read())
                    {
                        Alumno alum = new Alumno();
                        alum.Matricula = int.Parse(dtr[0].ToString());
                        alum.ApellidoPaterno = dtr[1].ToString();
                        alum.ApellidoMaterno = dtr[2].ToString();
                        alum.Nombre = dtr[3].ToString();
                        alum.FechaNac = Convert.ToDateTime(dtr[4].ToString());
                        alum.Dirección = dtr[5].ToString();
                        alum.Telefono = dtr[6].ToString();
                        alum.Curp = dtr[7].ToString();
                        alum.Materias = dtr[8].ToString();
                        alum.Grupo = dtr[9].ToString();
                        alum.Turno = dtr[10].ToString();
                        alum.Foto = dtr[11].ToString();
                        alum.Usuario = dtr[12].ToString();
                        alum.Contrasena = dtr[13].ToString();


                        //Agregar a la lista
                        listaAlumnos.Add(alum);

                    }
                    dtr.Close();

                }
                else
                {
                    return null;
                }
                return listaAlumnos;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public bool agregarAlumno()
        {
            acceso.conectar();
            bool agregar = acceso.insertar("alumno", "matricula, apellidoPaterno, apellidoMaterno, nombre, fechaNacimiento, direccion, telefono, curp, materias, grupo, turno, foto, usuario, contrasena", "NULL,'"+apellidoPaterno +"','" + apellidoMaterno + "','" + nombre + "','" + fechaNac.ToString("yyyy-MM-dd") + "','" + dirección + "','" + telefono + "','" + curp + "','" + materias + "','" + grupo + "','" + turno + "','" + foto.Replace(@"\", @"\\") +"','"+ Usuario + "','" + Contrasena + "'");
            acceso.desconectar();
            return agregar;
        }

        public bool agregarAlumnoGrupo()
        {
            acceso.conectar();
            bool agregarGrupo = acceso.insertar("grupo", "id_grupo, nombre, matricula", "NULL,'" +grupo+ "','" +matricula+ "'");
            acceso.desconectar();
            return agregarGrupo;
        }

        public bool eliminarAlumno()
        {
            acceso.conectar();
            bool eliminar = acceso.Eliminar("alumno", "matricula", matricula+"");
            acceso.desconectar();
            return eliminar;
        }

        public bool eliminarAlumnoGrupo()
        {
            acceso.conectar();
            bool eliminarGrupo = acceso.Eliminar("grupo", "id_grupo", matricula+"");
            acceso.desconectar();
            return eliminarGrupo;
        }

        public bool modificarAlumno()
        {
            acceso.conectar();
            bool modificar = acceso.modificar("alumno", "apellidoPaterno= \""+apellidoPaterno+"\",apellidoMaterno= \""+apellidoMaterno+"\",nombre= \""+nombre+"\",fechaNacimiento= \""+fechaNac.ToString("yyyy-MM-dd")+"\",direccion= \""+dirección+"\",telefono= \""+telefono+
                                              "\",curp= \""+curp+"\",materias= \""+materias+"\",grupo= \""+grupo+"\",turno= \""+turno+"\",foto= \""+foto.Replace(@"\", @"\\") +"\"","matricula= "+matricula);
            acceso.desconectar();
            return modificar;
           
        }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public List<Alumno> ListConsultarApellido()
        {
          MySqlDataReader dtr =  acceso.consultarAlumno("alumno", "apellidoPaterno", apellidoPaterno +"");
            listaAlumnos.Clear();
            if (dtr.HasRows)
            {
                while (dtr.Read())
                {
                    Alumno alum = new Alumno();
                    alum.Matricula = int.Parse(dtr[0].ToString());
                    alum.ApellidoPaterno = dtr[1].ToString();
                    alum.ApellidoMaterno = dtr[2].ToString();
                    alum.Nombre = dtr[3].ToString();
                    alum.FechaNac = Convert.ToDateTime(dtr[4].ToString());
                    alum.Dirección = dtr[5].ToString();
                    alum.Telefono = dtr[6].ToString();
                    alum.Curp = dtr[7].ToString();
                    alum.Materias = dtr[8].ToString();
                    alum.Grupo = dtr[9].ToString();
                    alum.Turno = dtr[10].ToString();
                    alum.Foto = dtr[11].ToString();
                    alum.Usuario = dtr[12].ToString();
                    alum.Contrasena = dtr[13].ToString();

                    listaAlumnos.Add(alum);

                }
                dtr.Close();

            }
            else
            {
                return null;
            }
            return listaAlumnos;


        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
