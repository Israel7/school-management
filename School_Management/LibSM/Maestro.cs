﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDB;
using MySql.Data.MySqlClient;

namespace LibSM
{
    public class Maestro
    {
        AccesoDBMySql acceso = new AccesoDBMySql("localhost", "ArvayoAdmin", "escolarsoft", "escolarsoft");

        List<Maestro> listaMaestros = new List<Maestro>();

        private int numEmpleado;
        private string apellidoPaterno;
        private string apellidoMaterno;
        private string nombre;
        private DateTime fechaNac;
        private string dirección;
        private string telefono;
        private string email;
        private string materia;
        private string cedula;
        private string rfc;
        private string foto;
        private string usuarioMaestro;
        private string contrasenaMaestro;

        public List<Maestro> enlistarMaestro()
        {
            MySqlDataReader dtr = acceso.mostrar("maestro");
            listaMaestros.Clear();
            try
            {
                if (dtr.HasRows)
                {
                    while (dtr.Read())
                    {
                        Maestro maestro = new Maestro();
                        maestro.numEmpleado = int.Parse(dtr[0].ToString());
                        maestro.ApellidoPaterno = dtr[1].ToString();
                        maestro.ApellidoMaterno = dtr[2].ToString();
                        maestro.Nombre = dtr[3].ToString();
                        maestro.FechaNac = Convert.ToDateTime(dtr[4].ToString());
                        maestro.Dirección = dtr[5].ToString();
                        maestro.Telefono = dtr[6].ToString();
                        maestro.email = dtr[7].ToString();
                        maestro.materia = dtr[8].ToString();
                        maestro.cedula = dtr[9].ToString();
                        maestro.rfc = dtr[10].ToString();
                        maestro.Foto = dtr[11].ToString();
                        maestro.UsuarioMaestro = dtr[12].ToString();
                        maestro.ContrasenaMaestro = dtr[13].ToString();


                        //Agregar a la lista
                        listaMaestros.Add(maestro);

                    }
                    dtr.Close();

                }
                else
                {
                    return null;
                }
                return listaMaestros;
            }
            catch (Exception)
            {
                return null;
            }
        }
            

        public bool agregarMaestro()
        {
            acceso.conectar();
            bool agregar = acceso.insertar("maestro", "numEmpleado, apellidoPaterno, apellidoMaterno, nombre, fechaNacimiento, direccion, telefono, email, materia, cedula, rfc, foto, usuario, contrasena", "NULL,'" + apellidoPaterno + "','" + apellidoMaterno + "','" + nombre + "','" + fechaNac.ToString("yyyy-MM-dd") + "','" + dirección + "','" + telefono + "','" + email + "','" + materia + "','" + cedula + "','" + rfc + "','" + Foto.Replace(@"\", @"\\") + "','" + UsuarioMaestro + "','" + ContrasenaMaestro + "'");
            acceso.desconectar();
            return agregar;
        }


        public int NumEmpleado
        {
            get
            {
                return numEmpleado;
            }

            set
            {
                numEmpleado = value;
            }
        }

        public string ApellidoPaterno
        {
            get
            {
                return apellidoPaterno;
            }

            set
            {
                apellidoPaterno = value;
            }
        }

        public string ApellidoMaterno
        {
            get
            {
                return apellidoMaterno;
            }

            set
            {
                apellidoMaterno = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public DateTime FechaNac
        {
            get
            {
                return fechaNac;
            }

            set
            {
                fechaNac = value;
            }
        }

        public string Dirección
        {
            get
            {
                return dirección;
            }

            set
            {
                dirección = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Materia
        {
            get
            {
                return materia;
            }

            set
            {
                materia = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Rfc
        {
            get
            {
                return rfc;
            }

            set
            {
                rfc = value;
            }
        }

        public string UsuarioMaestro
        {
            get
            {
                return usuarioMaestro;
            }

            set
            {
                usuarioMaestro = value;
            }
        }

        public string ContrasenaMaestro
        {
            get
            {
                return contrasenaMaestro;
            }

            set
            {
                contrasenaMaestro = value;
            }
        }

        public string Foto
        {
            get
            {
                return foto;
            }

            set
            {
                foto = value;
            }
        }
    }
}
