﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSM
{
    class Evaluaciones
    {
        private int Id_Evaluacion;
        private DateTime fechaEvaluación;
        private int Id_maestro;
        private double calificaciones;
        private string nomMateria;
        private int Id_Alumno;
        private string nomAlumno;

        public int Id_Evaluacion1
        {
            get
            {
                return Id_Evaluacion;
            }

            set
            {
                Id_Evaluacion = value;
            }
        }

        public DateTime FechaEvaluación
        {
            get
            {
                return fechaEvaluación;
            }

            set
            {
                fechaEvaluación = value;
            }
        }

        public int Id_maestro1
        {
            get
            {
                return Id_maestro;
            }

            set
            {
                Id_maestro = value;
            }
        }

        public double Calificaciones
        {
            get
            {
                return calificaciones;
            }

            set
            {
                calificaciones = value;
            }
        }

        public string NomMateria
        {
            get
            {
                return nomMateria;
            }

            set
            {
                nomMateria = value;
            }
        }

        public int Id_Alumno1
        {
            get
            {
                return Id_Alumno;
            }

            set
            {
                Id_Alumno = value;
            }
        }

        public string NomAlumno
        {
            get
            {
                return nomAlumno;
            }

            set
            {
                nomAlumno = value;
            }
        }
    }
}
