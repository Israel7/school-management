﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using AccesoDB;

namespace LibSM
{
    public class Grupos
    {
        AccesoDBMySql acceso = new AccesoDBMySql("localhost", "ArvayoAdmin", "escolarsoft", "escolarsoft");
        List<Grupos> nombreGrupo = new List<Grupos>();

        private int Id_grupo;
        private string nombre;
        private int matricula;
        //DATOS PARA DATAGRID
        private int matriculaGrid;
        private string nomGrupo;
        private string nombreAlumno;
        private string apelPater;
        private string apelMater;

        public List<Grupos> datosCombinados1a()
        {
            MySqlDataReader dta = acceso.innerJoin("grupo", "alumno", "nombre", "nombre", "apellidoPaterno", "apellidoMaterno", "matricula", "matricula", "1-A");
            nombreGrupo.Clear();

            if (dta.HasRows)
            {
                while (dta.Read())
                {
                    Grupos grup = new Grupos();
                    grup.MatriculaGrid = int.Parse(dta[1].ToString());
                    grup.NomGrupo = dta[0].ToString();
                    grup.NombreAlumno = dta[2].ToString();
                    grup.ApelPater= dta[3].ToString();
                    grup.ApelMater = dta[4].ToString();

                    nombreGrupo.Add(grup);
                }
            }
            else
            {
                return null;
            }
            acceso.desconectar();

            return nombreGrupo;
        }

        public List<Grupos> datosCombinados1b()
        {
            MySqlDataReader dta = acceso.innerJoin("grupo", "alumno", "nombre", "nombre", "apellidoPaterno", "apellidoMaterno", "matricula", "matricula", "1-B");
            nombreGrupo.Clear();

            if (dta.HasRows)
            {
                while (dta.Read())
                {
                    Grupos grup = new Grupos();
                    grup.MatriculaGrid = int.Parse(dta[1].ToString());
                    grup.NomGrupo = dta[0].ToString();
                    grup.NombreAlumno = dta[2].ToString();
                    grup.ApelPater = dta[3].ToString();
                    grup.ApelMater = dta[4].ToString();

                    nombreGrupo.Add(grup);
                }
            }
            else
            {
                return null;
            }
            acceso.desconectar();

            return nombreGrupo;
        }

        public List<Grupos> datosCombinados2a()
        {
            MySqlDataReader dta = acceso.innerJoin("grupo", "alumno", "nombre", "nombre", "apellidoPaterno", "apellidoMaterno", "matricula", "matricula", "2-A");
            nombreGrupo.Clear();

            if (dta.HasRows)
            {
                while (dta.Read())
                {
                    Grupos grup = new Grupos();
                    grup.MatriculaGrid = int.Parse(dta[0].ToString());
                    grup.NomGrupo = dta[1].ToString();
                    grup.NombreAlumno = dta[2].ToString();
                    grup.ApelPater = dta[3].ToString();
                    grup.ApelMater = dta[4].ToString();
                    nombreGrupo.Add(grup);
                }
            }
            else
            {
                return null;
            }
            acceso.desconectar();

            return nombreGrupo;
        }

        public List<Grupos> datosCombinados2b()
        {
            MySqlDataReader dta = acceso.innerJoin("grupo", "alumno", "nombre", "nombre", "apellidoPaterno", "apellidoMaterno", "matricula", "matricula", "2-B");
            nombreGrupo.Clear();

            if (dta.HasRows)
            {
                while (dta.Read())
                {
                    Grupos grup = new Grupos();
                    grup.MatriculaGrid = int.Parse(dta[0].ToString());
                    grup.NomGrupo = dta[1].ToString();
                    grup.NombreAlumno = dta[2].ToString();
                    grup.ApelPater = dta[3].ToString();
                    grup.ApelMater = dta[4].ToString();

                    nombreGrupo.Add(grup);
                }
            }
            else
            {
                return null;
            }
            acceso.desconectar();

            return nombreGrupo;
        }

        public List<Grupos> datosCombinados3a()
        {
            MySqlDataReader dta = acceso.innerJoin("grupo", "alumno", "nombre", "nombre", "apellidoPaterno", "apellidoMaterno", "matricula", "matricula", "3-A");
            nombreGrupo.Clear();

            if (dta.HasRows)
            {
                while (dta.Read())
                {
                    Grupos grup = new Grupos();
                    grup.MatriculaGrid = int.Parse(dta[0].ToString());
                    grup.NomGrupo = dta[1].ToString();
                    grup.NombreAlumno = dta[2].ToString();
                    grup.ApelPater = dta[3].ToString();
                    grup.ApelMater = dta[4].ToString();

                    nombreGrupo.Add(grup);
                }
            }
            else
            {
                return null;
            }
            acceso.desconectar();

            return nombreGrupo;
        }

        public List<Grupos> datosCombinados3b()
        {
            MySqlDataReader dta = acceso.innerJoin("grupo", "alumno", "nombre", "nombre", "apellidoPaterno", "apellidoMaterno", "matricula", "matricula", "3-B");
            nombreGrupo.Clear();

            if (dta.HasRows)
            {
                while (dta.Read())
                {
                    Grupos grup = new Grupos();
                    grup.MatriculaGrid = int.Parse(dta[0].ToString());
                    grup.NomGrupo = dta[1].ToString();
                    grup.NombreAlumno = dta[2].ToString();
                    grup.ApelPater = dta[3].ToString();
                    grup.ApelMater = dta[4].ToString();

                    nombreGrupo.Add(grup);
                }
            }
            else
            {
                return null;
            }
            acceso.desconectar();

            return nombreGrupo;
        }

        public int Id_grupo1
        {
            get
            {
                return Id_grupo;
            }

            set
            {
                Id_grupo = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public int Matricula
        {
            get
            {
                return matricula;
            }

            set
            {
                matricula = value;
            }
        }

        public string NomGrupo
        {
            get
            {
                return nomGrupo;
            }

            set
            {
                nomGrupo = value;
            }
        }

        public string NombreAlumno
        {
            get
            {
                return nombreAlumno;
            }

            set
            {
                nombreAlumno = value;
            }
        }

        public string ApelPater
        {
            get
            {
                return apelPater;
            }

            set
            {
                apelPater = value;
            }
        }

        public string ApelMater
        {
            get
            {
                return apelMater;
            }

            set
            {
                apelMater = value;
            }
        }

        public int MatriculaGrid
        {
            get
            {
                return matriculaGrid;
            }

            set
            {
                matriculaGrid = value;
            }
        }
    }
}
